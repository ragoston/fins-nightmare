﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Checkpoints
{
    [Serializable]
    public class CheckpointData
    {
        public int levelNumber;
        public int latestCheckpoint;
        public int selectedCheckpoint;
        public CheckpointData(int levelNumber)
        {
            this.levelNumber = levelNumber;
            latestCheckpoint = 0;
            selectedCheckpoint = 0;
        }
    }
}
