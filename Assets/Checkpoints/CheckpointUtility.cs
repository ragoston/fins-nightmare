﻿using Assets.Scripts.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Checkpoints
{
    internal class CheckpointUtility : ICheckpointProvider
    {
        CheckpointSave c;
        private DataBank bank;
        [Serializable]
        public class CheckpointSave
        {
            public CheckpointData[] checkpointData;
            public CheckpointSave(int size)
            {
                checkpointData = new CheckpointData[size];
            }
        }
        private const string LEVEL_CLASS_TAG = "LevelCheckpoint";
        //private List<LevelCheckpoint> levelCheckpoints;
        //TODO: need to set the selected checkpoint every time we hit one. just don't increase the unlocked ones.
        public CheckpointUtility(DataBank bank)
        {
            this.bank = bank;
            c = bank.Load<CheckpointSave>(DataBank.CHECKPNT_NAME);
            if(c == null || c.checkpointData.Count() == 0)
            {
                c = new CheckpointSave(bank.Stats.levelIdentifiers.Count);
                for (int i = 0; i < bank.Stats.levelIdentifiers.Count; i++)
                {
                    c.checkpointData[i] = new CheckpointData(
                        bank.Stats.levelIdentifiers[i].LevelNumber);
                }

                bank.Save(JsonUtility.ToJson(c), DataBank.CHECKPNT_NAME);
            }
        }

        public void CheckpointWrapper(bool isUnlocked)
        {
            UpdateCheckpoints(bank.Stats.LastPlayedLevel - 1,
                isUnlocked);
        }
        public void UpdateCheckpoints(int levelIndex, bool isUnlocked)
        {
            /*
             * logic here: if it was unlocked, just set the selected to this and save.
             * if it wasn't unlocked, unlock it, set the selected and save
             */
            
            var levelNumber = levelIndex + 1;
            var d = c.checkpointData.Where(x => x.levelNumber == levelNumber).First();


            if(d != null)
            {
                if (!isUnlocked)
                {
                    d.latestCheckpoint++;
                    d.selectedCheckpoint = d.latestCheckpoint;
                }
                else
                {
                    d.selectedCheckpoint++;
                }
                try
                {
                    bank.SaveAsync(JsonUtility.ToJson(c), DataBank.CHECKPNT_NAME);
                }
                catch
                {
                    //then so on the same thread.
                    bank.Save(JsonUtility.ToJson(c), DataBank.CHECKPNT_NAME);
                }
                
            }
        }

        public string GetTag()
        {
            return LEVEL_CLASS_TAG;
        }

        public List<CheckpointData> GetCheckpointData()
        {
            return c.checkpointData.ToList();
        }
    }
}
