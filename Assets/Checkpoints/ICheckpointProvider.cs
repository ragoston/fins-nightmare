﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Checkpoints
{
    public interface ICheckpointProvider
    {
        /// <summary>
        /// Call this to update the checkpoints. No new input needed - 
        /// just a ++.
        /// </summary>
        /// <param name="levelNumber"></param>
        void UpdateCheckpoints(int levelNumber, bool isUnlocked);
        /// <summary>
        /// Doesn't do shit, just a wrapper for the former, in case it's needed.
        /// </summary>
        void CheckpointWrapper(bool isUnlocked);
        /// <summary>
        /// Getting a tag that is meant for the scene objects to identify.
        /// </summary>
        /// <returns></returns>
        string GetTag();
        /// <summary>
        /// Getting a collection of the checkpoints' references.
        /// </summary>
        /// <returns></returns>
        List<CheckpointData> GetCheckpointData();
    }
}
