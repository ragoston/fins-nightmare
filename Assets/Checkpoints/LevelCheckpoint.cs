﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Checkpoints
{
    class LevelCheckpoint : MonoBehaviour
    {
        public delegate void Update_chkpnt(bool isUnlocked);
        public event Update_chkpnt UPDATE_CHECKPOINT;
        public bool isEnabled = true;
        public bool isUnlocked = false;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                if (isEnabled && UPDATE_CHECKPOINT != null)
                {
                    UPDATE_CHECKPOINT(isUnlocked);
                    isEnabled = false;
                }         
            }
        }
    }
}
