﻿using Assets.MainMenuStuff.MMToolkitConsumers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using Assets.MainMenuStuff;
using UnityEngine;
using UnityEngine.UI;
using Assets.MainMenuStuff.DataStructures;

namespace Assets.Checkpoints._MainMenu
{
    public class CheckpointSelector : MonoBehaviour
    {
        private ISwipeFunctionality mySwiper;
        private ICheckpointProvider checkpoint;
        //private class Dots
        //{
        //    List<Image> images;
        //    Sprite[] sprites;
        //    public Dots(GameObject parent)
        //    {
        //        images = new List<Image>();
        //        DoRecursivelyInGameObject((go) =>
        //        {
        //            var im = go.GetComponent<Image>();
        //            if(im != null)
        //            {
        //                images.Add(im);
        //            }
        //        }, parent);
        //        sprites = Resources.LoadAll<Sprite>("checkPointMatrix");
                
        //    }
        //    public void SwapSprite(int numberOfDots)
        //    {
        //        if (numberOfDots <= 0)
        //            numberOfDots = 1;
        //        int i = 0;
        //        i = sprites.Length - numberOfDots;
        //        foreach (var item in images)
        //        {
        //            item.sprite = sprites[i];
        //        }
        //    }
        //    private void DoRecursivelyInGameObject(Action<GameObject> whatToDo, GameObject obj)
        //    {
        //        whatToDo(obj);
        //        foreach (Transform child in obj.transform)
        //        {
        //            DoRecursivelyInGameObject(whatToDo, child.gameObject);
        //        }
        //    }
        //}
        private int selectedLevel;
        private CircularList<CheckpointData> checkpoint_data;

        private sealed class TextManager : IIfExistsAction
        {
            /// <summary>
            /// Requires: UnityEngine.Text text to modify, String message
            /// </summary>
            /// <param name="parameters"></param>
            public void DoAction(params object[] parameters)
            {
                (parameters[0] as Text)
                    .text = parameters[1] as String;
            }
        }
        private sealed class TextManager_empty : IIfExistsAction
        {
            /// <summary>
            /// Don't do shit. We had no text so here we avoid errors.
            /// </summary>
            /// <param name="parameters"></param>
            public void DoAction(params object[] parameters) { }
        }
        private IIfExistsAction textModifier;
        //Dots dots;
        //[SerializeField]
        //private Image fillImage;
        private Text checkpointText;
        public void Initialize(ICheckpointProvider provider)
        {
            this.checkpoint = provider;

            mySwiper = (ISwipeFunctionality)GetComponent(typeof(ISwipeFunctionality));
            if (mySwiper == null)
            {
                print("couldn't find swiper on checkpoint selector");
                return;
            }
            //dots = new Dots(gameObject);
            
            try
            {
                checkpointText = transform.Find("selectedCheckpoint")
                .GetComponent<Text>();
                textModifier = new TextManager();
            }
            catch
            {
                print("unable to show checkpoint selection. removing it now...");
                foreach (Transform c in transform)
                {
                    c.gameObject.SetActive(false);
                }
                textModifier = new TextManager_empty();
            }
            
            checkpoint_data = new CircularList<CheckpointData>();
            checkpoint_data.ReadEnumerable(checkpoint.GetCheckpointData());
            transform.parent.GetComponentInChildren<LevelSelectHelper>()
                .OnTurnTaken += this.OnTurnTaken;
            mySwiper.OnSwiped += (direction) =>
            {
                //change the selected checkpoint and update UI here
                CheckpointData d = checkpoint_data[selectedLevel];
                if(direction > 0)
                {
                    if(d.selectedCheckpoint+1 <= d.latestCheckpoint)
                    {
                        d.selectedCheckpoint++;
                        //ChangeFillByNumber(d);
                    }
                }
                else
                {
                    if(d.selectedCheckpoint-1 >= 0)
                    {
                        //the first checkpoint.
                        d.selectedCheckpoint--;
                        //ChangeFillByNumber(d);
                    }
                }
                SetText(d);
            };
        }
        private void SetText(CheckpointData d)
        {
            string s;
            if (d.selectedCheckpoint <= 0)
            {
                s = "Beginning";
            }
            else
            {
                s = d.selectedCheckpoint.ToString();
            }
            textModifier.DoAction(checkpointText, s);
        }
        //private void ChangeFillByNumber(CheckpointData data)
        //{
        //    float fillAm;
        //    try
        //    {
        //        fillAm = (data.selectedCheckpoint * 1f)
        //        / (data.latestCheckpoint * 1f);
        //    }
        //    catch
        //    {
        //        fillAm = 0f;
        //    }
        //    fillImage.fillAmount = fillAm+0.1f;
        //}
        public void OnTurnTaken(int Index)
        {
            

            print("Turn taken, updating checkpoints with index: "
                + Index.ToString());
            selectedLevel = Index;

            //dots.SwapSprite(checkpoint_data[Index].latestCheckpoint);
            CheckpointData d = checkpoint_data[Index];
            if(d != null)
            {
                d.selectedCheckpoint = d.latestCheckpoint;
                //ChangeFillByNumber(d);
                SetText(d);
            }      
        }
    }
}
