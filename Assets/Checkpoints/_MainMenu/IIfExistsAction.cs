﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Checkpoints._MainMenu
{
    /// <summary>
    /// If the component exsists, pull off the action. 
    /// else don't do shit. 
    /// Made so I can get rid of if x == null bullshit all the time.
    /// </summary>
    public interface IIfExistsAction
    {
        void DoAction(params object[] parameters);
    }
}
