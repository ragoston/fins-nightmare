﻿using Assets.MainMenuStuff.MMToolkitConsumers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;

namespace Assets.Checkpoints._MainMenu
{
    public class InitCheckpointSelector : InitializeMe
    {
        public override void INIT(INITIALIZER pers)
        {
            GetComponent<CheckpointSelector>()
                .Initialize(pers.DATA.CheckpointProvider);
        }
    }
}
