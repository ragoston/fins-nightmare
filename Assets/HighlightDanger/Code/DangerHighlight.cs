﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.HighlightDanger.Code
{
    [RequireComponent(typeof(ParticleSystem))]
    public class DangerHighlight : MonoBehaviour
    {
        private ParticleSystem system;
        public bool wasInitializedFine = true;
        private static Dictionary<string, Mesh> LoadedAdditionalMeshes;
        Mesh emitterMesh;
        static Mesh[] replacements;
        //to load only once.
        private static bool resourcesLoaded = false;
        internal void Initialize()
        {
            if(LoadedAdditionalMeshes == null)
            {
                LoadedAdditionalMeshes = new Dictionary<string, Mesh>();
            }
            system = GetComponent<ParticleSystem>();
            if(system.shape.mesh == null)
            {
                GetMeshEmitter();
            }
        }
        private void GetMeshEmitter()
        {
            /*
             * shit to pull off:
             * 1. try to find a mesh already on the renderer -- done.
             * 2. if we don't have, we need to find a replacement in the Resources/highlighMeshes folder.
             * 3. if we found it, go ahead and match up the scale/rotation.
             * 
             */

            //1.
            var mf = gameObject.GetComponentFromParentRecursive<MeshFilter>();

            if (mf != null && mf.mesh != null)
            {
                try
                {
                    if (mf.mesh.isReadable)
                    {
                        Mesh myAwesomeNewMeshForNoApparentReason2 = new Mesh();
                        myAwesomeNewMeshForNoApparentReason2.Clear();
                        myAwesomeNewMeshForNoApparentReason2.name = mf.gameObject.name + "_fuckingNewMesh";
                        myAwesomeNewMeshForNoApparentReason2.vertices = mf.sharedMesh.vertices;
                        myAwesomeNewMeshForNoApparentReason2.triangles = mf.sharedMesh.triangles;
                        myAwesomeNewMeshForNoApparentReason2.uv = mf.sharedMesh.uv;
                        var sh2 = system.shape;
                        sh2.mesh = myAwesomeNewMeshForNoApparentReason2;
                        return;
                    }

                }
                catch { wasInitializedFine = false; }


                

                try
                {
                    string nameToLookFor = mf.mesh.name.Replace(" Instance", "");
                    Mesh myAwesomeNewMeshForNoApparentReason = null;
                    if (!LoadedAdditionalMeshes.ContainsKey(nameToLookFor))
                    {
                        Mesh m = Resources.Load<Mesh>("Meshes/" + nameToLookFor);
                        if (m != null)
                        {
                            //add to dict.
                            LoadedAdditionalMeshes.Add(m.name, m);
                        }
                    }
                    myAwesomeNewMeshForNoApparentReason = LoadedAdditionalMeshes[nameToLookFor];
                    var sh2 = system.shape;

                    sh2.mesh = myAwesomeNewMeshForNoApparentReason;
                }
                catch
                {
                    wasInitializedFine = false;
                }

                return;
            }
            //2.
            try
            {
                if (!resourcesLoaded)
                {
                    resourcesLoaded = true;
                    replacements = Resources.LoadAll<Mesh>("highlightMeshes");
                }

            }
            catch { wasInitializedFine = false; return; }

            if (replacements == null)
            {
                wasInitializedFine = false;
                return;
            }
            //here we have the meshes we need to make the replacement.
            //now find the sprite to replace.
            Mesh myMesh;
            var sr = gameObject.GetComponentFromParentRecursive<SpriteRenderer>();
            if (sr != null && sr.sprite != null)
            {
                myMesh = replacements.Where(x => x.name.Equals(sr.sprite.name)).FirstOrDefault();
                if (myMesh != null)
                {
                    var s = system.shape;
                    s.mesh = myMesh;
                }
                else
                {
                    wasInitializedFine = false;
                    #region old failsafe
                    //try
                    //{


                    //    ////need to load a default mesh
                    //    //GameObject rep = Resources.Load<GameObject>("dangerCollider/DangerCollider.prefab");
                    //    //Instantiate<GameObject>(rep);
                    //    //rep.transform.parent = transform.parent;
                    //    //rep.transform.localPosition = Vector3.zero;
                    //    //rep.transform.localRotation = Quaternion.identity;
                    //    //var b = rep.GetComponent<Renderer>().bounds;
                    //    //try
                    //    //{
                    //    //    b.size = transform.parent.GetComponent<Renderer>().bounds.size;
                    //    //}
                    //    //catch
                    //    //{
                    //    //    try
                    //    //    {
                    //    //        b.size = transform.parent.GetComponent<Collider2D>().bounds.size;
                    //    //    }
                    //    //    catch { }
                    //    //}

                    //    //myMesh = rep.GetComponent<MeshFilter>().mesh;

                    //    //myMesh = replacements.Where(x => x.name == "default").FirstOrDefault();
                    //    //var s = system.shape;
                    //    //s.mesh = myMesh;
                    //    //if (sr != null && sr.sprite != null)
                    //    //{
                    //    //    transform.localScale = new Vector3(
                    //    //        sr.bounds.extents.x, sr.bounds.extents.y, transform.localScale.z);
                    //    //}
                    //    //else if (gameObject.GetComponentFromParentRecursive<Collider2D>() != null)
                    //    //{
                    //    //    var col = gameObject.GetComponentFromParentRecursive<Collider2D>()
                    //    //        .bounds.extents;
                    //    //    transform.localScale = new Vector3(
                    //    //        col.x, col.y, transform.localScale.z);
                    //    //}

                    //    //last resort: get mesh from sprite.
                    //    myMesh = SpriteToMesh(sr.sprite);
                    //    var s = system.shape;
                    //    s.mesh = myMesh;
                    //    var ss = system.main.startSize;
                    //    ss.curveMultiplier += 0.2f;

                    //}
                    //catch { }
                    #endregion
                }
            }

        }
        //public static bool ContainsAppropriateSpriteOrMesh(GameObject o)
        //{
        //    var mf = o.GetComponentFromParentRecursive<MeshFilter>();
        //    if (mf != null && mf.mesh != null)
        //    {
        //        return true;
        //    }
        //    var sr = o.GetComponentFromParentRecursive<SpriteRenderer>();
        //    if(sr != null && sr.sprite != null)
        //    {
        //        try
        //        {
        //            if (!resourcesLoaded)
        //            {
        //                resourcesLoaded = true;
        //                replacements = Resources.LoadAll<Mesh>("highlightMeshes");
        //                if ((replacements.Where(x => x.name == sr.sprite.name).FirstOrDefault()) != null)
        //                {
        //                    return true;
        //                }
        //            }
        //        }
        //        catch { return false; }
        //    }

        //    return false;
        //}
//        private void SetupMesh()
//        {
//            var mf = transform.parent.gameObject.GetComponentFromParentRecursive<MeshFilter>();

//            if (mf != null && mf.mesh != null)
//            {
//                emitterMesh = mf.mesh;
//            }
//            else
//            {
//                var sr = transform.parent.gameObject.GetComponentFromParentRecursive<SpriteRenderer>();
//                if (sr != null && sr.sprite != null)
//                {
//                    try
//                    {
//                        emitterMesh = SpriteToMesh(sr.sprite);
//                    }
//                    catch
//                    {
//                        Debug.Log("SpriteToMesh failed. " + transform.parent.name);
//                    }

//                }
//                else
//                {
//#if UNITY_EDITOR
//                    //throw new MissingComponentException("No mesh or sprite on object hierarchy for DangerHighlight script on object"
//                    //    + transform.parent.name);
//#endif

//                }

//            }
//            var s = system.shape;
//            s.enabled = true;
//            s.shapeType = ParticleSystemShapeType.Mesh;
//            s.mesh = emitterMesh;
//        }
        private Mesh SpriteToMesh(Sprite sprite)
        {
            Mesh mesh = new Mesh();
            mesh.SetVertices(Array.ConvertAll(sprite.vertices, i => (Vector3)i).ToList());
            mesh.SetUVs(0,sprite.uv.ToList());
            mesh.SetTriangles(Array.ConvertAll(sprite.triangles, i => (int)i),0);

            return mesh;
        }
        public void SetHighlightColor(Color c)
        {
            var myColor = system.main;
            myColor.startColor = c;
        }
    }
}
