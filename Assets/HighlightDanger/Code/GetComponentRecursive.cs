﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.HighlightDanger.Code
{
    public static class GetComponentRecursive
    {
        /// <summary>
        /// Returns the first Component (T) found on the parent or its children
        /// = the gameobjects on the same level as this is called on.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="g"></param>
        /// <returns></returns>
        public static T GetComponentFromParentRecursive<T>(this GameObject g)
            where T : Component
        {
            if(g.transform.parent.GetComponent<T>() != null)
            {
                return g.transform.parent.GetComponent<T>();
            }
            else
            {
                foreach (Transform c in g.transform.parent.transform)
                {
                    if(c.GetComponent<T>() != null)
                    {
                        return c.GetComponent<T>();
                    }
                }
            }
            return null;
        }
    }
}
