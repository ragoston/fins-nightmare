﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.HighlightDanger.Code
{
    public static class HighlightExtensions
    {
        public static GameObject InstantiateHighlightDanger(this GameObject prototype,
            Transform parent, DangerHighlightEffectColor cg)
        {
            if(prototype == null)
            {
#if UNITY_EDITOR
                throw new NullReferenceException(
                    "Highlight object received NULL as input to initialize.");
#else
                return null;
#endif
            }
            var danger = GameObject.Instantiate<GameObject>(prototype);

            danger.transform.parent = parent.transform;
            danger.transform.localPosition = Vector3.zero;
            danger.transform.localRotation = Quaternion.identity;
            danger.transform.localScale = Vector3.one;
            //var dangerParticles = danger.GetComponent<HighlightDangerSaver>()
            //    .FindOnlyChild();
            parent.gameObject.AddComponent<HighlightDangerSaver>();

            try
            {

                //float originalStartSize = danger.GetComponent<ParticleSystem>().main.startSizeMultiplier;

                //var b = parent.GetComponent<Renderer>().bounds.size;
                //var m = danger.GetComponent<ParticleSystem>().main;
                //m.startSize = new ParticleSystem.MinMaxCurve(
                //    ((b.x+b.y)/2) * originalStartSize, m.startSize.curve);
                float originalStartSize = danger.GetComponent<ParticleSystem>().main.startSizeMultiplier;

                var b = parent.GetComponent<Renderer>().bounds.size;
                var m = danger.GetComponent<ParticleSystem>().main;
                m.startSize = new ParticleSystem.MinMaxCurve(
                    ((b.x + b.y) / 2) * originalStartSize, m.startSize.curve);

            }
            catch { /*fuck if I care*/}
            var dh = danger.GetComponent<DangerHighlight>();
            if(dh != null)
            {
                dh.Initialize();
            }
            if (cg != null)
            {
                danger.GetComponent<DangerHighlight>().SetHighlightColor(
                    cg.HighlightColor);
            }
            var hds = parent.GetComponent<HighlightDangerSaver>();
            if (hds != null)
            {
                hds.Initialize();
            }
            danger.SetActive(false);
            return danger;
        }
    }
}
