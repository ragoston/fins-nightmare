﻿using Assets.Scripts.InGameInit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using UnityEngine;

namespace Assets.HighlightDanger.Code
{
    [RequireComponent(typeof(DangerHighlight))]
    public class InitHighlight : InitializeMeIngame
    {
        public override void INITIALIZE(DataBank bank)
        {
            if (bank.Stats.GamePlaySettings.highlights/*true*/)
            {
                GetComponent<DangerHighlight>().Initialize();
            }          
        }
    }
}
