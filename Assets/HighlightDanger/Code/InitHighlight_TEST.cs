﻿using Assets.MainMenuStuff.MMToolkitConsumers;
using Assets.Scripts.InGameInit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.HighlightDanger.Code
{
    public class InitHighlight_TEST : MonoBehaviour
    {
        [SerializeField]
        private GameObject highlightPrefab;
        private void Awake()
        {
            if (highlightPrefab == null)
                return;
            foreach (var g in GameObject.FindGameObjectsWithTag("DANGER"))
            {
                //highlightPrefab.InstantiateHighlightDanger(g.transform);

            }
            foreach (var item in GameObject.FindGameObjectsWithTag("InGameInit")
                .Where(x => x.GetComponent<InitializeMeIngame>() != null))
            {
                item.GetComponent<InitializeMeIngame>().INITIALIZE(
                    new Scripts.Statistics.DataBank());
            }
        }
    }
}
