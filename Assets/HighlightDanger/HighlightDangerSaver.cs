﻿using Assets.HighlightDanger.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.HighlightDanger
{
    public class HighlightDangerSaver : MonoBehaviour
    {
        GameObject particleObject;
        DangerHighlight highlight;
        public void Initialize()
        {
            particleObject = GetChildrenOfGameObject(transform)
                .Where(x => x.CompareTag("DangerIndicator")).FirstOrDefault();
            highlight = particleObject.GetComponent<DangerHighlight>();
            //FindOnlyChild().SetActive(false);
        }
        private void OnBecameVisible()
        {
            if(particleObject != null && highlight != null)
            {
                if (highlight.wasInitializedFine)
                {
                    particleObject.SetActive(true);
                }               
            }
                
//#if UNITY_EDITOR
//            Debug.Log("Highlight became visible.");
//#endif
//            FindOnlyChild().SetActive(true);
        }
        private void OnBecameInvisible()
        {
            if (particleObject != null)
            {
                particleObject.SetActive(false);
            }
                
//#if UNITY_EDITOR
//            Debug.Log("Highlight gone.");
//#endif
//            FindOnlyChild().SetActive(false);
        }
        public GameObject FindOnlyChild()
        {
            foreach (Transform item in transform)
            {
                return item.gameObject;
            }
            return null;
        }
        IEnumerable<GameObject> GetChildrenOfGameObject(Transform obj)
        {
            foreach (Transform item in obj.transform)
            {
                yield return item.gameObject;
            }
        }
    }
}
