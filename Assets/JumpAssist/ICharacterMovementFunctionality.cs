﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.JumpAssist
{
    public interface ICharacterMovementFunctionality
    {
        void Movement();

        bool movementDisabled { get; set; }
        Rigidbody2D rb2d { get; set; }
        float maxSpeed { get; set; }
        Transform groundCheck { get; set; }
        Transform wallCheck { get; set; }
        LayerMask whatIsGround { get; set; }
        LayerMask whatIsWall { get; set; }
        float groundRadius { get; set; }
        bool grounded { get; set; }
        bool jump { get; set; }
        float jumpSpeed { get; set; }
        Action UPDATE_ANIM { get; set; }
        Func<Vector2> GetJumpSpeed { get; set; }
        Func<Vector2> GetDefaultJumpSpeed { get;}
    }
}
