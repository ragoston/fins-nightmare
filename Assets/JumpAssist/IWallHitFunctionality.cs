﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.JumpAssist
{
    public interface IWallHitFunctionality
    {
        void OnWallHit(Collider2D other);
        GameObject Player { get; set; }
    }
}
