﻿using System;
using UnityEngine;

namespace Assets.JumpAssist
{
    internal class JumpAidUnit : MonoBehaviour
    {
        internal void Initialize(JumpAidUtility jumpAidUtility)
        {

        }
        internal Transform next;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            /*
             * Calculate the formula. Note that we do this on demand, meaning as soon as
             * the player hits jump, not any sooner.
             */

            if (collision.gameObject.CompareTag("Player"))
            {
                if (next != null)
                {
                    CharacterController2D.charMovement.GetJumpSpeed = () =>
                            {
                                return new Vector2(CharacterController2D.charMovement.maxSpeed,
                                    GetAssistedJumpSpeed(next.position.x - collision.transform.position.x,
                                    CharacterController2D.charMovement.maxSpeed,
                                    collision.transform.position.y - next.position.y));
                            };
                }
                else
                {
                    CharacterController2D.charMovement.GetJumpSpeed =
                       CharacterController2D.charMovement.GetDefaultJumpSpeed;
                }
            }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                CharacterController2D.charMovement.GetJumpSpeed =
                    CharacterController2D.charMovement.GetDefaultJumpSpeed;
            }
        }
        internal float GetAssistedJumpSpeed(float deltaX, float vDrag, float deltaY)
        {
            return Mathf.Abs(Physics2D.gravity.y) * vDrag *
                (((deltaX * deltaX) / (vDrag * vDrag)) - ((2 * deltaY) / (Mathf.Abs(Physics2D.gravity.y))))
                * (1 / (2 * deltaX));
        }
    }
}