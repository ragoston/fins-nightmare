﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.JumpAssist
{
    public class JumpAidUtility : MonoBehaviour
    {
        public List<Transform> myJumpUnits;
        internal JumpAidUnit Current { get; set; }
        private void Awake()
        {
            if (myJumpUnits.Count == 0)
                return;
            for (int i = 0; i < myJumpUnits.Count; i++)
            {
                var jumpUnitScript = myJumpUnits[i].GetComponent<JumpAidUnit>();

                if (jumpUnitScript == null)
                    continue;

                if(i < myJumpUnits.Count-1)
                    jumpUnitScript.next = myJumpUnits[i + 1];
                else
                {
                    jumpUnitScript.next = null;
                }
                jumpUnitScript.Initialize(this);                
            }
        }

    }
}
