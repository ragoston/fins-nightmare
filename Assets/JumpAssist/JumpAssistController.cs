﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.JumpAssist
{
    public class JumpAssistController : MonoBehaviour
    {
        /// <summary>
        /// If the character is inside a jump aim helper, jumps will be 
        /// calculated instead of letting the player to jump how it should.
        /// </summary>
        /// 
        [Tooltip("Set by the jump aid helpers the character can get inside.")]
        public bool jumpAimEnabled = false;


        public void SetJumpVector(Rigidbody2D target)
        {
            /*
             * setting the jump velocity for but 1 frame. After that, all will be the same
             * as it used to be.
             */ 


        }
    }
}
