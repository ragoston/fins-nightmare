﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.JumpAssist.Trampolines
{
    public interface ITrampolineFunctionality
    {
        Func<Rigidbody2D, Vector2> GetSimpleJump { get; set; }
        Func<Rigidbody2D, Vector2> GetDoubleJump { get; set; }
    }
}
