﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.JumpAssist.Trampolines
{
    public class TrampolineAssistUnit : MonoBehaviour
    {
        ITrampolineFunctionality trampoline;
        internal BoxCollider2D myCol;
        internal Transform next;
        [Tooltip("Does the player have to jump on the trampoline to succeed?")]
        public bool haveToDouble;

        private bool WasActivated = false;
        internal void Initialize(TrampolineAssistUtility master)
        {
            trampoline = GetComponent(typeof(ITrampolineFunctionality)) as
                ITrampolineFunctionality;
            var colladas = GetComponents<BoxCollider2D>();

            if (colladas == null && colladas.Length <= 0)
                return;

            //myCol = colladas
            //    .Where(y => (y.bounds.center.y + y.bounds.extents.y).Equals(
            //        colladas.Max(x => (x.bounds.center.y + x.bounds.extents.y))))
            //        .FirstOrDefault();                 
            myCol = colladas.Where(x => x.isTrigger).FirstOrDefault();
            WasActivated = true;
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                if (!WasActivated)
                    return;
                Vector2 _next;
                if (next != null)
                {
                    var nextCol = next.GetComponent<TrampolineAssistUnit>();
                    if (nextCol != null)
                    {
                        _next = new Vector2(nextCol.myCol.bounds.center.x, nextCol.myCol.bounds.center.y);
                    }
                    else
                    {
                        _next = new Vector2(next.position.x, next.position.y);
                    }

                }
                else
                {
                    return;
                }
                //then set the given delegate to our new one.
                if (haveToDouble)
                {

                    //we set the double jump delegate.
                    trampoline.GetDoubleJump = (other) =>
                    {
                        //return new Vector2(other.velocity.x, GetAssistedJumpSpeed(
                        //    Mathf.Abs(_next.x - myCol.bounds.center.x),
                        //    other.velocity.x, myCol.bounds.center.y - _next.y));
                        return new Vector2(other.velocity.x, GetAssistedJumpSpeed(
                             Mathf.Abs(_next.x - other.transform.position.x),
                             other.velocity.x, other.transform.position.y - _next.y));
                    };
                }
                else
                {
                    //we set the simple jump delegate.
                    trampoline.GetSimpleJump = (other) =>
                    {
                        //return new Vector2(other.velocity.x, GetAssistedJumpSpeed(
                        //    Mathf.Abs(_next.x - myCol.bounds.center.x),
                        //    other.velocity.x, myCol.bounds.center.y - _next.y));
                        return new Vector2(other.velocity.x, GetAssistedJumpSpeed(
                             Mathf.Abs(_next.x - other.transform.position.x),
                             other.velocity.x, other.transform.position.y - _next.y));
                    };
                }
            }
        }
        private float GetAssistedJumpSpeed(float deltaX, float vDrag, float deltaY)
        {
            return Mathf.Abs(Physics2D.gravity.y) * vDrag *
                (((deltaX * deltaX) / (vDrag * vDrag)) - ((2 * deltaY) / (Mathf.Abs(Physics2D.gravity.y))))
                * (1 / (2 * deltaX));
        }
        
    }
}
