﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.JumpAssist.Trampolines
{
    public class TrampolineAssistUtility : MonoBehaviour
    {
        public List<Transform> units;
        private void Awake()
        {
            if(units == null || units.Count <= 0)
            {
                return;
            }
            for (int i = 0; i < units.Count; i++)
            {
                TrampolineAssistUnit unit;
                unit = units[i].GetComponent<TrampolineAssistUnit>();
                if (unit == null) continue;
                if(unit != null)
                {
                    unit.Initialize(this);
                    if (i < units.Count - 1)
                    {
                        if (units[i + 1] != null)
                        {
                            unit.next = units[i + 1].transform;
                        }                        
                    }
                    else
                    {
                        unit.next = null;
                    }
                }
            }
        }
    }
}
