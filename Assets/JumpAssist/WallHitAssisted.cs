﻿using Assets.MainMenuStuff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.JumpAssist
{
    public class WallHitAssisted : MonoBehaviour, IWallHitFunctionality
    {
        public GameObject Player { get; set; }
        public void OnWallHit(Collider2D other)
        {
            /*
             * two things can happen: 
             * player hit the wall but not too much - teleport over.
             * player hit the wall hard - kill player.
             */

            if (other.gameObject.CompareTag("Wall"))
            {

                if (!CharacterController2D.playerDied && GetPlayerPositionToWall(other))
                {
                    AssistPlayerUp(other, 0.2f);
                    //JumpInput.JUMP = false;
                    Debug.Log("ASSISTED WALL JUMP");

                }
                else
                {
                    //meaning we hit the ground with the side
                    //and not the feet
                    //player.GetComponent<CharacterController2D>().facePalm = true;
                    var charC = Player.GetComponent<CharacterController2D>();
                    charC.playerSmoke.SetActive(true);
                    StartCoroutine(charC.HitTheRunnerBack());
                }

            }
        }
        /// <summary>
        /// Assist the player up on the wall.
        /// </summary>
        /// <param name="wall">wall the player has hit. </param>
        /// <param name="safety">the additional height to add 
        /// so the player doesn't stuck in the wall.</param>
        private void AssistPlayerUp(Collider2D wall, float safety)
        {
            var cm = Player.GetComponent<CharacterController2D>();
            float additionalY = Mathf.Abs(((wall.bounds.center.y + wall.bounds.extents.y)
                - cm.groundCheck.position.y)) + safety;
            var playerBox = Player.GetComponent<BoxCollider2D>();
            float additionalX = Mathf.Abs(playerBox.bounds.extents.x / 2);
            Player.transform.position = new Vector3(Player.transform.position.x + additionalX,
                Player.transform.position.y + additionalY, Player.transform.position.z);
        }
        private bool GetPlayerPositionToWall(Collider2D wall)
        {
            BoxCollider2D playerCollider = Player.GetComponent<BoxCollider2D>();
            if((playerCollider.bounds.center.y >
                wall.bounds.center.y + wall.bounds.extents.y))
            {
                return true;
            }return false;
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            OnWallHit(collision);
        }
    }
}
