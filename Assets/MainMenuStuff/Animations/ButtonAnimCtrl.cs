﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animation))]
[RequireComponent(typeof(Button))]
public class ButtonAnimCtrl : MonoBehaviour
{
    Animation buttonAnim;
    UnityEngine.Events.UnityAction pressed;
    Button but;
    private void Start()
    {
        buttonAnim = GetComponent<Animation>();
        pressed += PlayButtonAnim;
        but = GetComponent<Button>();
        but.onClick.AddListener(pressed);
    }
    public void PlayButtonAnim()
    {
        if (!buttonAnim.isPlaying)
            buttonAnim.Play();
    }	
}
