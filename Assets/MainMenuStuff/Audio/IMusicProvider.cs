﻿using System.Collections;

namespace Assets.MainMenuStuff.Audio
{
    public interface IMusicProvider
    {
        IEnumerator PlayMusic(UnityEngine.AudioSource source,
            UnityEngine.AudioClip clip);
        IEnumerator FadeMusicOut(float time);
        void ShutDownMusicImmediately();
        void SetVolume(float volume);
        void Initialize();
        void Pause();
        void Continue();
    }
} 