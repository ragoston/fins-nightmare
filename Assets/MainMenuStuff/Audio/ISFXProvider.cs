﻿using Assets.MainMenuStuff.MMToolkitConsumers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.MainMenuStuff.Audio
{
    public interface ISFXProvider
    {
        SettingsValues Settings { get; set; }
        void PlaySFX(AudioClip clipToPlay);
        void PlaySFX(uint clipIndex);
        void StopSFXImmediate();
    }
}
