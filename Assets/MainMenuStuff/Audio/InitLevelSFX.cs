﻿using Assets.Scripts.InGameInit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using UnityEngine;

namespace Assets.MainMenuStuff.Audio
{
    [RequireComponent(typeof(LevelSFXElement))]
    public class InitLevelSFX : InitializeMeIngame
    {
        public override void INITIALIZE(DataBank bank)
        {
            GetComponent<LevelSFXElement>()
                .Initialize(bank);
        }
    }
}
