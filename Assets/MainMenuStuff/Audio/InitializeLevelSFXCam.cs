﻿using Assets.Scripts.InGameInit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using UnityEngine;

namespace Assets.MainMenuStuff.Audio
{
    [RequireComponent(typeof(LevelSfxPlayer))]
    [RequireComponent(typeof(AudioSource))]
    public class InitializeLevelSFXCam : InitializeMeIngame
    {
        public override void INITIALIZE(DataBank bank)
        {
            var s =
            GetComponent<LevelSfxPlayer>();
            s.source = GetComponent<AudioSource>();
            s.Settings = bank.Stats.Settings;
        }
    }
}
