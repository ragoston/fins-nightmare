﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.MainMenuStuff.MMToolkitConsumers;
using UnityEngine;

namespace Assets.MainMenuStuff.Audio
{
    public class Level5_LightningSFX : MonoBehaviour, ISFXProvider
    {
        public AudioSource[] sources;
        public SettingsValues Settings { get; set; }

        public void PlaySFX(uint clipIndex)
        {
            foreach (var s in sources)
            {
                if (!s.isPlaying)
                {
                    s.Play();
                }
            }
        }
        public void InitMe(SettingsValues s)
        {
            sources = GetComponents<AudioSource>();
            foreach (var so in sources)
            {
                so.volume = s.SFXVolume;
            }
        }
        public void PlaySFX(AudioClip clipToPlay)
        {
            throw new NotImplementedException();
        }

        public void StopSFXImmediate()
        {
            foreach (var s in sources)
            {
                s.Stop();
            }
        }
    }
}
