﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.MainMenuStuff.MMToolkitConsumers;
using Assets.Scripts.Statistics;
using UnityEngine;

namespace Assets.MainMenuStuff.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class LevelSFXElement : MonoBehaviour, ILevelSFXProvider
    {
        public enum HowToPlay { Loop, OnEvent}
        public HowToPlay howToPlay = HowToPlay.Loop;
        public AudioClip[] clips;
        private AudioSource source;

        public void Initialize(DataBank bank)
        {
            source = GetComponent<AudioSource>();
            source.volume = bank.Stats.Settings.SFXVolume;
            this.Settings = bank.Stats.Settings;
        }

        public int whatToPlay = 0;

        public SettingsValues Settings
        {
            get;set;
        }

        public void PlaySFX(AudioClip clipToPlay)
        {
            if (!source.isPlaying)
            {
                source.clip = clipToPlay;
                source.volume = Settings.SFXVolume;
                source.Play();
            }

        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                if (howToPlay == HowToPlay.Loop)
                {
                    source.loop = true;
                    PlaySFX(Convert.ToUInt32(whatToPlay));
                }
            }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                if (source.isPlaying)
                    source.Stop();
            }
        }
        public void PlaySFX(uint index)
        {
            PlaySFX(clips[index]);
        }
        public void StopSFXImmediate()
        {
            if (source.isPlaying)
            {
                source.Stop();
            }
        }
    }
}
