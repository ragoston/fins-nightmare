﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.MainMenuStuff.MMToolkitConsumers;
using UnityEngine;

namespace Assets.MainMenuStuff.Audio
{
    public class LevelSfxPlayer : MonoBehaviour, ISFXProvider
    {
        public AudioClip[] clips;
        [HideInInspector]
        public AudioSource source;

        public SettingsValues Settings
        {
            get;
            set;
        }

        public void PlaySFX(AudioClip clipToPlay)
        {
            if (!source.isPlaying)
            {
                source.clip = clipToPlay;
                source.volume = Settings.MusicVolume;
                source.Play();
            }
                
        }
        public void PlaySFX(uint index)
        {
            PlaySFX(clips[index]);
        }
        public void StopSFXImmediate()
        {
            if (source.isPlaying)
            {
                source.Stop();
            }
        }
    }
}
