﻿using Assets.Scripts.Statistics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.MainMenuStuff.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class MusicPlayer : MonoBehaviour, IMusicProvider, ISettingsReceiver
    {
        [Tooltip("Has to be dragged from the inspector!")]
        public AudioClip myClip;
        public AudioSource mySource;
        //private void Start()
        //{
        //    Initialize();
        //}
        public IEnumerator PlayMusic(AudioSource source, AudioClip clip)
        {
            yield return null;
            if (source.clip == null)
                source.clip = clip;
            if (!source.isPlaying)
            {
                source.Play();
            }
        }

        public IEnumerator FadeMusicOut(float time)
        {
            float timer = mySource.volume;
            var startVol = mySource.volume;
            float lastTime = Time.unscaledTime;

            while (timer < time)
            {
                mySource.volume = Mathf.Lerp(startVol, 0, timer / time);
                timer += (Time.unscaledTime - lastTime);
                lastTime = Time.unscaledTime;
                yield return null;
            }
            mySource.volume = 0;
            if (mySource.isPlaying)
            {
                mySource.Stop();
            }
        }

        public void SetVolume(float volume)
        {
            if (volume < 0) volume = 0;
            mySource.volume = volume;
        }

        public void UpdateDependencies(INITIALIZER init)
        {
            SetVolume(init.DATA.Stats.Settings.MusicVolume);
        }

        public void Initialize()
        {
            //only time to init like so
            if (myClip == null)
            {
#if DEBUG
                Debug.Log("You forgot to add audio clip on the camera.");
#endif
                //then no audio
                Destroy(this);
            }
            mySource = GetComponent<AudioSource>();
            mySource.clip = myClip;
            StartCoroutine(PlayMusic(mySource, myClip));
        }

        public void ShutDownMusicImmediately()
        {
            mySource.volume = 0;
            if (mySource.isPlaying)
                mySource.Stop();
        }

        public void Pause()
        {
            mySource.Pause();
        }

        public void Continue()
        {
            mySource.UnPause();
        }
    }
}
