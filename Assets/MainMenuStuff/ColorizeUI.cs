﻿using Assets.MainMenuStuff.DataStructures;
using Assets.Scripts.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Internal;
using UnityEngine.UI;

namespace Assets.MainMenuStuff
{
    /// <summary>
    /// Class that holds the capability of recolouring a given collection of gameobjects'
    /// components that are either text, image or outline.
    /// </summary>
    public class ColorizeUI
    {
        //[SerializeField]
        //private Material[] levelPlayerMats;
        //private CircularList<Material> _levelPlayerMats;
        //[SerializeField]
        //private Material[] levelMidgMats;
        //private CircularList<Material> _levelMidgMats;
        //[SerializeField]
        //private Material[] levelBGMats;
        //private CircularList<Material> _levelBGMats;
        ////Statistics stats;
        //public string tagToLookFor;
        //GameObject[] elements;
        //private List<Component> componentsToModify;
        //private void Awake()
        //{
        //    if(_levelPlayerMats == null)
        //    {
        //        _levelPlayerMats = new CircularList<Material>();
        //        _levelPlayerMats.ReadEnumerable(levelPlayerMats);
        //        levelPlayerMats = null;
        //    }
        //    if(_levelMidgMats == null)
        //    {
        //        _levelMidgMats = new CircularList<Material>();
        //        _levelMidgMats.ReadEnumerable(levelMidgMats);
        //        levelMidgMats = null;
        //    }
        //    if(_levelBGMats == null)
        //    {
        //        _levelBGMats = new CircularList<Material>();
        //        _levelBGMats.ReadEnumerable(levelBGMats);
        //        levelBGMats = null;
        //    }
        //    //Persistence.LOAD_DEPENDENCIES += LoadData;
        //}
        //[InvocationOrder(2)]
        //private void LoadData(Statistics data, Persistence pers)
        //{
        //    elements = GameObject.FindGameObjectsWithTag(tagToLookFor);
        //    //componentsToModify = new List<Component>();
        //    //foreach (var e in 
        //    //    GameObject.FindGameObjectsWithTag(tagToLookFor))
        //    //{
        //    //    if (e.GetComponent<Text>() != null)
        //    //        componentsToModify.Add(e.GetComponent<Text>());
        //    //    if (e.GetComponent<Outline>() != null)
        //    //        componentsToModify.Add(e.GetComponent<Outline>());
        //    //    if (e.GetComponent<Image>() != null)
        //    //        componentsToModify.Add(e.GetComponent<Image>());
        //    //}
        //    //componentsToModify = 
        //    //    new Queue<Component>(elements.Where(x => x.GetComponent<Component>() is ILayoutElement));
        //    //RecolourUI(pers);
        //}
        /// <summary>
        /// Recolor the text, outline and image properties found on a given gameobject.
        /// </summary>
        /// <param name="obj"> object to get colors from</param>
        /// <param name="textColor"></param>
        /// <param name="imageColor"></param>
        /// <param name="outlineColor"></param>
        public void RecolourUI(GameObject obj,
            Color textColor, Color imageColor, Color outlineColor)
        {
            var e = obj.GetComponent<Text>();
            if (e != null) e.color = new Color(textColor.r,
                textColor.g, textColor.b, e.color.a);

            var f = obj.GetComponent<Outline>();
            if (f != null) f.effectColor = new Color(outlineColor.r,
                outlineColor.g, outlineColor.b, f.effectColor.a);

            var g = obj.GetComponent<Image>();
            if (g != null) g.color = new Color(imageColor.r,
                imageColor.g, imageColor.b, g.color.a);
        }
        //public void RecolourUI(int i, bool setInAllGOs = false)
        //{
        //    GameObject[] coll;
        //    if (setInAllGOs)
        //    {
        //        coll = elements;
        //    }else
        //    {
        //        coll = elements.Where(x => x.activeInHierarchy).ToArray();
        //    }
        //    foreach (GameObject item in coll)
        //    {
        //        var e = item.GetComponent<Text>();
        //        if (e != null) e.color = new Color(_levelPlayerMats[i].color.r,
        //            _levelPlayerMats[i].color.g, _levelPlayerMats[i].color.b, e.color.a);

        //        var f = item.GetComponent<Outline>();
        //        if (f != null) f.effectColor = new Color(_levelBGMats[i].color.r,
        //            _levelBGMats[i].color.g, _levelBGMats[i].color.b, f.effectColor.a);

        //        var g = item.GetComponent<Image>();
        //        if (g != null) g.color = new Color(_levelMidgMats[i].color.r,
        //            _levelMidgMats[i].color.g, _levelMidgMats[i].color.b, g.color.a);
        //    }
        //    //foreach (var item in componentsToModify
        //    //    .Where(x => x.gameObject.activeInHierarchy))
        //    //{
        //    //    if(item is Text)
        //    //    {
        //    //        var iT = item as Text;
        //    //        iT.color = new Color(_levelPlayerMats[i].color.r,
        //    //                _levelPlayerMats[i].color.g, _levelPlayerMats[i].color.b, iT.color.a);
        //    //    }
        //    //    if(item is Outline)
        //    //    {
        //    //        var iO = item as Outline;
        //    //        iO.effectColor = new Color(_levelBGMats[i].color.r,
        //    //                _levelBGMats[i].color.g, _levelBGMats[i].color.b, iO.effectColor.a);
        //    //    }
        //    //    if(item is Image)
        //    //    {
        //    //        var iI = item as Image;
        //    //        iI.color = new Color(_levelMidgMats[i].color.r,
        //    //                _levelMidgMats[i].color.g, _levelMidgMats[i].color.b, iI.color.a);
        //    //    }
        //    //}
        //}
    }
}
