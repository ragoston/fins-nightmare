﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.CodeDom;

namespace Assets.MainMenuStuff
{
    public class Confirm : MonoBehaviour
    {
        [SerializeField]
        private Button confirm;
        [SerializeField]
        private Button deny;

        //public UnityEvent CONFIRM;
        //public UnityEvent DENY;

        //private UnityAction ConfirmAction;
        //private UnityAction DenyAction;
        //public void Awake()
        //{
        //    //confirm.onClick.AddListener(ConfirmAction);
        //    //deny.onClick.AddListener(DenyAction);

        //    //compile input

        //}
        public void OnConfirm()
        {
#if UNITY_EDITOR
            Debug.Log("Quit.");
#else
            Application.Quit();
#endif

        }

    }
}
