﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.MainMenuStuff
{
    public class CustomAttributes
    {
    }
    public class FirstToInvokeOnInitAttribute : Attribute { }
    public class InvocationOrderAttribute : Attribute , IComparable
    {
        public int order;
        public InvocationOrderAttribute(int order)
        {
            this.order = order;
        }

        public int CompareTo(object obj)
        {
            return this.order.CompareTo((obj as InvocationOrderAttribute).order);
        }
    }
}
