﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Assets.MainMenuStuff.DataStructures
{
    public class CircularList<T> : List<T>
    {
        public new T this[int i]
        {
            get
            {
                if (base.Count == 0) return default(T);
                while(i < 0)
                {
                    i += base.Count;
                }
                return base[i % base.Count];
            }
        }
        public IEnumerable<T> ToEnumerable()
        {
            foreach (var item in this)
                yield return item;           
        }
        public void ReadEnumerable(IEnumerable<T> collection)
        {
            if(collection != null)
            {
                foreach (var item in collection)
                    base.Add(item);
            }else
            {
#if DEBUG
                UnityEngine.Debug.Log("input collection is null.");
#endif
                
            }

        }
    }
}
