﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.MainMenuStuff.DataStructures
{
    //class EventLinkedList<T> : LinkedList<T>
    //    where T : class
    //{
    //    public void LaunchConsecutive()
    //    {
    //        First.Value.Start();
    //    }
    //    public new void AddLast(T item)
    //    {
    //        base.Last.Next = new LinkedListNode<T>(item);
    //    }
    //}
    class Instruction<T> : IChainable
    {
        private Func<bool> cont;
        private Func<IEnumerator<T>> body;
        private Instruction<T> next;

        public Instruction(Func<bool> cont, 
            Func<IEnumerator<T>> body, Instruction<T> next)
        {
            this.cont = cont;
            this.body = body;
            this.next = next;
        }
        public void Start()
        {
            while (cont())
            {
                body();
            }
            if (next != null) next.Start();
        }
    }
}
