﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.MainMenuStuff
{
    public class SafeList<T> : List<T>
    {
        public new void Add(T item)
        {
            if (!base.Contains(item))
                base.Add(item);
        }
        public void AddIEnumerable(IEnumerable<T> ienum)
        {
            foreach (var item in ienum)
            {
                base.Add(item);
            }
        }
    }
}
