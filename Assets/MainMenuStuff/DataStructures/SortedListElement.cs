﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.MainMenuStuff.DataStructures
{
    class SortedListElement<Tkey, TValue>
    {
        public Tkey Key { get; set; }
        public TValue Value { get; set; }
        public SortedListElement(Tkey Key, TValue Value)
        {
            this.Key = Key;
            this.Value = Value;
        }
    }
}
