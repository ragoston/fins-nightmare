﻿using Assets.MainMenuStuff.DataStructures;
using Assets.Scripts.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.MainMenuStuff
{
    /// <summary>
    /// Functionality to display level data. 
    /// It holds logic, not data.
    /// </summary>
    public class DisplayLevelData
    {
        /// <summary>
        /// Check if level data is up to date or was modified by the developer. 
        /// All the data has to be comparent to the input relevant data.
        /// </summary>
        /// <param name="levelNumbers"> input from the editor. this is the relevant data</param>
        /// <param name="levelNames"> input from the editor. relevant data.</param>
        /// <param name="data"> the loaded data that has to be checked if something is missing.</param>
        /// <param name="saver"> functionality to save the data if it was modified.</param>
        /// <returns></returns>
        private List<LevelResult> CorrectLevelDatas(int[] levelNumbers,
            string[] levelNames, DataBank data, ISaver saver)
        {
            //but it won't be, checked in Persistence.
            if (data == null) return null;
            //but it won't be either.
            if (data.MMTools.StatisticsUtility.GetLevelDatas == null) return null;
            for (int i = 0; i < levelNumbers.Length; i++)
            {
                try
                {
                    if (data.Stats.levelIdentifiers[i].Name != levelNames[i])
                    {
                        data.Stats.levelIdentifiers[i].Name = levelNames[i];
                    }
                    if (data.Stats.levelIdentifiers[i].LevelNumber != levelNumbers[i])
                    {
                        data.Stats.levelIdentifiers[i].LevelNumber = levelNumbers[i];
                    }
                }
                catch
                {
                    //then new level.
                    data.Stats.levelIdentifiers.Add(new LevelIdentifiers(levelNames[i], levelNumbers[i]));
                }
            }
            //saving the modified data.
            saver.Save(data.Stats);
            return data.MMTools.StatisticsUtility.GetLevelDatas;
        }
        public void SetTextValue(Text t, string value)
        {
            t.text = value;
        }
        public void ActivateOnlyOneOfThem(IEnumerable<GameObject> objs,
            GameObject active)
        {
            foreach (var item in objs)
            {
                if(item == null)
                {
                    throw new NullReferenceException(item + " was null when trying to deactivate it.");
                }
                if (item.activeSelf) item.SetActive(false);
            }
            if(active == null)
            {
                throw new NullReferenceException(active + " was null when trying to activate it.");
            }
            active.SetActive(true);
        }
        public void ActivateOnlyOneOfThem(CircularList<GameObject> indexableOBJs,
            int indexOfObjToActivate)
        {
            for (int i = 0; i < indexableOBJs.Count; i++)
            {
                if (indexableOBJs[i] == null)
                {
                    throw new NullReferenceException(indexableOBJs[i] + " was null when trying to deactivate it.");
                }
                if (indexableOBJs[i].activeSelf) indexableOBJs[i].SetActive(false);
            }
            if(indexableOBJs[indexOfObjToActivate] == null)
            {
                throw new NullReferenceException(indexableOBJs[indexOfObjToActivate]
                    + " was null when trying to activate it.");
            }
            indexableOBJs[indexOfObjToActivate].SetActive(true);
        }
        public void SetImageFill(Image img, float fill)
        {
            img.fillAmount = fill;
        }
        public void DoRecursivelyInGameObject(Action<GameObject> whatToDo, GameObject obj)
        {
            whatToDo(obj);
            foreach (Transform child in obj.transform)
            {
                DoRecursivelyInGameObject(whatToDo, child.gameObject);
            }
        }
        /// <summary>
        /// Giving the camera a random target of a collection of targets without repeat oneself.
        /// </summary>
        /// <param name="camMove"></param>
        /// <param name="target"></param>
        public void SetCamRandomTarget(CameraMovement camMove, List<Transform> targets)
        {
            int i = UnityEngine.Random.Range(0, targets.Count);
            if(targets[i] == camMove.target)
            {
                i++;
                if (i >= targets.Count) i = 0;
            }
            camMove.target = targets[i];
        }
        public int NormalizeOverIndex(int index, int start, int end)
        {
            /*
             * e.g. from 1 to 6. we have 8: need 2.
             */

            int diff = end - start + 1;
            while (index <= 0)
            {
                index += diff;
            }
            while (index > end)
            {
                index -= end;
            }
            return index;
        }
        public List<LevelIdentifiers> CheckFileIntegrity(DataBank bank,
            string[] levelNames, int[] levelNumbers)
        {
            //but it won't be, checked in Persistence.
            if (bank.Stats == null) return null;
            //but it won't be either.
            if (bank.Stats.levelIdentifiers == null) return null;
            for (int i = 0; i < levelNumbers.Length; i++)
            {
                try
                {
                    if (bank.Stats.levelIdentifiers[i].Name != levelNames[i])
                    {
                        bank.Stats.levelIdentifiers[i].Name = levelNames[i];
                    }
                    if (bank.Stats.levelIdentifiers[i].LevelNumber != levelNumbers[i])
                    {
                        bank.Stats.levelIdentifiers[i].LevelNumber = levelNumbers[i];
                    }
                    //if we were missing a level identifier, we would already have left the try until now.
                    if(bank.Stats.originalLevelResults[i] == null 
                        || bank.Stats.assistedLevelResults[i] == null)
                    {
                        bank.MMTools.StatisticsUtility
                            .InsertEmptyLevelResult(bank.Stats.levelIdentifiers[i].LevelNumber);
                    }
                }
                catch
                {
                    //then new level.
                    bank.MMTools.StatisticsUtility.AddNewLevel(
                        new LevelIdentifiers(levelNames[i], levelNumbers[i]));
                }
            }
            //saving the modified data.
            bank.Save(JsonUtility.ToJson(bank.Stats),DataBank.STATS_NAME);
            return bank.Stats.levelIdentifiers;
        }
    }
}
