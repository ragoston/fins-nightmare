﻿using UnityEngine;
namespace Assets.MainMenuStuff
{
    public class DontDestroyThisOnLoad : MonoBehaviour
    {
        private static DontDestroyThisOnLoad dont;
        private void Awake()
        {
            if (dont == null)
            {
                DontDestroyOnLoad(gameObject);
                dont = this;
            }
            else if (dont != this)
            {
                Destroy(gameObject);
            }
        }
    }
}
