﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.MainMenuStuff
{
    [RequireComponent(typeof(Renderer))]
    class ElectricPolesSaver : MonoBehaviour
    {
        ElectricPoles el;
        private void Start()
        {
            el = transform.parent.GetComponent<ElectricPoles>();
            if (el != null)
                el.animationEnabled = false;
        }
        private void OnBecameInvisible()
        {
            if(el != null)
            {
                el.animationEnabled = false;
            }
            
        }
        private void OnBecameVisible()
        {
            if(el != null)
            {
                el.animationEnabled = true;
            }
            
        }
    }
}
