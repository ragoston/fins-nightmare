﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.MainMenuStuff
{
    public abstract class ExecuteBroadcastedMessage : MonoBehaviour
    {
        public abstract void EXECUTE(object parameter);
    }
}
