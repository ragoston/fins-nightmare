﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.MainMenuStuff
{
    /// <summary>
    /// Determine the time elapse speed based on screen diagonal size.
    /// The more size, the slower the speed has to get. 
    /// </summary>
    class GameSpeedUtility
    {
        private float minSpeed;
        private float maxSpeed;
        private float ratioTreshold;
        public float MinSpeed { get { return minSpeed; }private set { minSpeed = value; } }
        public float MaxSpeed { get { return maxSpeed; }private set { maxSpeed = value; } }
        public float GetDesiredSpeed
        {
            get { return _GetDesiredSpeed(); }
        }
        public GameSpeedUtility(float minSpeed, float maxSpeed, float ratioTreshold = 1.5f)
        {
            MinSpeed = minSpeed;
            MaxSpeed = maxSpeed;
            this.ratioTreshold = ratioTreshold;
        }
        public GameSpeedUtility()
        {
            MinSpeed = 1.02f;
            MaxSpeed = 1.1f;
            this.ratioTreshold = 1.4f;
        }
        //private float GetDiagonalInches()
        //{
        //    float screenWidth = Screen.width / Screen.dpi;
        //    float screenHeight = Screen.height / Screen.dpi;
        //    return Mathf.Sqrt(Mathf.Pow(screenWidth, 2) + Mathf.Pow(screenHeight, 2));
        //}
        private float _GetDesiredSpeed()
        {
            float ratio = Screen.width / Screen.height;
            if(ratio > ratioTreshold)
            {
                //wide, Phone
                return MaxSpeed;
            }else
            {
                //narrow, Tablet
                return MinSpeed;
            }
        }
        //private float _GetDesiredSpeed()
        //{
        //    float screenHeightInInch = Screen.height / Screen.dpi;
        //    if (screenHeightInInch < 3.1)
        //    {
        //        // it's a phone
        //        return MaxSpeed;
        //    }
        //    else
        //    {
        //        // it's tablet
        //        return MinSpeed;
        //    }
        //}
    }
}
