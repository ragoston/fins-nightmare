﻿using Assets.MainMenuStuff;
using Assets.MainMenuStuff.DataStructures;
using Assets.MainMenuStuff.MMToolkitConsumers;
using Assets.Scripts.Statistics;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HowToPlay : MonoBehaviour
{
    [SerializeField]
    Image howToPlayImage;
    Text howToPlayText;
    ISwipeFunctionality swiper;
    MoveCamAdditional moveCam;
    //CircularList<GameObject> howToPlayCards;
    CircularList<string> messages;
    public HowToPlayPuppetShow puppets;
    public int index = -1;
    INITIALIZER init;
	internal void Initialize (INITIALIZER init)
    {
        this.init = init;
        //howToPlayCards = new CircularList<GameObject>();
        //howToPlayImage = GetComponentInChildren<Image>();
        howToPlayText = GetComponentInChildren<Text>();
        puppets = GameObject.FindGameObjectWithTag("HowToPlayPuppetShow")
            .GetComponent<HowToPlayPuppetShow>();
        if (puppets != null)
            puppets.Initialize(init);
        messages = new CircularList<string>();
        messages.Add("The character moves by itself. Tap the screen to jump.");
        messages.Add("Hold the screen to make the character jump continuously.");
        messages.Add("Trampolines make you jump automatically. Recognize them by the particles floating upwards.");
        messages.Add("If you jump while hitting a trampoline, it makes you jump higher.");
        messages.Add("Sometimes you have to double jump, sometimes you have to avoid doing so.");
        messages.Add("Hitting a trampoline from midair by touching the screen beforehand is an easy way of making a double jump.");
        messages.Add("Bear in mind this isn't the only way. Some trampolines actually wait for your jump to ease up on timing.");
        messages.Add("Some obstacles are lined up with just a jumpful of distance between them.");
        messages.Add("If you mess up the first obstacle, you might fall only later.");
        messages.Add("Some objects scattered throughout the scene kill the player on contact.");
        messages.Add("To help you to decide what's dangerous, use Highlight Danger. It shows you the harmful objects on the field.");
        messages.Add("Note, that while they are not highlighted, hitting walls or falling between obstacles where it's not possible to get out of will still kill the player.");
        messages.Add("You can choose from three game speeds: Slow, Casual or Original.");
        messages.Add("If you find it troublesome to time your jumps, use Jump Assist. It calculates jump force and positions how the player lands to give you more room for mistakes.");
        messages.Add("The game is filled with all kinds of traps. In some cases you have to jump, in some cases you mustn't. Try to avoid them!");
        messages.Add("You have two different high scores: Assisted and Original.");
        messages.Add("You reach a score in Original only if you are not using any assists.");
        //messages.Add("Some objects kill the character on contact. Some just make you tip over them.");
        //messages.Add("If you hit a wall, you bounce off of it. Note that you can tip and hit the wall only three times.");

        swiper = GetComponent(typeof(ISwipeFunctionality))
            as ISwipeFunctionality;
        moveCam = GetComponent<MoveCamAdditional>();
        if(swiper != null)
        {
            swiper.Initialize(SwitchPage, () => { }, swiper.PositionXTresholdToDetermineSwipe
                , swiper.DivisionOfScreen); 
        }

        //SwitchPage(1);
    }

    //private void OnEnable()
    //{
    //    StartCoroutine(OnEnableActions());

    //}
    public IEnumerator OnEnableActions()
    {
        yield return null;
        try
        {
            if (puppets != null)
            {
                puppets.gameObject.SetActive(true);
                index = -1;
                SwitchPage(1);
            }

            DisableMenuBGs();
        }
        catch { }
    }
    //private void OnDisable()
    //{
    //    StartCoroutine(OnDisableActions());       
    //}
    public IEnumerator OnDisableActions()
    {
        yield return null;
        try
        {
            if (puppets != null)
            {
                puppets.gameObject.SetActive(false);

            }
            EnableLastMenuBG();
            index = 0;
        }
        catch { }
    }
    public void SwitchPage(int i)
    {
        if(i >= 0)
        {
            index++;
        }
        else { index--; }
        //howToPlayImage.sprite = howToPlayCards[index];
        if (!puppets.gameObject.activeSelf)
        {
            puppets.gameObject.SetActive(true);
        }
        howToPlayText.text = messages[index];
        puppets.SelectAnother(index);
        moveCam.OnClickAdditional(init);
    }
    private void SelectOnlyOneOfThem(List<GameObject> objs, int index)
    {
        foreach (var item in objs)
        {
            item.SetActive(false);
        }
        objs[index].SetActive(true);
    }
    public void DisableMenuBGs()
    {
        //When the game disables this canvas first, there won't be any levelbackgrounds. 
        //so disabling them doesn't make sense anyway. 
        DisableAll(init.CL_LevelBackgrounds);
        
    }
    public void EnableLastMenuBG()
    {
        SelectOnlyOneOfThem(init.CL_LevelBackgrounds, init.DATA.Stats.LastPlayedLevel - 1);
    }
    private void DisableAll(IEnumerable<GameObject> coll)
    {
        foreach (var item in coll)
        {
            item.SetActive(false);
        }
    }
}
