﻿using Assets.MainMenuStuff.DataStructures;
using Assets.Scripts.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.MainMenuStuff
{
    public class HowToPlayPuppetShow : MonoBehaviour
    {
        public Material[] bgMats;
        public Material[] playergMats;
        public Material[] midgMats;
        public ParticleSystem danger;
        internal CircularList<GameObject> midgObjects = new CircularList<GameObject>();
        internal CircularList<GameObject> bgObjects = new CircularList<GameObject>();
        internal CircularList<GameObject> playergObjects = new CircularList<GameObject>();
        internal GameObject BACKGROUND;
        internal INITIALIZER init;
        internal CircularList<GameObject> puppets = new CircularList<GameObject>();
        internal void Initialize(INITIALIZER init)
        {
            this.init = init;

            midgObjects.ReadEnumerable(GetChildrenOfGameObject(transform.Find("midg")));
            bgObjects.ReadEnumerable(GetChildrenOfGameObject(transform.Find("background")));
            playergObjects.ReadEnumerable(GetChildrenOfGameObject(transform.Find("playerground")));
            BACKGROUND = transform.Find("BG").gameObject;
            GameObject o = null;
            try
            {
                var originalGO = GetChildrenOfGameObject(
                    init.CL_LevelBackgrounds[init.DATA.Stats.LastPlayedLevel - 1].transform)
                    .Where(x => x.CompareTag("MM_BGfar")).FirstOrDefault();
                Vector3 originalPos = originalGO.transform.position;
                o = Instantiate<GameObject>(originalGO);
                BACKGROUND.transform.position = originalPos;

                o.transform.parent = BACKGROUND.transform;
                o.transform.localPosition = Vector3.zero;
            }
            catch { }


            
            puppets.ReadEnumerable(GetChildrenOfGameObject(transform.Find("Puppets")));
            //recolour dem meshes.
            foreach (var item in midgObjects)
            {
                try
                {
                    item.GetComponent<Renderer>().material = midgMats[
                        init.DATA.Stats.LastPlayedLevel - 1];
                }
                catch { }
            }
            foreach (var item in bgObjects)
            {
                try
                {
                    item.GetComponent<Renderer>().material = bgMats[
                        init.DATA.Stats.LastPlayedLevel - 1];
                }
                catch { }
            }
            var m = danger.main;
            m.startColor = init.DATA.outlineColor[init.DATA.Stats.LastPlayedLevel - 1];
            gameObject.SetActive(false);
        }
        IEnumerable<GameObject> GetChildrenOfGameObject(Transform obj)
        {
            foreach (Transform item in obj.transform)
            {
                yield return item.gameObject;
            }
        }
        public void SelectAnother(int index)
        {
            SelectOnlyOneOfThem(puppets,index);
        }
        private void SelectOnlyOneOfThem(CircularList<GameObject> objs, int index)
        {
            foreach (var item in objs)
            {
                item.SetActive(false);
            }
            objs[index].SetActive(true);
        }
    }
}
