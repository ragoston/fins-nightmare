﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IPageNavigator
{
    IEnumerator Appear(UnityEvent follow);
    IEnumerator Disappear(UnityEvent follow);
    void OnNavigate();
}
