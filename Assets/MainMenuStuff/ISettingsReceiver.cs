﻿using Assets.Scripts.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.MainMenuStuff
{
    public interface ISettingsReceiver
    {
        void UpdateDependencies(INITIALIZER init);
    }
}
