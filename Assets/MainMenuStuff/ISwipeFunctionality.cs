﻿using System;

namespace Assets.MainMenuStuff
{
    public interface ISwipeFunctionality
    {
        /// <summary>
        /// Treshold to determine swipe, [pixels]
        /// </summary>
        float PositionXTresholdToDetermineSwipe { get; set; }
        /// <summary>
        /// Screen width / (this value) [pixels] to swipe in order to call it a swipe. 0 means use fix pixel treshold.
        /// </summary>
        float DivisionOfScreen { get; set; }
        /// <summary>
        /// positive is to the right, negative is to the left. 0 is nothing.
        /// </summary>
        Action<int> OnSwiped { get; set; }
        Action OnClicked { get; set; }

        void Initialize(Action<int> OnSwiped, Action OnClicked, float PositionXTresholdToDetermineSwipe,
            float DivisionOfScreen);
    }
}