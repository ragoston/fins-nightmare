﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    [RequireComponent(typeof(ImageSequencePlayer))]
    public class ImageSequenceSaver : MonoBehaviour
    {
        ImageSequencePlayer player;
        private void Start()
        {
            player = GetComponent<ImageSequencePlayer>();
            player.enabled = false;
            player.canIterate = false;
        }
        private void OnBecameInvisible()
        {
            //Debug.Log("IM GONE");
            player.enabled = false;
            player.canIterate = false;
        }
        private void OnBecameVisible()
        {
            //Debug.Log("IM HERE");
            player.canIterate = true;
            player.enabled = true;
        }
    }
}
