﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.MainMenuStuff
{
    /// <summary>
    /// A class that tells the persistent class what it needs to initialize
    /// and in what order. 
    /// </summary>
    public class InitializerIWant : MonoBehaviour
    {
        /// <summary>
        /// There are more classes of such in a scene. 
        /// This tells the global order, meaning the order
        /// between those classes.
        /// </summary>
        [Tooltip("The order between these classes accross the level.")]
        public int globalOrder;
        public string initializerMethodName;
        
    }
}
