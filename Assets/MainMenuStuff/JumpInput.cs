﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.MainMenuStuff
{
    public class JumpInput : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public static bool JUMP;

        private void Start()
        {
            JUMP = false;
        }
        public void OnPointerDown(PointerEventData eventData)
        {
            JUMP = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            JUMP = false;
        }

    }

}
