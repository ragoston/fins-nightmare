﻿using Assets.MainMenuStuff.DataStructures;
using Assets.Scripts.Statistics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.MainMenuStuff
{
    public class LevelSelectHelper : MonoBehaviour
    {
        public ISwipeFunctionality swiper;
        public Text SelectedGameMode;
        public Text levelName;
        public Text progress;
        public Text deathCount;
        public Image fill;
        public Image fillBG;
        public Button[] buttons;
        public int Index { get; set; }
        public GameObject parent;
        [HideInInspector]
        public INITIALIZER pers;
        [HideInInspector]
        public CircularList<LevelResult> levelResults;
        [HideInInspector]
        public CircularList<LevelIdentifiers> levelIDs;
        public int[] levelNumbers;
        public string[] levelNames;
        public Action<int> OnTurnTaken;
        public void TakeTurn(int dir)
        {           
            Index += dir;
            
            //SetGivenSkinActive(Index);
            StartCoroutine(_SetGivenSkinActive(Index));
        }
        IEnumerator _SetGivenSkinActive(int level)
        {
            yield return new WaitForEndOfFrame();
            SetGivenSkinActive(level);
        }
        public void SetGivenSkinActive(int level)
        {
            //transform.parent.BroadcastMessage("OnTurnTaken", Index);
            if(OnTurnTaken != null)
            {
                OnTurnTaken(Index);
            }
            pers.DATA.MMTools.DisplayDataTools.ActivateOnlyOneOfThem(
               pers.CL_LevelBackgrounds, level);
            levelName.text = levelIDs[level].Name;
            progress.text = "Progress: " +
                levelResults[level].BestPercent.ToString()
                + "%";
            deathCount.text = "Death count: " + levelResults[level].DeathCount.ToString();

            if (pers.DATA.MMTools.StatisticsUtility.isOriginal(pers.DATA.Stats.GamePlaySettings))
            {
                SelectedGameMode.text = "Game Mode: Original";
            }
            else { SelectedGameMode.text = "Game Mode: Assisted"; }

            pers.DATA.MMTools.DisplayDataTools.SetImageFill(fill, levelResults[level].BestPercent / 100f);
            //recolor
            pers.DATA.MMTools.DisplayDataTools.DoRecursivelyInGameObject((parent) =>
            {
                pers.DATA.MMTools.Colorizer.RecolourUI(parent, pers.DATA.baseColor[level], pers.DATA.baseColor2[level],
                       pers.DATA.outlineColor[level]);
            }, parent);
        }
        public void InitLevelIndex(int v)
        {
            Index = v;
        }
        public void LoadGivenLevel()
        {
            //pers.Stats.LastPlayedLevel = pers.MMTools.DisplayDataTools.NormalizeOverIndex(Index+1,
            //    1,pers.Stats.LevelDatas.Count);
            //pers.Save(pers.Stats);
            //SceneManager.LoadScene(pers.MMTools.DisplayDataTools
            //    .NormalizeOverIndex(Index + 1, 1, pers.Stats.LevelDatas.Count));

            pers.DATA.MMTools.SceneManager.LoadScene(Index + 1);
            //SceneManager.LoadScene(Index+1);
        }
    }
}
