﻿using Assets.Scripts.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.MainMenuStuff
{
    public static class MMExtensions
    {
        public static GameObject InstantiateListElement(this GameObject o, Transform parent
            ,string text, string num, ref int multiplierForContentSize)
        {
            try
            {
                var tot = GameObject.Instantiate<GameObject>(o);
                tot.transform.Find("Text").GetComponent<Text>().text
                    = text;
                tot.transform.SetParent(parent);
                tot.transform.localScale = Vector3.one;
                tot.transform.Find("Num").GetComponent<Text>().text
                    = num;
                multiplierForContentSize++;
                return tot;
            }catch
            {
                throw new Exception("Expected list element for UI but got none.");
            }
        }
        public static GameObject InstantiateSoloElement(this GameObject o, Transform parent
           ,string textToShow, ref int multiplierForContentSize)
        {
            try
            {
                var s1 = GameObject.Instantiate<GameObject>(o);
                s1.transform.Find("Text").GetComponent<Text>()
                    .text = textToShow;
                s1.transform.SetParent(parent);
                s1.transform.localScale = Vector3.one;
                multiplierForContentSize++;
                return s1;
            }
            catch
            {
                throw new Exception("Expected list element for UI but got none.");
            }
        }
    }
}
