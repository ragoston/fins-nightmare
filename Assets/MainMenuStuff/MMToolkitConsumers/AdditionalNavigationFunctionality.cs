﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    [RequireComponent(typeof(UnityEngine.UI.Button))]
    //[RequireComponent(typeof(UInavigation.Origin))]
    public abstract class AdditionalNavigationFunctionality : MonoBehaviour
    {
        public abstract void OnClickAdditional(Scripts.Statistics.INITIALIZER pers);
    }
}
