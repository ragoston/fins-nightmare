﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using System.Collections;
using UnityEngine;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    public class FromHowToPlayAdditional : AdditionalNavigationFunctionality
    {
        public HowToPlay HOWTOPLAY;
        public override void OnClickAdditional(INITIALIZER pers)
        {
            StartCoroutine(OnDisableActions());
        }
        public IEnumerator OnDisableActions()
        {
            yield return new WaitForEndOfFrame();
            try
            {
                if (HOWTOPLAY.puppets != null)
                {
                    HOWTOPLAY.puppets.gameObject.SetActive(false);
                }
                HOWTOPLAY.EnableLastMenuBG();
                HOWTOPLAY.index = 0;
            }
            catch { }

        }
    }
}
