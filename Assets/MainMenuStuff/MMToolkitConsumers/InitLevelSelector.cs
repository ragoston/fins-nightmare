﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using UnityEngine;
using Assets.MainMenuStuff.DataStructures;
using UnityEngine.UI;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    [RequireComponent(typeof(LevelSelectHelper))]
    public class InitLevelSelector : InitializeMe
    {
        [Tooltip("Treshold to determine swipe, [pixels]")]
        public float PositionXTresholdToDetermineSwipe = 0;
        [Tooltip("Screen width / (this value) [pixels] to swipe in order to call it a swipe. 0 means use fix pixel treshold.")]
        public float DivisionOfScreen = 8;
        public override void INIT(INITIALIZER pers)
        {
            var helper = GetComponent<LevelSelectHelper>();
            helper.Index = pers.DATA.Stats.LastPlayedLevel;
            helper.pers = pers;
            helper.levelResults = new CircularList<LevelResult>();
            helper.levelIDs = new CircularList<LevelIdentifiers>();
            pers.DATA.MMTools.DisplayDataTools.CheckFileIntegrity(pers.DATA,
                helper.levelNames, helper.levelNumbers);
            if (pers.DATA.Stats.levelIdentifiers.Count <= 0)
            {
                Debug.Log("Fucked, levelDatas in persistence not yet initialized.");
            }else
            {
                helper.levelResults.ReadEnumerable(
                    pers.DATA.MMTools.StatisticsUtility.GetLevelDatas);
                helper.levelIDs.ReadEnumerable(pers.DATA.MMTools.StatisticsUtility.GetLevelIDs);
            }

            helper.InitLevelIndex(pers.DATA.Stats.LastPlayedLevel-1);
            helper.swiper = GetComponent(typeof(ISwipeFunctionality)) as ISwipeFunctionality;
            if(helper.swiper == null)
            {
                //at least can click and select level
                helper.GetComponent<Button>().onClick.AddListener(helper.LoadGivenLevel);
            }
            else
            {
                helper.swiper.Initialize(helper.TakeTurn, helper.LoadGivenLevel, PositionXTresholdToDetermineSwipe, DivisionOfScreen);
            }           
        }
    }
}
