﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using UnityEngine;
using UnityEngine.PostProcessing;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    [RequireComponent(typeof(SettingsInput))]
    public class InitSettingsSave : InitializeMe
    {
        public override void INIT(INITIALIZER pers)
        {
            var set = GetComponent<SettingsInput>();
            set.valuesToSave = pers.DATA.Stats.Settings;
            if(pers != null && pers.DATA.Stats != null)
            {
                set.valuesToSave.MusicVolume = pers.DATA.Stats.Settings.MusicVolume;
                set.musicBar.value = set.valuesToSave.MusicVolume;
                set.valuesToSave.SFXVolume = pers.DATA.Stats.Settings.SFXVolume;
                set.sfxBar.value = set.valuesToSave.SFXVolume;
                set.valuesToSave.CharacterOutline_value = pers.DATA.Stats.Settings.CharacterOutline_value;
                set.valuesToSave.PostProc_value = pers.DATA.Stats.Settings.PostProc_value;
                set.postProc.isOn = set.valuesToSave.PostProc_value;
                set.characterOutline.isOn = set.valuesToSave.CharacterOutline_value;
            }
            set.ValuesChanged += set.OnValuesChanged;
            set.musicBar.onValueChanged.AddListener(set.ValuesChanged);
            set.sfxBar.onValueChanged.AddListener(set.ValuesChanged);
            set.characterOutline.onValueChanged.AddListener((b) =>
            {
                set.valuesToSave.CharacterOutline_value = b;
            });
            set.postProc.onValueChanged.AddListener((b) =>
            {
                set.valuesToSave.PostProc_value = b;
                try
                {
                    Camera.main.GetComponent<PostProcessingBehaviour>()
                    .enabled = b;
                }
                catch { }
            });
            //set.BROADCAST_DATA += GameObject.FindGameObjectWithTag("GameData")
            //    .GetComponent<Scripts.Statistics.Persistence>().GetSettings;
        }
    }
}
