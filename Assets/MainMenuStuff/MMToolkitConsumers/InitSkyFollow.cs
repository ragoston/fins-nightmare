﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using UnityEngine;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    [RequireComponent(typeof(SkyFollow))]
    public class InitSkyFollow : InitializeMe
    {
        public override void INIT(INITIALIZER pers)
        {
            GetComponent<SkyFollow>().Initialize();
        }
    }
}
