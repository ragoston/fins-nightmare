﻿using Assets.Scripts.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    public class InitStats : InitializeMe
    {
        [SerializeField]
        private GameObject listElement;
        [SerializeField]
        private GameObject soloContent;
        INITIALIZER pers;
        public bool isOriginal = false;
        public Text currentModeText;
        public override void INIT(INITIALIZER pers)
        {
            this.pers = pers;
            FillContent();
        }
        public void FillContent()
        {
            isOriginal = !isOriginal;
            pers.ScrollerFiller.FillViewerWithData(
                pers.DATA, transform, listElement, soloContent, isOriginal);

            //recursively recolour the UI, only the modified list contents.
            pers.DATA.MMTools.DisplayDataTools.DoRecursivelyInGameObject(
                (g) =>
                {
                    if(g.transform.childCount > 0)
                    {
                        foreach (Transform child in g.transform)
                        {
                            pers.DATA.MMTools.Colorizer.RecolourUI(child.gameObject,
                                pers._baseColor[pers.DATA.Stats.LastPlayedLevel - 1],
                                pers._baseColor[pers.DATA.Stats.LastPlayedLevel - 1],
                                pers._outlineColor[pers.DATA.Stats.LastPlayedLevel - 1]);
                        }
                    }

                },gameObject);
            
            if (isOriginal)
            {
                currentModeText.text = "Current game mode: Original";
            }
            else { currentModeText.text = "Current game mode: Assisted"; }
        }
    }
}
