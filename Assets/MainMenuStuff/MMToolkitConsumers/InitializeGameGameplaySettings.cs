﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    public class InitializeGameGameplaySettings : InitializeMe
    {
        Scrollbar sb;
        Text selectedGameMode;
        Toggle highlight;
        Text sb_text;
        Toggle jumpAssist;
        string defText = "Game speed: ";
        string[] gameSpeedNames;
        public override void INIT(INITIALIZER pers)
        {
            //getting the texts
            sb = transform.Find("SpeedScroll").GetComponent<Scrollbar>();
            selectedGameMode = transform.parent.Find("SelectedGameModeText")
                .GetComponent<Text>();
            highlight = transform.Find("HighlightDanger").GetComponent<Toggle>();
            jumpAssist = transform.Find("JumpAssist").GetComponent<Toggle>();
            sb_text = transform.Find("GameSpeedText").GetComponent<Text>();


            gameSpeedNames = Enum.GetNames(typeof(GamePlaySettings.GameSpeed));
            sb.numberOfSteps = gameSpeedNames.Length;
            //getting the current slider value.

            highlight.isOn = pers.DATA.Stats.GamePlaySettings.highlights;
            jumpAssist.isOn = pers.DATA.Stats.GamePlaySettings.jumpAssist;
            sb.value = ((int)pers.DATA.Stats.GamePlaySettings.gameSpeed*1f / (gameSpeedNames.Length-1)*1f);
            UpdateGameModeText();
            if (sb_text != null && (int)pers.DATA.Stats.GamePlaySettings.gameSpeed < 3)
            {
                sb_text.text = defText + gameSpeedNames[(int)pers.DATA.Stats.GamePlaySettings.gameSpeed];
            }
            sb.onValueChanged.AddListener((f) =>
            {
                int speed =
                    Mathf.RoundToInt(sb.value * (gameSpeedNames.Length-1));
                pers.DATA.Stats.GamePlaySettings.gameSpeed = (GamePlaySettings.GameSpeed)speed;
                if (sb_text != null)
                {
                    sb_text.text = defText + gameSpeedNames[(int)pers.DATA.Stats.GamePlaySettings.gameSpeed];
                }
                UpdateGameModeText();
#if UNITY_EDITOR
                Debug.Log("Current game speed: " + pers.DATA.Stats.GamePlaySettings.gameSpeed.ToString());
#endif
            });
            highlight.onValueChanged.AddListener((b) =>
            {
                pers.DATA.Stats.GamePlaySettings.highlights = highlight.isOn;
                UpdateGameModeText();
            });
            jumpAssist.onValueChanged.AddListener((b) =>
            {
                pers.DATA.Stats.GamePlaySettings.jumpAssist = jumpAssist.isOn;
                UpdateGameModeText();
            });
        }
        private void UpdateGameModeText()
        {
            if(!highlight.isOn && sb.value >= 1 && !jumpAssist.isOn)
            {
                selectedGameMode.text = "Selected game mode: Original";
            }
            else
            {
                selectedGameMode.text = "Selected game mode: Assisted";
            }
        }
    }
}
