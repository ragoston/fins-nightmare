﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using UnityEngine;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    [RequireComponent(typeof(HowToPlay))]
    public class InitializeHowToPlay : InitializeMe
    {
        public override void INIT(INITIALIZER pers)
        {
            var h = GetComponent<HowToPlay>();
            h.Initialize(pers);
        }
    }
}
