﻿using Assets.Scripts.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    public abstract class InitializeMe : MonoBehaviour
    {
        public int orderNumber;
        public abstract void INIT(INITIALIZER pers);
    }
}
