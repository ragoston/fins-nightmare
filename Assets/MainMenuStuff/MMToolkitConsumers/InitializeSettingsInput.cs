﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using UnityEngine;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    [RequireComponent(typeof(UpdateMusicVolume))]
    public class InitializeSettingsInput : InitializeMe
    {
        public override void INIT(INITIALIZER pers)
        {
            var aSource = 
            GetComponent<UpdateMusicVolume>()
                .source = GameObject.FindGameObjectWithTag("MainCamera")
                .GetComponent<AudioSource>();
            //aSource.volume = pers.DATA.Stats.Settings.MusicVolume;                  
        }
    }
}
