﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using UnityEngine;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    public class MoveCamAdditional : AdditionalNavigationFunctionality
    {
        public override void OnClickAdditional(INITIALIZER pers)
        {
            try
            {
                pers.DATA.MMTools.DisplayDataTools.SetCamRandomTarget(pers.mainCam,
    pers.camTargets.ToList());
            }catch { }

        }
    }
}
