﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using Assets.MainMenuStuff.UInavigation;
using UnityEngine;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    [RequireComponent(typeof(Origin))]
    public class NavInit : InitializeMe
    {
        public INITIALIZER pers;
        public override void INIT(INITIALIZER pers)
        {
            this.pers = pers;
            var origin = GetComponent<Origin>();
            origin.FROM = GetParentWithCanvas(gameObject);
            FindDestination(pers.MainMenuGOs
                .Where(x => x.GetComponent<Destination>() != null), origin);
        }
        public void FindDestination(IEnumerable<GameObject> objs, Origin origin)
        {
            origin.TO = objs.Where(x => x.GetComponent<Destination>() != null
            && x.GetComponent<Destination>().TagToDestination == origin.TagOfDestination).FirstOrDefault();
            //if (origin.TO == null) throw new NullReferenceException("No destination found for origin" + origin.TagOfDestination);
        }
        private GameObject GetParentWithCanvas(GameObject element)
        {
            if (element.GetComponent<Canvas>() != null)
            {
                return element;
            }
            else if (element.GetComponent<Canvas>() == null)
            {
                return GetParentWithCanvas(element.transform.parent.gameObject);
            }
            else return null;
        }
    }
}
