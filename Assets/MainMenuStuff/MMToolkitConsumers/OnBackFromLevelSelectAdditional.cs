﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    public class OnBackFromLevelSelectAdditional : AdditionalNavigationFunctionality
    {
        public LevelSelectHelper helper;
        public override void OnClickAdditional(Scripts.Statistics.INITIALIZER pers)
        {
            try
            {
                helper.SetGivenSkinActive(helper.pers.DATA.Stats.LastPlayedLevel - 1);
            }catch { }
            
        }
    }
}
