﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    public class OnNavAdditional_Save : AdditionalNavigationFunctionality
    {
        public override void OnClickAdditional(INITIALIZER pers)
        {
            
            pers.DATA.Save(UnityEngine.JsonUtility.ToJson(pers.DATA.Stats),
                DataBank.STATS_NAME);
#if UNITY_EDITOR
            UnityEngine.Debug.Log("Saved settings.");
#endif
        }
    }
}
