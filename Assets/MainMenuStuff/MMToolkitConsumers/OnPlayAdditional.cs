﻿using Assets.Scripts.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    public class OnPlayAdditional : AdditionalNavigationFunctionality
    {
        public LevelSelectHelper nextWindow_helper;
        public override void OnClickAdditional(INITIALIZER pers)
        {
            //basically push in the new values into the UI elements.
            try
            {
                nextWindow_helper.SetGivenSkinActive(nextWindow_helper.pers.DATA.Stats.LastPlayedLevel - 1);
                //nextWindow_helper.TakeTurn(1);
                //also, we have to load the given game mode's stats. 
                nextWindow_helper.levelResults = null;
                nextWindow_helper.levelResults = new DataStructures.CircularList<LevelResult>();
                if (pers.DATA.MMTools.StatisticsUtility.isOriginal(pers.DATA.Stats.GamePlaySettings))
                {
                    nextWindow_helper.levelResults.ReadEnumerable(pers.DATA.Stats.originalLevelResults);
                }
                else
                {
                    nextWindow_helper.levelResults.ReadEnumerable(pers.DATA.Stats.assistedLevelResults);
                }
            }
            catch
            {

            }
            
        }
    }
}
