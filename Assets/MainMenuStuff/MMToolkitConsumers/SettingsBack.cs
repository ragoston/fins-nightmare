﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using UnityEngine;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    [RequireComponent(typeof(SettingsInput))]
    public class SettingsBack : AdditionalNavigationFunctionality
    {
        public override void OnClickAdditional(INITIALIZER pers)
        {
            try
            {
                var set = GetComponent<SettingsInput>().valuesToSave;
                pers.DATA.Stats.Settings = set;
                pers.DATA.Save(JsonUtility.ToJson(pers.DATA.Stats), DataBank.STATS_NAME);
                //foreach (var sr in pers.settingsReceivers)
                //{
                //    sr.UpdateDependencies(pers);
                //}
            }catch
            {
                Debug.Log("SAVE UNSUCCESSFUL");
            }

        }
    }
}
