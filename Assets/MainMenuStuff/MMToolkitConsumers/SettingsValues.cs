﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    [Serializable]
    public class SettingsValues
    {
        public bool PostProc_value;
        public bool CharacterOutline_value;
        public float MusicVolume;
        public float SFXVolume;
        public SettingsValues()
        {
            PostProc_value = true;
            CharacterOutline_value = true;
            MusicVolume = 1f;
            SFXVolume = 1f;
        }
    }
}
