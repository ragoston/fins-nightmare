﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using System.Collections;
using UnityEngine;

namespace Assets.MainMenuStuff.MMToolkitConsumers
{
    public class ToHowToPlayAdditional : AdditionalNavigationFunctionality
    {
        public HowToPlay HOWTOPLAY;
        public override void OnClickAdditional(INITIALIZER pers)
        {
            StartCoroutine(OnEnableActions());
        }
        public IEnumerator OnEnableActions()
        {
            yield return new WaitForEndOfFrame();
            try
            {
                if (HOWTOPLAY.puppets != null)
                {
                    HOWTOPLAY.puppets.gameObject.SetActive(true);
                    HOWTOPLAY.index = -1;
                    HOWTOPLAY.SwitchPage(1);
                }

                HOWTOPLAY.DisableMenuBGs();

            }
            catch { }
            
        }
    }
}
