﻿using Assets.Scripts.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.MainMenuStuff.Prefabs
{
    /// <summary>
    /// Functionality to load a given scrollviewer with some data.
    /// Has to be called beforehand to colorizer. That's the last.
    /// </summary>
    /// 
    public class FillListContent
    {
        public delegate void Generated(GameObject go);
        public event Generated GET_GENERATED;
        public FillListContent(INITIALIZER pers)
        {
            GET_GENERATED += pers.GetGeneratedObjects;
        }
        ///// <summary>
        ///// A list element with a text and a number (that is a text, but will
        ///// show a number).
        ///// </summary>
        //public GameObject listElement;
        ///// <summary>
        ///// One text, layout in the center.
        ///// </summary>
        //public GameObject soloContent;
        //public delegate void DisplayUpdater();
        //public event DisplayUpdater UPDATE_DISPLAY;
        //List<LevelData> toDisplay;
        //public List<LevelData> ToDisplay
        //{
        //    get { return toDisplay; }
        //    set
        //    {
        //        toDisplay = value;
        //        if(UPDATE_DISPLAY != null)
        //        {
        //            UPDATE_DISPLAY();
        //        }
        //    }
        //}

        //private void Awake()
        //{
        //    Persistence.LOAD_DEPENDENCIES += LoadPrefs;
        //}
        //[InvocationOrder(1)]
        //private void LoadPrefs(Statistics data, Persistence pers)
        //{
        //    /*
        //     * get data to list
        //     * instantiate the content objects
        //     * and parent them under this.
        //     */
        //    //ToDisplay = data.LevelDatas;

        //    //Total death count: x
        //    if(data.LevelDatas.Count <= 0)
        //    {
        //        int shit = 0;
        //        listElement.InstantiateListElement(transform, "There's nothing to show yet. Go and play!",
        //            data.LevelDatas.Sum(x => x.DeathCount).ToString(),ref shit);
        //    }
        //    else
        //    {
        //        var scale = LoadWithActualData(data);
        //        RescaleContent(scale);
        //    }
        //}
        /// <summary>
        /// Fills GO with data. 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="viewer">The scrollviewer to fill data with.</param>
        public void FillViewerWithData(DataBank data, Transform viewer,
            GameObject listElement, GameObject soloContent, bool isOriginal)
        {
            //first remove everything, so all children
            foreach (Transform item in viewer)
            {
                INITIALIZER.Destroy(item.gameObject);
            }
            if (data.MMTools.StatisticsUtility._GetLevelDatas(isOriginal).Count <= 0)
            {
                int shit = 0;
                listElement.InstantiateListElement(viewer, "There's nothing to show yet. Go and play!",
                    data.MMTools.StatisticsUtility._GetLevelDatas(isOriginal).Sum(x => x.DeathCount).ToString(), ref shit);
            }
            else
            {
                var scale = LoadWithActualData(data, viewer, listElement, soloContent, isOriginal);
                var RectContainer = viewer.GetComponent<RectTransform>();
                //RescaleContent(scale, RectContainer);
            }
        }
        private int LoadWithActualData(DataBank data, Transform viewer,
            GameObject listElement, GameObject soloContent, bool isOriginal)
        {
            int multiplier = 0;
            var goOut = listElement.InstantiateListElement(viewer, "Total death count:",
                 data.MMTools.StatisticsUtility._GetLevelDatas(isOriginal).Sum(x => x.DeathCount).ToString(),ref multiplier);
            if (GET_GENERATED != null)
            {
                GET_GENERATED(goOut);
            }
            //Death counts:
            var goOut3 = soloContent.InstantiateSoloElement(viewer, "Death counts:",ref multiplier);
            if (GET_GENERATED != null)
            {
                GET_GENERATED(goOut3);
            }
            foreach (var item in data.MMTools.StatisticsUtility._GetLevelDatas(isOriginal))
            {
                var goOut2 = listElement.InstantiateListElement(viewer, 
                    data.MMTools.StatisticsUtility.GetLevelIDbyLevelNumber(item.levelNumber).Name,
                item.DeathCount.ToString(),ref multiplier);
                if(GET_GENERATED != null)
                {
                    GET_GENERATED(goOut2);
                }
            }
            var goOut4 = soloContent.InstantiateSoloElement(viewer, "Best results:",ref multiplier);
            if (GET_GENERATED != null)
            {
                GET_GENERATED(goOut4);
            }
            foreach (var item in data.MMTools.StatisticsUtility._GetLevelDatas(isOriginal))
            {
                var goOut2 = listElement.InstantiateListElement(viewer, 
                    data.MMTools.StatisticsUtility.GetLevelIDbyLevelNumber(item.levelNumber).Name,
                item.BestPercent.ToString()+"%", ref multiplier);
                if (GET_GENERATED != null)
                {
                    GET_GENERATED(goOut2);
                }
            }
            return multiplier;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="multiplier"></param>
        /// <param name="transformOfContainer"> the rect transform of the container
        /// we will resize so we can scroll it. The content of scrollviewer.</param>
        private void RescaleContent(int multiplier, RectTransform transformOfContainer)
        {
            transformOfContainer.sizeDelta = 
                new Vector2(0, transformOfContainer.rect.height * multiplier);
        }
    }
}
