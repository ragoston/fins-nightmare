﻿using UnityEngine;
public class SerialTag : MonoBehaviour
{
    [Tooltip("The serial for the other UI elements to refer to.")]
    public new int tag;
    private static int _tag;
    public static int _Tag { get { return _tag; } }
    private static int GiveTag() { return ++_tag; }
    [ExecuteInEditMode]
    private void Reset() {tag = GiveTag(); }
}
