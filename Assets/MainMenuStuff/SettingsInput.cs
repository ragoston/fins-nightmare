﻿using Assets.MainMenuStuff.MMToolkitConsumers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.MainMenuStuff
{
    public class SettingsInput : MonoBehaviour
    {
        [HideInInspector]
        public UnityAction<float> ValuesChanged;
        public SettingsValues valuesToSave;
        public Scrollbar musicBar;
        public Scrollbar sfxBar;
        public Toggle postProc;
        public Toggle characterOutline;
        private AudioSource mainMenuMusic;

        private void Awake()
        {
            mainMenuMusic = Camera.main.GetComponent<AudioSource>();

        }
        public void OnValuesChanged(float val)
        {
            valuesToSave.MusicVolume = musicBar.value;
            valuesToSave.SFXVolume = sfxBar.value;
            if (mainMenuMusic != null)
            {
                mainMenuMusic.volume = valuesToSave.MusicVolume;
            }
            
            //            if (BROADCAST_DATA != null)
            //            {
            //                BROADCAST_DATA(this);
#if DEBUG
            Debug.Log("Broadcasting settings.");
#endif
            //            }
            //now updating the main menu music.
            //one time only, just here.
            //BroadcastMessage("EXECUTE", musicBar.value);


        }
        //public void OnCharOutlineChanged()
        //{
        //    characterOutline_value = characterOutline.che
        //}
        //public void OnPostProcChanged()
        //{

        //}
    }
}
