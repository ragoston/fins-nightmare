﻿using Assets.Scripts.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.MainMenuStuff
{
    /// <summary>
    /// Due to different game modes we need a solution to easily know
    /// which mode we are using.
    /// </summary>
    public class StatisticsUtility
    {
        Statistics_v2 stats;
        /// <summary>
        /// Original = no assist whatsoever.
        /// </summary>
        public Predicate<GamePlaySettings> isOriginal;
        public enum GameType { Assisted, Original}
        public StatisticsUtility(Predicate<GamePlaySettings> pred)
        {
            this.isOriginal = pred;
        }
        public List<LevelResult> _GetLevelDatas(bool isOriginal)
        {
            if (isOriginal)
            {
                return Stats.originalLevelResults;
            }
            else { return Stats.assistedLevelResults; }
        }
        public List<LevelIdentifiers> GetLevelIDs
        {
            get { return Stats.levelIdentifiers; }
        }
        public LevelIdentifiers GetLevelIDbyLevelNumber(int i)
        {
            foreach (var item in Stats.levelIdentifiers)
            {
                if (item.LevelNumber == i)
                    return item;
            }
            return null;
        }
        public List<LevelResult> GetLevelDatas
        {
            get
            {
                if (isOriginal(Stats.GamePlaySettings))
                {
                    return Stats.originalLevelResults;
                }
                else { return Stats.assistedLevelResults; }
            }
        }

        public Statistics_v2 Stats
        {
            get
            {
                if(stats == null)
                {
                    throw new NullReferenceException("STATS UTILITY HAS NO STATISTICS!!");
                }
                return stats;
            }

            set
            {
                stats = value;
            }
        }

        public void AddNewLevel(LevelIdentifiers levelID)
        {
            List<int> nums = new List<int>();
            foreach (var item in Stats.levelIdentifiers)
            {
                nums.Add(item.LevelNumber);
            }
            if (!nums.Contains(levelID.LevelNumber))
            {
                //so it's new to levelIndentifier as well, not just level data.
                Stats.levelIdentifiers.Add(levelID);
            }
            Stats.assistedLevelResults.Add(new LevelResult(levelID.LevelNumber));
            Stats.originalLevelResults.Add(new LevelResult(levelID.LevelNumber));
        }
        public void InsertEmptyLevelResult(int levelIndex)
        {
            Stats.assistedLevelResults.Add(new LevelResult(levelIndex));
            Stats.originalLevelResults.Add(new LevelResult(levelIndex));
        }
    }
}
