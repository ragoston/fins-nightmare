﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.MainMenuStuff
{
    public class SwipeUtility : MonoBehaviour, IPointerDownHandler, IPointerUpHandler,
        /*IPointerClickHandler,*/ ISwipeFunctionality
    {

        /// <summary>
        /// Treshold to determine swipe, [pixels]
        /// </summary>
        public float PositionXTresholdToDetermineSwipe
        {
            get; set;
        }

        /// <summary>
        /// Screen width / (this value) [pixels] to swipe in order to call it a swipe. 0 means use fix pixel treshold.
        /// </summary>
        public float DivisionOfScreen { get; set; }
        Vector2 oldPos;
        Vector2 newPos;
        public Action<int> OnSwiped { get; set; }
        public Action OnClicked { get; set; }

        public SwipeUtility(Action<int> OnSwiped, Action OnClicked,
            float PositionXTresholdToDetermineSwipe, float DivisionOfScreen)
        {
            this.OnSwiped = OnSwiped;
            this.OnClicked = OnClicked;
        }
        public void OnPointerDown(PointerEventData eventData)
        {
            oldPos = eventData.position;
        }
        public void OnPointerUp(PointerEventData eventData)
        {
            newPos = eventData.position;
            if(newPos.x > oldPos.x + GetTreshold())
            {
                //NEED THE OPPOSITE DIRECTION!!
                //swiped right
                if(OnSwiped != null)
                {
                    OnSwiped.Invoke(-1);
                }
            }
            else if(newPos.x < oldPos.x - GetTreshold())
            {
                //swiped left

                if (OnSwiped != null)
                {
                    OnSwiped.Invoke(1);
                }
            }
            else
            {
                if(OnClicked != null)
                {
                    OnClicked.Invoke();
                }
            }
        }
        /// <summary>
        /// Get the treshold to differentiate swipe and click.
        /// </summary>
        /// <returns></returns>
        float GetTreshold()
        {
            if(DivisionOfScreen <= 0)
            {
                if(PositionXTresholdToDetermineSwipe > 20)
                {
                    return Mathf.Abs(PositionXTresholdToDetermineSwipe);
                }
                else { return 30; }
                
            }
            else
            {
                return Mathf.Abs(Screen.width / DivisionOfScreen);
            }
        }

        //public void OnPointerClick(PointerEventData eventData)
        //{
        //    if(OnClicked != null)
        //    {
        //        OnClicked.Invoke();
        //    }
        //}

        public void Initialize(Action<int> OnSwiped, Action OnClicked, 
            float PositionXTresholdToDetermineSwipe, float DivisionOfScreen)
        {
            this.OnSwiped += OnSwiped;
            this.OnClicked += OnClicked;
            this.PositionXTresholdToDetermineSwipe = PositionXTresholdToDetermineSwipe;
            this.DivisionOfScreen = DivisionOfScreen;
        }
    }
}
