﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.MainMenuStuff.Threading
{
    public class RecolorUIElements_JOB : ThreadedJob
    {
        public IEnumerator<GameObject> elements;
        public int actionInput;
        public Action<int> colorizeElementsAction;
        public RecolorUIElements_JOB(int actionInput, Action<int> colorizeElementsAction)
        {
            this.colorizeElementsAction = colorizeElementsAction;
            this.actionInput = actionInput;
        }
        protected override void ThreadFunction()
        {
            colorizeElementsAction(actionInput);
        }

        protected override void OnFinished()
        {
            
        }
    }
}
