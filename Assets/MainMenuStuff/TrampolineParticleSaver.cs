﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    [RequireComponent(typeof(ParticleSystem))]
    public class TrampolineParticleSaver : MonoBehaviour
    {
        ParticleSystem sys;
        private void Start()
        {
            sys = GetComponent<ParticleSystem>();
            if(sys != null)
            {
                sys.Stop();
            }

        }
        private void OnBecameVisible()
        {
            //Debug.Log("IM HERE");
            if(sys != null)
                sys.Play();
        }
        private void OnBecameInvisible()
        {
            //Debug.Log("IM GONE");
            if(sys != null)
                sys.Stop();
        }
    }
}
