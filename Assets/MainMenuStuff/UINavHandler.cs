﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;
using UnityEngine.Events;
using System.Linq;
using Assets.MainMenuStuff;
using Assets.MainMenuStuff.DataStructures;
using System;
using Assets.Scripts.Statistics;

[RequireComponent(typeof(CanvasGroup))]
public class UINavHandler : MonoBehaviour
{
    public int linkTagHere;
    [HideInInspector]
    public UINavHandler parent;

    private static SafeList<UINavHandler> pagesToLoad =
        new SafeList<UINavHandler>();
    public static SafeList<UINavHandler> PagesToLoad
    {
        get { return pagesToLoad; }
        set { pagesToLoad = value as SafeList<UINavHandler>; }
    }

    public float animOverTime = 0.4f;
    [HideInInspector]
    public CanvasGroup cg;
    private UINavigation[] navs;
    private void Awake()
    {
        cg = GetComponent<CanvasGroup>();
        navs = GetComponentsInChildren<UINavigation>();
        if (PagesToLoad.Count <= 0)
        {
            PagesToLoad.AddIEnumerable(GetGameObjectsWithComponent<UINavHandler>());
        }

        for (int i = 0; i < navs.Length; i++)
        {
            navs[i].BroadcastClick += _OnNavigate;
        }
        foreach (var c in 
            GetComponentsInChildren<UINavHandler>())
        {
            c.parent = this;
            if (c.gameObject.tag != "FirstMenu")
                c.cg.alpha = 0f;
        }

        //DisableGos();
    }
    private void Start()
    {
        StartCoroutine(DisableGos());
    }
    private IEnumerator DisableGos()
    {
        yield return new WaitForEndOfFrame();
        if(PagesToLoad != null)
        {
            foreach (var item in PagesToLoad)
            {
                if(item != null && item.gameObject.tag != "FirstMenu")
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
        //foreach (var item in PagesToLoad
        //    .Where(p => p.gameObject.tag != "FirstMenu"))
        //{
        //    item.gameObject.SetActive(false);
        //}
    }
    //private void Start()
    //{
    //    foreach (var item in PagesToLoad
    //        .Where(p => p.gameObject.tag != "FirstMenu"))
    //    {
    //        item.gameObject.SetActive(false);
    //    }
    //}
    //public void Appear()
    //{

    //}
    public void SetThisActive()
    {
        cg.alpha = 0f;
        gameObject.SetActive(true);
//#if DEBUG
//        Debug.Log(gameObject.name + " is acive.");
//#endif
        //Persistence.playerSettings.colorize.RecolourUI(Persistence.playerSettings.levelData.Index+1);
        //Persistence.persistence.levelData.Init(Persistence.persistence.levelData.data);
        AlphaAction fadein = new AlphaAction(cg, 0f, 1f, animOverTime);
        fadein.LerpAlpha(this);
    }
    public void SetThisInactive(UINavigation sender)
    {
        var page = PagesToLoad.Where(p => p.linkTagHere == sender.Tag.tag).ToList();
        if (page == null || page.Count <= 0) { Debug.Log("No page of such just yet."); return; }

        AlphaAction fadeOut = new AlphaAction(cg, 1f, 0f, animOverTime);
        fadeOut.NEXT_FUNCTION += (x) =>
        {
            page.First().parent.SetThisActive();
            gameObject.SetActive(false);
        };
        fadeOut.LerpAlpha(this);
    }
    private void _OnNavigate(UINavigation sender)
    {
        ////this will be called on click.
        //var page = PagesToLoad.Where(p => p.linkTagHere == sender.Tag.tag).ToList();
        //if (page == null || page.Count <= 0) { Debug.Log("No page of such just yet."); return; }

        //AlphaAction fadeOut = new AlphaAction(cg, 1f, 0f, animOverTime);
        //AlphaAction nextFadeIn = new AlphaAction(sender.parent.cg, 0f, 1f, sender.parent.animOverTime);
        //fadeOut.NEXT_FUNCTION += (x) =>
        //{
        //    page.First().gameObject.SetActive(true);
        //    gameObject.SetActive(false);
        //};
        ////page.First().gameObject.SetActive(true);
        ////gameObject.SetActive(false);
        //fadeOut.LerpAlpha(this);
        SetThisInactive(sender);
    }

    IEnumerator alphaRoutine = null;
    public IEnumerator AlphaRoutine
    {
        get { return alphaRoutine; }
    }
    private IEnumerable<T> GetGameObjectsWithComponent<T>()
        where T : Component
    {
        foreach (var item in GameObject.FindObjectsOfType<GameObject>()
            .Where(g => g.GetComponent<T>() != null).AsEnumerable())
        {
            yield return item.GetComponent<T>();
        }
    }
    ///// <summary>
    ///// Lerping a UnityEngine.CanvasGroup's alpha over time, from the
    ///// desired scale to the desired scale.
    ///// </summary>
    ///// <param name="cg"></param>
    ///// <param name="start"></param>
    ///// <param name="end"></param>
    ///// <param name="time"></param>
    //public void LerpAlpha(MonoBehaviour mono, 
    //    float start, float end, float time, UnityAction follow)
    //{
    //    if (alphaRoutine != null)
    //    {
    //        mono.StopCoroutine(alphaRoutine);
    //    }
    //    alphaRoutine = _LerpAlpha(cg, start, end, time, mono, follow);
    //    mono.StartCoroutine(alphaRoutine);
    //}
    //IEnumerator _LerpAlpha(CanvasGroup cg, float start, float end, float time,
    //    MonoBehaviour mono, UnityAction follow)
    //{
    //    float lastTime = Time.unscaledTime;
    //    float timer = 0.0f;

    //    while (timer < time)
    //    {
    //        cg.alpha = Mathf.Lerp(start, end, timer / time);
    //        timer += (Time.unscaledTime - lastTime);
    //        lastTime = Time.unscaledTime;
    //        yield return null;
    //    }
    //    cg.alpha = end;
    //    if (follow != null)
    //        follow();
    //}
    //public void Disappear()
    //{
    //    var alpha = new AlphaAction(cg, 1f, 0f, animOverTime);
    //}
}