﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(SerialTag))]
[RequireComponent(typeof(Button))]
public class UINavigation : MonoBehaviour
{
    public delegate void NavigateAction();
    public event NavigateAction NAVIGATE;
    private UnityAction Navigate;
    public UnityAction<UINavigation> BroadcastClick;
    private new SerialTag tag;
    public SerialTag Tag { get { return tag; } }
    private void Awake()
    {
        tag = GetComponent<SerialTag>();
        Navigate += OnNavigate;
        GetComponent<Button>().onClick.AddListener(Navigate);

        ////lerp camera
        //Navigate += Assets.Scripts.CamTargetSwitcher.Instance.ChangeTarget;
    }
    private void OnNavigate()
    {
        if (BroadcastClick != null)
            BroadcastClick(this);
        if (NAVIGATE != null)
        {
            NAVIGATE();
        }
    }
}