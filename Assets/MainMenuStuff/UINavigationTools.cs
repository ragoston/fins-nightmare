﻿using Assets.MainMenuStuff.UInavigation;
using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.MainMenuStuff
{
    /// <summary>
    /// Logic to navigate between UI pages.
    /// </summary>
    public class UINavigationTools
    {
        private MonoBehaviour mono;
        public UINavigationTools(MonoBehaviour mono)
        {
            this.mono = mono;
        }
        public IEnumerator WaitThenDisableGOs(IEnumerable<GameObject> objs,
            YieldInstruction waiter)
        {
            yield return waiter;
            foreach (var item in objs)
            {
                if(item != null && item.activeSelf)
                {
                    item.SetActive(false);
                }
            }
        }
        public IEnumerator _OnTurnLevelSelect(int index)
        {
            yield return new WaitForEndOfFrame();
            /*
             * 1. update the ui: namely the death count, the best percent and the name of level.
             * 2. recolor everything on the screen.
             */ 
            

        }
        public void OnNavigate(Origin origin)
        {
            try
            {
                _OnNavigate(origin.FROM, origin.TO, 0.2f, 0.2f, mono);
            }catch
            {
                origin.TO.SetActive(true);
                origin.FROM.SetActive(false);
            }
            
        }
        /// <summary>
        /// Navigating from from to to, after given fade time to
        /// both of them. for now, only fade is available due to
        /// lack of capability from UIToolkit. 
        /// FROM AND TO REQUIRE CANVASGROUPS.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="fadeTimeOfFrom"></param>
        /// <param name="fadeTimeOfTo"></param>
        public void _OnNavigate(GameObject from,
            GameObject to, float fadeTimeOfFrom, float fadeTimeOfTo,
            MonoBehaviour mono)
        {
            var fromCG = from.GetComponent<CanvasGroup>();
            if (fromCG == null) return;
            AlphaAction fromA = new AlphaAction(fromCG,
                fromCG.alpha, 0, fadeTimeOfFrom);

            var toCG = to.GetComponent<CanvasGroup>();
            if (toCG == null) return;
            AlphaAction toA = new AlphaAction(toCG, 0f, 1f, fadeTimeOfTo);

            /*
             * fade first out.
             * at the end set is inactive. 
             * set second active.
             * fade that in.
             */

            fromA.NEXT_FUNCTION += (x) =>
            {

                toCG.alpha = 0;
                to.SetActive(true);
                from.SetActive(false);
                toCG.blocksRaycasts = true;
                toCG.interactable = true;
                toA.LerpAlpha(mono);
            };
            toA.NEXT_FUNCTION += (x) =>
            {
                try
                {
                    toCG.alpha = 1;
                }
                catch { }
                
            };
            fromCG.interactable = false;
            fromCG.blocksRaycasts = false;
            fromA.LerpAlpha(mono);
        }
    }
}
