﻿using Assets.MainMenuStuff.MMToolkitConsumers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.MainMenuStuff.UInavigation
{
    //[RequireComponent(typeof(MMToolkitConsumers.NavInit))]
    [RequireComponent(typeof(Button))]
    //[RequireComponent(typeof(AdditionalNavigationFunctionality))]
    public class Origin : MonoBehaviour
    {
        public int TagOfDestination;
        public delegate void NavigateAction();
        public event NavigateAction NAVIGATE;
        private UnityAction Navigate;
        public UnityAction<Origin> BroadcastClick;
        [HideInInspector]
        [Tooltip("Parent with canvas, the origin's canvas GO.")]
        public GameObject FROM;
        [HideInInspector][Tooltip("The destination, the canvas GO.")]
        public GameObject TO;
        private void Awake()
        {
            Navigate += OnNavigate;
            GetComponent<Button>().onClick.AddListener(Navigate);
        }
        private void OnNavigate()
        {
            if (BroadcastClick != null)
            {
                try
                {
                    var pers = GetComponent<NavInit>().pers;
                    foreach (var cmd in GetComponents<AdditionalNavigationFunctionality>())
                    {
                        cmd.OnClickAdditional(pers);
                    }
                    //GetComponent<AdditionalNavigationFunctionality>().OnClickAdditional(
                    //    GetComponent<NavInit>().pers);
                    BroadcastClick(this);
                }catch
                {
                    Debug.Log("COULDN'T NAVIGATE");
                }
            }

            if (NAVIGATE != null)
            {
                NAVIGATE();
            }
        }
        
    }
}
