﻿using Assets.Scripts.Statistics;
using Assets.Scripts.Statistics.MapSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.MainMenuStuff
{
    public class UnifiedSceneManager
    {
        private float globalTimeScale;
        public float GlobalTimeScale
        {
            get { return globalTimeScale; }
            set
            {
                globalTimeScale = value;
            }
        }
        public void SetTimeScale(float value)
        {
            Time.timeScale = value;
            Time.fixedDeltaTime = Time.fixedDeltaTime = 0.02f * value;
            if (Time.fixedDeltaTime > 0.02f)
                Time.fixedDeltaTime = 0.02f;
#if UNITY_EDITOR
            Debug.Log("Current timescale: " + Time.timeScale);
            Debug.Log("Current fixed delta time: " + Time.fixedDeltaTime);
#endif
        }
        DataBank bank;
        GameSpeedUtility gameSpeed = new GameSpeedUtility(1.02f,1.08f);
        public UnifiedSceneManager(DataBank bank)
        {
            this.bank = bank;
            globalTimeScale = gameSpeed.GetDesiredSpeed;
        }
        /// <summary>
        /// Unified solution when loading a non mainmenu level.
        /// </summary>
        /// <param name="sceneBuildIndex"></param>
        public void LoadScene(int sceneBuildIndex)
        {
            AdManager.AdManager.DestroyAllAds();
            globalTimeScale = gameSpeed.GetDesiredSpeed;
            GlobalTimeScale *= MapSettings.MapValues<GamePlaySettings.GameSpeed>(
                bank.Stats.GamePlaySettings.gameSpeed, 0.8f, 0.1f);
            SetTimeScale(GlobalTimeScale);

            bank.Stats.LastPlayedLevel = bank.MMTools.DisplayDataTools.NormalizeOverIndex(
                sceneBuildIndex, 1, bank.Stats.levelIdentifiers.Count);
            bank.Save(JsonUtility.ToJson( bank.Stats), DataBank.STATS_NAME);
            
            SceneManager.LoadScene(bank.Stats.LastPlayedLevel);            
        }
        public void LoadMainMenu()
        {
            SetTimeScale(1);
            bank.Save(JsonUtility.ToJson(bank.Stats), DataBank.STATS_NAME);
            SceneManager.LoadScene(0);
        }
    }
}
