﻿using Assets.MainMenuStuff.MMToolkitConsumers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.MainMenuStuff
{
    public class UpdateMusicVolume : ExecuteBroadcastedMessage
    {
        public AudioSource source;
        public override void EXECUTE(object parameter)
        {
            //knowing it will be a one param method.
            try
            {
                source.volume = (float)parameter;
            }
            catch
            {
#if DEBUG
                Debug.Log("Couldn't get a parameter out of update music volume.");
#endif
            }

        }
    }
}
