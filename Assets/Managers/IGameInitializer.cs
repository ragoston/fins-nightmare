﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Managers
{
    public interface IGameInitializer
    {
        /// <summary>
        /// Find by tag and initialize all
        /// </summary>
        /// <param name="tagOfGameObjects"></param>
        void LaunchInitialization(string tagOfGameObjects);
        /// <summary>
        /// Find by component and launch initialization
        /// </summary>
        /// <typeparam name="T"></typeparam>
        void LaunchInitialization<T>();
    }
}
