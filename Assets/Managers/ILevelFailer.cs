﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Managers
{
    interface ILevelFailer
    {
        IEnumerator WaitThenFailLevel();
    }
}
