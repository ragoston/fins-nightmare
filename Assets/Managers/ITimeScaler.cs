﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Managers
{
    public interface ITimeScaler
    {
        void ScaleTime(float start, float end, float time);
        void SetTimeScale(float time);
        bool ScaleTimeInLine(int priority);
        Dictionary<int, Action> TimeScaleInitiations { get; }
        void AddInitiation(int priority, Action action);
        void RemoveInitiation(int priority);
        IEnumerator Routine { get; }
    }
}
