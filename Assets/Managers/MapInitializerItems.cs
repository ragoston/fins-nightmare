﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Managers
{
    public static class MapInitializerItems
    {
        public static Dictionary<Type, int> InitializerTypeToObjectArrayIndex
            = new Dictionary<Type, int>();
        public static void LoadUsedMappings()
        {
            InitializerTypeToObjectArrayIndex.Add(
                typeof(Scripts.Statistics.DataBank),0);
            InitializerTypeToObjectArrayIndex.Add(
                typeof(ITimeScaler), 1);
        }
        //public static IEnumerable<object> OrderParametersByType(object[] input)
        //{
        //    foreach (var item in input
        //        .OrderBy(x => ()=>
        //        {
        //            return InitializerTypeToObjectArrayIndex.Where(y => y.Key.Equals(x.GetType())).First().Value;
        //        }))
        //    {
        //        yield return item;
        //    }
        //}
        //private static int FindByType(Type t)
        //{
        //    foreach (var item in InitializerTypeToObjectArrayIndex)
        //    {
        //        if(item.Key is typeof(t))
        //    }
        //}
    }
}
