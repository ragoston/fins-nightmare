﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Managers
{
    internal class PriorityTimeManager : ITimeScaler
    {
        private MonoBehaviour mono;
        public PriorityTimeManager(MonoBehaviour mono)
        {
            this.mono = mono;
            TimeScaleInitiations = new Dictionary<int, Action>();
        }
        public Dictionary<int, Action> TimeScaleInitiations { get; private set; }
        
        public IEnumerator Routine { get; private set; }

        private IEnumerator _ScaleTime(float start, float end, float time)
        {
            float lastTime = Time.realtimeSinceStartup;
            float timer = 0.0f;

            while (timer < time)
            {
                //scaling down time 
                Time.timeScale = Mathf.Lerp(start, end, timer / time);
                timer += (Time.realtimeSinceStartup - lastTime);
                lastTime = Time.realtimeSinceStartup;

                yield return null;
            }
            Time.timeScale = end;
        }

        public bool ScaleTimeInLine(int priority)
        {
            if (TimeScaleInitiations.Count <= 0)
            {
                Debug.Log("No elements in TimeScaleInitiations and ScaleTimeInLine was called.");
                return false;
            }
            if (Routine != null)
                mono.StopCoroutine(Routine);
            KeyValuePair<int, Action> myCor = new KeyValuePair<int, Action>();
            myCor = TimeScaleInitiations.OrderByDescending(x => x.Key).FirstOrDefault();
            if (priority == myCor.Key)
            {
                myCor.Value.Invoke();
                //TimeScaleInitiations.Remove(myCor.Key);
                TimeScaleInitiations.Clear();
                return true;
            }
            return false;
        }

        public void SetTimeScale(float time)
        {
            if (Routine != null)
                mono.StopCoroutine(Routine);
            Time.timeScale = time;
        }

        public void AddInitiation(int priority, Action action)
        {
            if (!TimeScaleInitiations.ContainsKey(priority))
            {
                TimeScaleInitiations.Add(priority, action);
            }
        }

        public void RemoveInitiation(int priority)
        {
            if (TimeScaleInitiations.ContainsKey(priority))
            {
                TimeScaleInitiations.Remove(priority);
            }
        }

        public void ScaleTime(float start, float end, float time)
        {
            if (Routine != null)
                mono.StopCoroutine(Routine);
            Routine = _ScaleTime(start, end, time);
            mono.StartCoroutine(Routine);
        }
    }
}
