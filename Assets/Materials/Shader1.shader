﻿Shader "Practice/Basic/Shader1"
{
	Properties{
		_MainTex("DiffTexture",2D) = "white"{}
	}
	SubShader {
		Tags {"RenderType"="Opaque"}
		CGPROGRAM
			#pragma surface surf Lambert
			struct Input{
				float2 uv_MainTex;
				float4 color : COLOR;
			};
			void surf (Input IN, inout SurfaceOutput o ){
				o.Albedo = 1;
			}
		ENDCG
	}
	Fallback "Diffuse"
}