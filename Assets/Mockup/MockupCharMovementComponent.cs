﻿using Assets.JumpAssist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Mockup
{
    class MockupCharMovementComponent : ICharacterMovementFunctionality
    {
        public bool movementDisabled { get; set; }
        public Rigidbody2D rb2d { get; set; }
        public float maxSpeed { get; set; }
        public Transform groundCheck { get; set; }
        public LayerMask whatIsGround { get; set; }
        public LayerMask whatIsWall { get; set; }
        public float groundRadius { get; set; }
        public bool grounded { get; set; }
        public bool jump { get; set; }
        public float jumpSpeed { get; set; }

        public Transform wallCheck { get; set; }

        public Action UPDATE_ANIM { get; set; }
        public Func<Vector2> GetJumpSpeed { get; set; }

        public Func<Vector2> GetDefaultJumpSpeed
        {
            get
            {
                return () => new Vector2(rb2d.velocity.x, jumpSpeed);
            }
        }

        public MockupCharMovementComponent(Action updateAnimation,
            Rigidbody2D rb2d, Transform groundCheck, Transform wallCheck,
            LayerMask whatIsGround, LayerMask whatIsWall)
        {
            jumpSpeed = 23.0f;
            groundRadius = 0.26f;
            //MAXSPEED IS ORIGINALLY 8.0f
            maxSpeed = 8.0f;
            jump = false;
            grounded = false;
            movementDisabled = false;
            UPDATE_ANIM = updateAnimation;
            this.whatIsGround = whatIsGround;
            this.whatIsWall = whatIsWall;
            this.rb2d = rb2d;
            this.groundCheck = groundCheck;
            this.wallCheck = wallCheck;
            //by default
            GetJumpSpeed = GetDefaultJumpSpeed;
        }

        public void Movement()
        {
            float h = Input.GetAxis("Horizontal");
            //float h = 1;
            ///*
            // * additional for direction change: if we change it, we multiply dat shit by -1
            //*/
            //if (changeDir)
            //{
            //    h *= -1.0f;
            //}

            //additional until here.
            if (!movementDisabled)
            {
                rb2d.velocity = new Vector2(maxSpeed * Input.GetAxis("Horizontal"), rb2d.velocity.y);
            }
            //FROM HERE IT WAS IN UPDATE
            grounded = Physics2D.OverlapCircle(new Vector2(groundCheck.transform.position.x, groundCheck.transform.position.y),
                groundRadius, whatIsGround);
            if (!movementDisabled && grounded &&
        Input.GetButton("Jump"))
            {
                //then we can jump
                //update animation state and set image index to 0 - needed to use delegates.
                UPDATE_ANIM();

                //added again from here

                //zeroing out the Y axis velocity if we had one
                rb2d.velocity = new Vector2(rb2d.velocity.x, 0.0f);

                //until here
                jump = true;
            }

            //UNTIL HERE.
            if (jump && !movementDisabled)
            {
                jump = false;
                //old from here
                //rb2d.AddForce(new Vector2(0.0f, jumpForce));
                //to here.

                /*
                 * let's try giving the player a velocity instead of a force.
                 * this shit is the new one. 
                 * note that we are not ADDING to the velocity but setting an entirely new one.
                 * with this we may eliminate the possibilities of adding too much force
                 * or speed and get unpredictable movement.
                */

                rb2d.velocity = GetJumpSpeed();
            }
        }
    }
}
