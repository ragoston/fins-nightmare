﻿using Assets.Scripts.AdditionalGameUI;
using Assets.Scripts.InGameInit;
using Assets.Scripts.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;
using Assets.HighlightDanger.Code;
using Assets.Managers;

namespace Assets.Mockup
{
    /// <summary>
    /// test class that has similar functionality to the SceneGameManager, just knows a bit less,
    /// has no persistent information and requires no DataBank to be present (creates its own).
    /// </summary>
    public class MockupInitializer : MonoBehaviour, Managers.ITimeScaler
    {
        GameObject bank;
        public GameObject DangerIndicator;

        public bool TimeScaleAllowed
        {
            get { return true; }
            set { /*readonly this time.*/}
        }

        Dictionary<int, Action> TimeScaleInitiations { get; set; }

        public IEnumerator Routine
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        Dictionary<int, Action> ITimeScaler.TimeScaleInitiations
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IEnumerator _ScaleTime(float start, float end, float time)
        {
            float lastTime = Time.realtimeSinceStartup;
            float timer = 0.0f;

            while (timer < time)
            {
                //scaling down time 
                Time.timeScale = Mathf.Lerp(start, end, timer / time);
                timer += (Time.realtimeSinceStartup - lastTime);
                lastTime = Time.realtimeSinceStartup;

                yield return null;
            }
            Time.timeScale = end;
        }

        private void Awake()
        {
            bank = GameObject.FindGameObjectWithTag("DataBank");
            bank.GetComponent<DataBank>().LoadDataFromINIT(null, null, null);
            if (DangerIndicator != null)
            {
                foreach (var item in GameObject.FindGameObjectsWithTag("DANGER"))
                {
                    DangerIndicator.InstantiateHighlightDanger(item.transform, 
                        GetComponent<DangerHighlightEffectColor>());
                }
            }
        }

        public void ScaleTimeInLine(float start, float end, float time)
        {
            //if using this, it won't be an issue.
            StartCoroutine(_ScaleTime(start, end, time));
        }

        public void SetTimeScale(float time)
        {
            Time.timeScale = time;
        }

        public bool ScaleTimeInLine(int priority)
        {
            throw new NotImplementedException();
        }

        public void AddInitiation(int priority, Action action)
        {
            throw new NotImplementedException();
        }

        public void RemoveInitiation(int priority)
        {
            throw new NotImplementedException();
        }

        public void ScaleTime(float start, float end, float time)
        {
            throw new NotImplementedException();
        }
    }
}
