﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class ParentParticleSaver : MonoBehaviour {

    /*
     * simply start or stop playing, based on camera visibility.
     * note it requires an active renderer to be present on the component. 
     */ 

    ParticleSystem sys;

	// Use this for initialization
	void Start () {
        sys = GetComponentInChildren<ParticleSystem>();
        if (sys != null)
            sys.Stop();
	}

    private void OnBecameInvisible()
    {
        if (sys != null)
            sys.Stop();
    }

    private void OnBecameVisible()
    {
        if (sys != null)
            sys.Play();
    }
}
