﻿using Assets.Scripts.InGameInit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using UnityEngine;

namespace Assets.PostProcessingAssets
{
    [RequireComponent(typeof(Light))]
    public class EnableLensFlare : InitializeMeIngame
    {
        public override void INITIALIZE(DataBank bank)
        {
            GetComponent<Light>().enabled = bank.Stats.Settings.PostProc_value;
        }
    }
}
