﻿using Assets.Scripts.InGameInit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Statistics;
using System;

[RequireComponent(typeof(LensFlareTrigger))]
public class InitLensFlare : InitializeMeIngame
{
    public override void INITIALIZE(DataBank bank)
    {
        GetComponent<LensFlareTrigger>().flareAllowed
            = bank.Stats.Settings.PostProc_value;
    }
}
