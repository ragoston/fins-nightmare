﻿using Assets.Scripts.InGameInit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using UnityEngine;

namespace Assets.PostProcessingAssets
{
    [RequireComponent(typeof(LightFlicker))]
    public class InitLightFlicker : InitializeMeIngame
    {
        public override void INITIALIZE(DataBank bank)
        {
            GetComponent<LightFlicker>().postProcAllowed
                = bank.Stats.Settings.PostProc_value;
        }
    }
}
