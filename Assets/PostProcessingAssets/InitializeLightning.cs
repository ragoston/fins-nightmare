﻿using Assets.Scripts.InGameInit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using UnityEngine;

namespace Assets.PostProcessingAssets
{
    [RequireComponent(typeof(Lightning))]
    public class InitializeLightning : InitializeMeIngame
    {
        public override void INITIALIZE(DataBank bank)
        {
            var l = GetComponent<Lightning>();
            l.InitializeSelf();
            l.mainCam = GameObject.FindGameObjectWithTag("MainCamera");
            l.ALLOWED = bank.Stats.Settings.PostProc_value;
            l.lightningSFX.InitMe(bank.Stats.Settings);
        }
    }
}
