﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LensFlareTrigger : MonoBehaviour
{
    public Light _light;
    public float chanceOfFlare;
    public float lengthOfFlare;
    public float lightPower;

    private LensFlare flare;
    public bool flareAllowed = true;
	void Start ()
    {
        if(_light == null)
        {
            Debug.Log("Found no light on the script.");
            Destroy(this.gameObject);
        }
        _light.intensity = 0;
        flare = _light.GetComponent<LensFlare>();
        _light.gameObject.SetActive(false);
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            float res = Random.Range(0f, 100f);
            if (res <= chanceOfFlare)
            {
                StartCoroutine(StartFlare());
            }
        }
    }
    public IEnumerator StartFlare()
    {
        _light.gameObject.SetActive(true);
        _light.intensity = lightPower;
        yield return new WaitForSeconds(lengthOfFlare);
        StartCoroutine(DimFlare());
        
    }
    private IEnumerator DimFlare()
    {
        while(flare.brightness > 0)
        {
            flare.brightness -= Time.deltaTime * 0.5f;
            yield return null;
        }
    }
}
