﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.PostProcessingAssets
{
    public class LightFlicker : MonoBehaviour
    {
        public bool postProcAllowed = true;
        private Light myLight;
        private void LateUpdate()
        {
            if (postProcAllowed)
            {
                SetLightValue();
            }
        }
        private void SetLightValue()
        {
            myLight.intensity = UnityEngine.Random.Range(0.2f, 1f);
        }
    }
}
