﻿using Assets.MainMenuStuff.Audio;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class Lightning : MonoBehaviour
{
    public PostProcessingProfile postProcProf;
    public PostProcessingProfile proc;
    public GameObject mainCam;
    public SpriteRenderer lightSprite;
    public float chanceOfLightning;
    public bool ALLOWED = true;
    private CircularList data 
        = new CircularList();
    public Level5_LightningSFX lightningSFX;
    private bool finished = false;
    public void InitializeSelf()
    {
        lightningSFX = GetComponent<Level5_LightningSFX>();
        postProcProf.bloom.enabled = false;
        lightSprite.gameObject.SetActive(false);
        data.AddNew(new LightningInfo(0.2f, 0.4f, 0f));
        data.AddNew(new LightningInfo(0.5f, 0.16f, 0.22f));
        data.AddNew(new LightningInfo(0.2f, 0.7f, 0f));
        data.AddNew(new LightningInfo(0.9f, 0.3f, 0.85f));
        data.AddNew(new LightningInfo(0.12f, 0.7f, 0.2f));
        data.AddNew(new LightningInfo(1.3f, 0.4f, 2.6f));
        data.AddNew(new LightningInfo(0.3f, 0.5f, 0.6f));
        data.AddNew(new LightningInfo(1.7f, 0.4f, 1f));
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //throw a dice and if so, start coroutine.
            int res = UnityEngine.Random.Range(0, 100);
            if(res <= chanceOfLightning && ALLOWED)
            {
                lightSprite.gameObject.SetActive(true);
                mainCam.GetComponent<PostProcessingBehaviour>().profile = postProcProf;
                //start coroutine.
                StartCoroutine(Storm());
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            finished = true;
            var bloom = postProcProf.bloom.settings;
            bloom.bloom.intensity = 0f;
            postProcProf.bloom.settings = bloom;
            mainCam.GetComponent<PostProcessingBehaviour>().profile = proc;
            Destroy(this);
        }
    }
    private Color Linearize(Color color, float factor)
    {
        factor /= 2.1f;
        Color min = new Color(54f, 54f, 54f);
        Color max = new Color(131f, 163f, 255f);
        Vector3 diff = new Vector3(max.r - min.r,
            max.g - min.g, max.b - min.b);
        return new Color(factor / diff.x + min.r, 
            factor / diff.y + min.g,
            factor / diff.z + min.b, color.a);
        
    }
    /// <summary>
    /// Percent will be input like "intensity / maxIntensity * 100"
    /// </summary>
    /// <param name="percent"></param>
    /// <returns></returns>
    private float LinearizeDesat(float percent)
    {
        float min = 98f;
        float max = 245f;
        float diff = max - min;
        return (diff * percent / 100f)/(255f);
    }
    private IEnumerator Storm()
    {
        float timer = 0f;
        float lastTime = Time.time;
        postProcProf.bloom.enabled = true;
        for (int i = 0; !finished; ++i)
        {
            for (timer = 0; timer < data[i].TimeToReachNext;)
            {
                var bloom = postProcProf.bloom.settings;
                bloom.bloom.intensity =
                    Mathf.Lerp(data[i].Strength, data[i + 1].Strength,
                    timer / data[i].TimeToReachNext);
                postProcProf.bloom.settings = bloom;
                var col = LinearizeDesat((bloom.bloom.intensity / 2.1f) * 100f);
                lightSprite.color = new Color(col, col, col, lightSprite.color.a);

                timer += (Time.time - lastTime);
                lastTime = Time.time;
                yield return null;
            }
            //yield return new WaitForSeconds(data[i].WaitHere);
        }
    }

}
public class LightningInfo
{
    [Tooltip("The time it takes to reach next node for bloom intensity.")]
    private float timeToReachNext;    
    public float TimeToReachNext { get { return timeToReachNext; } }

    private float strength;
    public float Strength { get { return strength; } }

    private float waitHere;
    public float WaitHere { get { return waitHere; } }

    public LightningInfo(float timeToReachNext, float waitHere, 
        float strength)
    {
        this.waitHere = waitHere;
        this.timeToReachNext = timeToReachNext;
        this.strength = strength;
    }
}
class CircularList : List<LightningInfo>
{
    public new LightningInfo this[int i]
    {
        get { return base[i % base.Count]; }
    }
    public void AddNew(LightningInfo element)
    {
        LightningInfo l1 = new LightningInfo(element.WaitHere,
            element.WaitHere, element.Strength);
        this.Add(l1);
        Add(element);
    }
}
