﻿using UnityEngine;
using System.Collections;

public class ActivateLevelParts : MonoBehaviour
{
    public static GameObject[] partsToDisable;
    public static GameObject[] partsToEnable;
    //public static GameObject[] intermediateParts;
	// Update is called once per frame
	public static void ChangeLevelParts()
    {
        for (int i = 0; i < partsToEnable.Length; i++)
        {
            partsToEnable[i].SetActive(true);
        }
        for(int i = 0; i < partsToDisable.Length; i++)
        {
            partsToDisable[i].SetActive(false);
        }
    }
    public static void LevelPartsInitialize()
    {
        partsToDisable = GameObject.FindGameObjectsWithTag("LevelPartToDisable");
        partsToEnable = GameObject.FindGameObjectsWithTag("LevelPartToEnable");
        //intermediateParts = GameObject.FindGameObjectsWithTag("IntermediatePart");
        //we need to set them not active beforehand in order to be able to set them active later on.
        for(int i = 0; i < partsToEnable.Length; i++)
        {
            partsToEnable[i].SetActive(false);
        }
    }
    //public static void WaitThenDisableInterMediateParts()
    //{
    //    for (int i = 0; i < intermediateParts.Length; i++)
    //    {
    //        intermediateParts[i].SetActive(false);
    //    }
    //}
}
