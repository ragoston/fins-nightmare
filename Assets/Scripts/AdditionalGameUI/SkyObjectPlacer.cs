﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.AdditionalGameUI
{
    public class SkyObjectPlacer : MonoBehaviour
    {
        Renderer rend;
        public float padding = 1;
        public enum FromWhatEnd { left, right }
        public FromWhatEnd anchor = FromWhatEnd.right;
        private void Start()
        {
            rend = GetComponent<Renderer>();
            foreach (Transform c in transform)
            {
                if (rend != null)
                    break;
                rend = c.GetComponent<Renderer>();
            }

            int dir;
            switch (anchor)
            {
                case FromWhatEnd.left:
                    dir = 0;
                    break;
                case FromWhatEnd.right:
                    dir = Screen.width;
                    break;
                default:
                    dir = Screen.width;
                    break;
            }
            var screenPos = Camera.main.ScreenToWorldPoint(
                 new Vector3(dir, Screen.height / 2,
                 Mathf.Abs(Camera.main.transform.position.z) + Mathf.Abs(transform.position.z)));
            transform.position = new Vector3(screenPos.x - GetPadding(rend.bounds), transform.position.y,
                transform.position.z);

        }
        private float GetPadding(Bounds b)
        {
            float r;
            switch (anchor)
            {
                case FromWhatEnd.left:
                    r = -1 * (b.extents.x + padding);
                    break;
                case FromWhatEnd.right:
                    r = b.extents.x + padding;
                    break;
                default:
                    r = b.extents.x + padding;
                    break;
            }
            return r;
        }
    }
}
