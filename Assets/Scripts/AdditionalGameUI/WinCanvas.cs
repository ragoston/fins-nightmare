﻿using Assets.Scripts.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.AdditionalGameUI
{
    public class WinCanvas : MonoBehaviour
    {
        DataBank bank;
        List<GameObject> childrenReq = new List<GameObject>();
        public Button nextLevel;
        public Button mainmenu;
        public WinObject winObject;
        public Text _levelCompleted;
        public Text deathCount;
//        private void Awake()
//        {
//            pers = GameObject.Find("GameData").GetComponent<Persistence>();
//            if(pers == null)
//            {
//#if DEBUG
//                Debug.Log("Persistent GO missing.");
//#endif
//            }

//        }
        public void Initialize(DataBank bank)
        {
            this.bank = bank;
            bank.MMTools.DisplayDataTools.DoRecursivelyInGameObject(
    (g) => { childrenReq.Add(g); }, gameObject);
            foreach (var c in childrenReq.Where(x => x.CompareTag("Recolorable")))
            {
                bank.MMTools.Colorizer.RecolourUI(c, bank.baseColor[bank.Stats.LastPlayedLevel-1],
                    bank.baseColor2[bank.Stats.LastPlayedLevel-1], bank.outlineColor[bank.Stats.LastPlayedLevel-1]);
            }
            //winObject.LEVEL_COMPLETE += WinObject_LEVEL_COMPLETE;
            //instead of looking for a GO named WinObject, look for an object with the component <WinObject>

            //winObject = GameObject.Find("WinObject").GetComponent<WinObject>();
            try
            {
                winObject = FindObjectsOfType<GameObject>()
                    .Where(x => x.GetComponent<WinObject>() != null)
                    .First().GetComponent<WinObject>();
            }
            catch (NullReferenceException)
            {
#if DEBUG
                Debug.Log("WIN OBJECT MISSING OR NOT ACTIVE!");
#else
               // winObject has to be instantiated.
#endif
            }

        }
        public void WinObject_LEVEL_COMPLETE()
        {
            WinObject_LevelComplete_NoCoroutine();
            //Vector3 originalScale = _levelCompleted.transform.localScale;
            ScaleAction s = new ScaleAction(_levelCompleted.transform,
                1f, 1.3f,
                0.3f);
            ScaleAction s2 = new ScaleAction(_levelCompleted.transform,
                1.3f, 1f,
                0.3f);
            s.NEXT_FUNCTION += s2.UniformScale;
            s.UniformScale(this);

            SetDeathCountText();
        }
        public void WinObject_LevelComplete_NoCoroutine()
        {
            if (!this.gameObject.activeSelf || !this.gameObject.activeInHierarchy)
            {
                gameObject.SetActive(true);
            }
            bank.MMTools.StatisticsUtility.GetLevelDatas[bank.Stats.LastPlayedLevel - 1].BestPercent = 100;
            bank.Save(JsonUtility.ToJson(bank.Stats), DataBank.STATS_NAME);
        }
        public void SetDeathCountText()
        {
            deathCount.text = "Death count: " +
                bank.MMTools.StatisticsUtility
                .GetLevelDatas[bank.Stats.LastPlayedLevel - 1].DeathCount.ToString();
        }
        public void LoadNextLevel()
        {
            bank.MMTools.SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        public void LoadMM()
        {
            SceneManager.LoadScene(0, LoadSceneMode.Single);
        }
    }
}
