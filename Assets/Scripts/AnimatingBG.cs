﻿using UnityEngine;
using System.Collections;

public class AnimatingBG : MonoBehaviour
{
    private SpriteRenderer[] sp;
    public float overTime = 10.0f;
    private bool alreadyStarted = false;
    public bool oneEnded = true;
    public bool canContinue = true;
    private enum Direction
    {
        firstDown, firstUp
    }
    private Direction scaleDir = Direction.firstDown;
	void Start ()
    {
        //hopefully the two sprite is found.
        sp = GetComponentsInChildren<SpriteRenderer>();
	}

	void LateUpdate()
    {
        if (!alreadyStarted)
        {
            alreadyStarted = true;
            StartCoroutine(Changer());
        }
    }
    IEnumerator ScaleSpriteAlpha(float start, float end, float time, SpriteRenderer spr)
    {
        oneEnded = false;
        float lastTime = Time.realtimeSinceStartup;
        float timer = 0.0f;

        while (timer < time)
        {
            //fading out the sprite using its sprite renderer alpha
            spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, Mathf.Lerp(start, end, timer / time));
            timer += (Time.realtimeSinceStartup - lastTime);
            lastTime = Time.realtimeSinceStartup;
            yield return null;
        }
        oneEnded = true;
    }
    IEnumerator Changer()
    {
        while (canContinue)
        {
            if (oneEnded)
            {
                switch (scaleDir)
                {
                    case Direction.firstDown:
                        //scaling down the first sprite renderer
                        StartCoroutine(ScaleSpriteAlpha(sp[0].color.a, 0.0f, overTime, sp[0]));
                        //now scaling up the second
                        StartCoroutine(ScaleSpriteAlpha(0.0f, sp[1].color.a, overTime, sp[1]));
                        scaleDir = Direction.firstUp;
                        break;
                    case Direction.firstUp:
                        StartCoroutine(ScaleSpriteAlpha(0, sp[0].color.a, overTime, sp[0]));
                        StartCoroutine(ScaleSpriteAlpha(sp[1].color.a, 0.0f, overTime, sp[1]));
                        scaleDir = Direction.firstDown;
                        break;
                }
                yield return null;
            }else
            {
                yield return null;
            }
        }
    }
}
