﻿using UnityEngine;
using System.Collections;

public class BreakingBranch : MonoBehaviour
{
    public event System.EventHandler Fire;

    private Rigidbody2D branchRB;
    private HingeJoint2D branchHingeJoint;
    public float wantedGravityScale = 0.54f;
	// Use this for initialization
	void Awake ()
    {
        branchRB = GetComponent<Rigidbody2D>();
        branchRB.gravityScale = 0.0f;
        branchHingeJoint = GetComponent<HingeJoint2D>();
        branchHingeJoint.enabled = false;
        branchRB.Sleep();
	}
	
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && !CharacterController2D.playerDied)
        {
            if (branchRB.IsSleeping())
            {
                if (Fire != null) Fire(this, null);
                branchRB.WakeUp();
                branchHingeJoint.enabled = true;
            }
            //meaning we activate this shit only if the player enters
            //the bloody circle and is not dead. that would ruin the fun next time they 
            //actually make it to the branch.
            branchRB.gravityScale = wantedGravityScale;
            //to make sure we don't play the animation again, 
            //we disable the collider 
            GetComponent<CircleCollider2D>().enabled = false;
            StartCoroutine(WaitThenDisableThisShit());
        }
    }
    
    /*
     * when the player hits a given collider, we set the gravity scale to 0.54.
     * that will make the joint fall, thus break the branch. 
    */
    IEnumerator WaitThenDisableThisShit()
    {
        yield return new WaitForSeconds(1.0f);
        //then now we disable this shit to ease up on memory and processing power.
        branchRB.Sleep();
        branchHingeJoint.enabled = false;
        Destroy(branchHingeJoint);
        Destroy(branchRB);
        GetComponent<CircleCollider2D>().enabled = false;
    }
}
