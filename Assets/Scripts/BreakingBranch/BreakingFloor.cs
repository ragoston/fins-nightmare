﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakingFloor : MonoBehaviour
{
    public GameObject cover;
    private bool started = false;
    public BreakingBranch[] branches;
    private SpriteRenderer coverSprite;
    private void Awake()
    {
        coverSprite = cover.GetComponent<SpriteRenderer>();
        foreach (var item in branches)
        {
            item.Fire += OnFadeOut;
        }
    }
    public void OnFadeOut(object sender, System.EventArgs args)
    {
        if (!started)
        {
            started = true;
            StartCoroutine(_FadeOut(coverSprite));
        }   
    }
    private IEnumerator _FadeOut(SpriteRenderer spr)
    {
        float i = 0;
        while(spr.color.a > 0)
        {
            i += Time.deltaTime;
            spr.color = new Color(
                spr.color.r, spr.color.g, spr.color.b,
                Mathf.Lerp(spr.color.a, 0f, i*2));
            yield return null;
        }
    }
}
