﻿using UnityEngine;
using System.Collections;

public class CamShakeActivator : MonoBehaviour
{
    public CameraShaking camShakeScript;
    private BoxCollider2D boxColl;
    private CircleCollider2D circleColl;
    private enum Actions
    {
        entering, exiting
    }
    private Actions whichAction = Actions.entering;
	// Use this for initialization
	void Start ()
    {
	    if(camShakeScript == null)
        {
            GameObject.FindGameObjectWithTag("MainCamera").GetComponentInChildren<CameraShaking>();
            if(camShakeScript == null)
            {
                Destroy(this.gameObject);
            }
        }
        boxColl = GetComponent<BoxCollider2D>();
        circleColl = GetComponent<CircleCollider2D>();

	}
	void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            switch (whichAction)
            {
                case Actions.entering:
                    Destroy(boxColl);
                    camShakeScript.enabled = true;
                    camShakeScript.currentShakeState = CameraShaking.ShakeState.shaking;
                    whichAction = Actions.exiting;
                    break;
                case Actions.exiting:
                    Destroy(circleColl);
                    camShakeScript.currentShakeState = CameraShaking.ShakeState.idle;
                    StartCoroutine(WaitThenDeactivateShake());
                    break;
                
            }
        }
    }
    IEnumerator WaitThenDeactivateShake()
    {
        yield return new WaitForSeconds(3.0f);
        camShakeScript.enabled = false;
    }
}
