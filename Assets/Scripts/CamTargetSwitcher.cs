﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts
{
    public class CamTargetSwitcher : MonoBehaviour
    {
        public static CamTargetSwitcher Instance;
        int currentlySelected = 0;
        int formerlySelected = 0;
        CameraMovement camMove;
        GameObject[] targets;
        private void Start()
        {

            if (Instance == null)
                Instance = this;

            camMove = GameObject.FindGameObjectWithTag("MainCamera")
                .GetComponent<CameraMovement>();
            camMove.howManyPercent = 0;
            targets = GameObject.FindGameObjectsWithTag("MM_cam_pos");
            UINavigation[] navs = GameObject.FindObjectsOfType<UINavigation>();
            foreach (var nav in navs)
            {
                nav.NAVIGATE += ChangeTarget;
            }
        }
        public void ChangeTarget()
        {
            //int result = UnityEngine.Random.Range(0, targets.Length);
            //if (result >= currentlySelected) result += 1;
            //if (result >= targets.Length) result = 0;
            //currentlySelected = result;
            currentlySelected = UnityEngine.Random.Range(0, targets.Length);
            if (currentlySelected == formerlySelected)
            {
                currentlySelected++;
                if (currentlySelected >= targets.Length)
                    currentlySelected = 0;
            }
                
            
            camMove.target = targets[currentlySelected].transform;
            formerlySelected = currentlySelected;
        }
    }
}
