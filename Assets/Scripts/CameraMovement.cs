﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour
{
    public Vector2 SkyDeltaMovement = new Vector2();

    public Transform target;
    public float damping = 1;
    public float lookAheadFactor = 3;
    public float lookAheadReturnSpeed = 0.5f;
    public float lookAheadMoveThreshold = 0.1f;
    public float yPosRestriction = -1;

    public Transform SkyGOTransform;
    float offsetZ;
    Vector3 lastTargetPosition;
    Vector3 currentVelocity;
    Vector3 lookAheadPos;

    private float currentHeight;
    private float wantedHeight;
    private float percentOfScreen;
    public float howManyPercent = 10.0f;
    public float negativeHowManyPercent;
    public float originalHowManyPercent;
    //float nextTimeToSearch = 0;

        //from this 
    public enum SkyStates
    {
        stable, moving
    }
    public static SkyStates currentSkyState = SkyStates.stable;
        //until this for the moving sky.

    // Use this for initialization
    void Start()
    {
        lastTargetPosition = target.position;
        offsetZ = (transform.position - target.position).z;
        transform.parent = null;

        //howManyPercent = 10.0f;
        negativeHowManyPercent = howManyPercent * -1.0f;
        originalHowManyPercent = howManyPercent;
        //not we are getting the one percent of the current screen width.

        percentOfScreen = (Screen.width * 1.0f) * 10.0f / (Screen.height * 1.0f);
        percentOfScreen /= 100.0f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // only update lookahead pos if accelerating or changed direction
        float xMoveDelta = (target.position - lastTargetPosition).x;

        bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;

        if (updateLookAheadTarget)
        {
            lookAheadPos = lookAheadFactor * Vector3.right * Mathf.Sign(xMoveDelta);
        }
        else
        {
            lookAheadPos = Vector3.MoveTowards(lookAheadPos, Vector3.zero, Time.deltaTime * lookAheadReturnSpeed);
        }

        Vector3 aheadTargetPos = target.position + lookAheadPos + Vector3.forward * offsetZ;
        //new one line here, used to be:
        //Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref currentVelocity, damping);

        Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos + Vector3.right*(howManyPercent * percentOfScreen), ref currentVelocity, damping);
        //the line below used to be active.
            //newPos = new Vector3(target.position.x + (howManyPercent * percentOfScreen),newPos.y, newPos.z);
        transform.position = newPos;

        /*
         * Additional code: changing the sky movement in respect to the state.
        */
        //switch (currentSkyState)
        //{
        //    case SkyStates.stable:
        //        SkyGOTransform.position = new Vector3(transform.position.x, transform.position.y, SkyGOTransform.position.z);
        //        break;
        //    case SkyStates.moving:
        //        SkyGOTransform.position = new Vector3(SkyGOTransform.position.x + SkyDeltaMovement.x, transform.position.y + SkyDeltaMovement.y, SkyGOTransform.position.z);
        //        break;
        //}
        //until here. the next line was active.
        //SkyGOTransform.position = new Vector3(transform.position.x, transform.position.y, SkyGOTransform.position.z);
        /*
         * To avoid the flickering of the sky, we try setting the sky gameobject's
         * transform here instead of the master script.
        */ 

        lastTargetPosition = target.position;
    }
    public void SmoothChangeView(bool playerChanged)
    {
        float increment = 0.0f;
        StartCoroutine(SmoothChangeCoroutine(playerChanged, increment));
    }
    public IEnumerator SmoothChangeCoroutine(bool playerChanged, float increment)
    {
        if (playerChanged)
        {
            while (howManyPercent > negativeHowManyPercent)
            {
                increment += Time.deltaTime;
                howManyPercent = Mathf.Lerp(howManyPercent, negativeHowManyPercent, increment);
                yield return new WaitForEndOfFrame();
            }
        }
        else
        {
            while (howManyPercent < originalHowManyPercent)
            {
                increment += Time.deltaTime;
                howManyPercent = Mathf.Lerp(howManyPercent, originalHowManyPercent, increment);
                yield return new WaitForEndOfFrame();
            }
        }
    }
}
