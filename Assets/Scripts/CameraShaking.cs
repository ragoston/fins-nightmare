﻿using UnityEngine;
using System.Collections;

public class CameraShaking : MonoBehaviour
{
    private bool shakingContinues = true;
    //public float frameRate = 10.0f;
    private Vector3 basePosition;
    private Quaternion baseRotation;
    private bool alreadyStarted = false;
    [Tooltip("A multiplier that decides how smooth or harsh the transition between the positions. Best be between 0 and 1.")]
    public float lerpRate = 0.8f;
    [Tooltip("A value that decides how ofter a new transform is called = the rate of shaking.")]
    public float lerpTime = 10.0f;
    [Tooltip("Same as the lerprate, just for the quaternion.")]
    public float quatLerpRate = 0.2f;
    private Vector3 newPos;
    private Quaternion newRot;
    Vector3 currentVelocity;
    public float deltaPos = 1.0f;
    public float deltaRotDegree = 10.0f;
    private bool gettingBackDone = false;
    private float idleReturnTime = 2.0f;
    public enum ShakeState
    {
        shaking, idle
    }
    public ShakeState currentShakeState = ShakeState.idle;
    void Start ()
    {
        basePosition = transform.localPosition;
        baseRotation = transform.localRotation;
    }
	
	void LateUpdate ()
    {
        //if (!alreadyStarted)
        //{
        //    alreadyStarted = true;
        //    StartCoroutine(CamShake(lerpTime));
        //}
        switch (currentShakeState)
        {
            case ShakeState.idle:
                //lerping back to the idle camera state
                //and stopping the shaking
                if (!gettingBackDone)
                {
                    gettingBackDone = true;
                    StopCoroutine("CamShake");
                    StartCoroutine(ReturnToIdle(idleReturnTime));
                    alreadyStarted = false; 
                }
                break;
            case ShakeState.shaking:
                //DO THE HARLEM SHAKE
                if (!alreadyStarted)
                {
                    //if we didn't start it already
                    //the coroutine has to be called only once.
                    alreadyStarted = true;
                    if (gettingBackDone)
                        gettingBackDone = false;
                    StartCoroutine(CamShake(lerpTime));
                }
                break;
        }
    }
    IEnumerator CamShake(float time)
    {
        while (shakingContinues)
        {
            //getting the new values
            newPos = basePosition + new Vector3(Random.Range(-1.0f * deltaPos, deltaPos), Random.Range(-1.0f * deltaPos, deltaPos));
            newRot = Quaternion.Euler(baseRotation.x,
                baseRotation.y + Random.Range(-1.0f * deltaRotDegree, deltaRotDegree), baseRotation.z + Random.Range(-1.0f*deltaRotDegree, deltaRotDegree));
            //now we have them. 

            //initializing the timers
            float lastTime = Time.time;
            float timer = 0.0f;

            while (timer < time)
            {
                transform.localPosition = Vector3.SmoothDamp(transform.localPosition, newPos, ref currentVelocity, lerpTime*lerpRate);
                transform.localRotation = Quaternion.Slerp(transform.localRotation, newRot, (timer / time)*quatLerpRate);
                timer += (Time.time - lastTime);
                lastTime = Time.time;
                if(currentShakeState == ShakeState.idle)
                {
                    //then we get out of the coroutine.
                    break;
                }
                yield return null;
            }
            if (currentShakeState == ShakeState.idle)
            {
                //then we get out of the coroutine.
                break;
            }
        }
    }
    IEnumerator ReturnToIdle(float time)
    {
        float lastTime = Time.time;
        float timer = 0.0f;

        while (timer < time)
        {
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, basePosition, ref currentVelocity, lerpTime * lerpRate);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, baseRotation, (timer / time) * quatLerpRate);
            timer += (Time.time - lastTime);
            lastTime = Time.time;
            yield return null;
        }
    }
}
