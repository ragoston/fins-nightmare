﻿using UnityEngine;
using System.Collections;

public class CannonShoot : MonoBehaviour
{
    public GameObject cannonBallGO;
    public float gravityStartingFactor = 0.1f;

    private CannonBall cannonBall;
    private Animator cannonShootCtrl;
    private Animation cannonShootAnim;
    public float shootingPower = 400.0f;
    public GameObject cannonFlamesGO;
    private SpriteRenderer cannonFlamesSpriteRenderer;
    private static Sprite[] cannonFlames;
    public GameObject cannonSmoke;
    public GameObject[] twoLilColliders;
    
	// Use this for initialization
	void Awake ()
    {
        cannonSmoke.SetActive(false);
        cannonFlamesSpriteRenderer = cannonFlamesGO.GetComponent<SpriteRenderer>();
        cannonBall = new CannonBall(cannonBallGO, gravityStartingFactor);
        cannonShootAnim = GetComponent<Animation>();
        cannonFlames = null;
        cannonFlames = Resources.LoadAll<Sprite>("CannonFlames");
        if(twoLilColliders != null && twoLilColliders.Length >= 2)
        {
            twoLilColliders[0].SetActive(false);
            twoLilColliders[1].SetActive(false);
        }
        
	}
	
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //meaning the player has entered he shooting range
            Shooting(shootingPower);
        }
    }
    void Shooting(Vector2 direction, float force)
    {
        AnimateShooing();
        StartCoroutine(FlamesAnim());
        cannonSmoke.SetActive(true);
        cannonBall.ballGO.SetActive(true);
        cannonBall.ballRB.AddForce(direction * force);
        StartCoroutine(DestroyCannonBall());
        StartCoroutine(ActivateTwoColliders());
    }
    void Shooting(float force)
    {
        AnimateShooing();
        StartCoroutine(FlamesAnim());
        cannonSmoke.SetActive(true);
        cannonBall.ballGO.SetActive(true);
        cannonBall.ballRB.AddForce(Vector2.left * force);
        StartCoroutine(DestroyCannonBall());
        StartCoroutine(ActivateTwoColliders());
    }
    IEnumerator DestroyCannonBall()
    {
        yield return new WaitForSeconds(4.0f);
        Destroy(cannonBall.ballGO);
        yield return new WaitForSeconds(0.3f);
        cannonBall = null;
        DisableThatShit();
    }
    void DisableThatShit()
    {
        Destroy(cannonFlamesGO);
        Destroy(GetComponent<BoxCollider2D>());
        Destroy(cannonShootAnim);
    }
    void AnimateShooing()
    {
        cannonShootAnim.Play();
    }
    IEnumerator FlamesAnim()
    {
        cannonFlamesSpriteRenderer.sprite = cannonFlames[0];
        yield return new WaitForSeconds(0.1f);
        cannonFlamesSpriteRenderer.sprite = cannonFlames[1];
        yield return new WaitForSeconds(0.1f);
        Destroy(cannonFlamesGO);
    }
    IEnumerator ActivateTwoColliders()
    {
        if (twoLilColliders != null && twoLilColliders.Length >= 2)
        {
            yield return new WaitForSeconds(0.6f);
            twoLilColliders[0].SetActive(true);
            twoLilColliders[1].SetActive(true); 
        }
    }
}
public class CannonBall
{
    public Rigidbody2D ballRB;
    public GameObject ballGO;
    
    public CannonBall(GameObject ballInput, float gravityStartingFactor)
    {
        ballGO = ballInput;
        ballRB = ballGO.GetComponent<Rigidbody2D>();
        ballRB.gravityScale = gravityStartingFactor;
        ballGO.SetActive(false);
    }
}
