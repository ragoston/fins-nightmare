﻿using UnityEngine;
using System.Collections;

public class CathedralGodRay : MonoBehaviour
{
    public GameObject godRayStrongGO;
    public GameObject godRayMildGO;
    private SpriteRenderer godRayStrong;
    private SpriteRenderer godRayMild;
    [Tooltip("The time between the start of the alpha lerps.")]
    public float initiateAlphaSwap;
    [Tooltip("The time between the start of the rotation.")]
    //public float initiateRotation;
    private bool lerpStarted = false;
    public bool continueLerping = true;
    public float frameRate = 24.0f;
    public float leftBoundary = -10.0f;
    public float rightBoundary = 10.0f;

    private float godRayStrongPrior;
    private float godRayMildPrior;

    private QuatLerpInput[] godRays;
    void Awake()
    {
        godRayStrong = godRayStrongGO.GetComponent<SpriteRenderer>();
        godRayMild = godRayMildGO.GetComponent<SpriteRenderer>();
        godRays = new QuatLerpInput[2];
        godRays[0] = new QuatLerpInput(godRayStrongGO);
        godRays[1] = new QuatLerpInput(godRayMildGO);
    }

	void LateUpdate ()
    {
        if (!lerpStarted)
        {
            lerpStarted = true;
            StartCoroutine(InitiateAlphaSwap());
            //StartCoroutine(LerpQuat(godRayStrongGO.transform, initiateAlphaSwap, leftBoundary, rightBoundary));
            //StartCoroutine(LerpQuat(godRayMildGO.transform, initiateAlphaSwap, leftBoundary, rightBoundary));
        }
	}
    IEnumerator InitiateAlphaSwap()
    {
        while (continueLerping)
        {
            VaryAlphas(godRayStrong);
            VaryAlphas(godRayMild);
            LerpQuat(godRays[0], initiateAlphaSwap, leftBoundary, rightBoundary);
            LerpQuat(godRays[1], initiateAlphaSwap, leftBoundary, rightBoundary);
            yield return new WaitForSeconds(initiateAlphaSwap);
        }
    }
    IEnumerator LerpAlpha(SpriteRenderer sprToLerp, float start, float end, float time)
    {
        float lastTime = Time.realtimeSinceStartup;
        float timer = 0.0f;

        while (timer < time)
        {
            //fading out the character using its sprite renderer alpha
            sprToLerp.color = new Color(sprToLerp.color.r, sprToLerp.color.g, sprToLerp.color.b, Mathf.Lerp(start, end, timer / time));
            timer += (Time.realtimeSinceStartup - lastTime);
            lastTime = Time.realtimeSinceStartup;
            yield return null;
        }
    }
    void VaryAlphas(SpriteRenderer spr)
    {
        if (spr.color.a >= 0.8f)
        {
            //meaning we already have a high alpha
            //then we should rather decrease it.
            StartCoroutine(LerpAlpha(spr,
            spr.color.a, Random.Range(0.07f, 0.4f), initiateAlphaSwap));
        }
        else if (spr.color.a <= 0.2f)
        {
            StartCoroutine(LerpAlpha(spr,
            spr.color.a, Random.Range(0.4f, 1.0f), initiateAlphaSwap));
        }
        else
        {
            StartCoroutine(LerpAlpha(spr,
            spr.color.a, Random.Range(0.07f, 1.0f), initiateAlphaSwap));
        }
    }
    void LerpQuat(QuatLerpInput rotThis, float time, float leftBoundary, float rightBoundary)
    {
        /*
         * we want to rotate forward, then backward the same amount
         * to avoid spinning. we store the given value to both of these.
        */

        //Transform stuffToRot = rotThis.godRayGO.transform;
      
        //float lastTime = Time.realtimeSinceStartup;

        switch (rotThis.rotState)
        {
            case (QuatLerpInput.RotationStates.rotAway):
                rotThis.deltaRot = Random.Range(leftBoundary, rightBoundary);
                Quaternion destination = new Quaternion(rotThis.idleQuat.x, rotThis.idleQuat.y, rotThis.deltaRot, 1.0f);
                rotThis.deltaQuat = destination;
                rotThis.deltaEulerAngle = Vector3.forward * rotThis.deltaRot;               
                StartCoroutine(RotateMe(rotThis, rotThis.idleQuat, rotThis.deltaEulerAngle, initiateAlphaSwap));
                break;
            case (QuatLerpInput.RotationStates.setBack):
                StartCoroutine(RotateMe(rotThis, rotThis.deltaQuat,-1.0f * rotThis.deltaEulerAngle, initiateAlphaSwap));
                break;
        }
    }
    IEnumerator RotateMe(QuatLerpInput whatToRotate,Quaternion fromWhere,Vector3 byAngles, float inTime)
    {
        Quaternion fromAngle = fromWhere;
        Quaternion toAngle = Quaternion.Euler(whatToRotate.godRayGO.transform.eulerAngles + byAngles);
        toAngle = new Quaternion(toAngle.x, toAngle.y, toAngle.z, 1.0f);

        if(whatToRotate.rotState == QuatLerpInput.RotationStates.setBack)
        {
            fromAngle = new Quaternion(whatToRotate.RotEndPos.x, whatToRotate.RotEndPos.y, whatToRotate.RotEndPos.z, 1.0f);
            toAngle = new Quaternion(0.0f, 0.0f, 0.0f, 1.0f);
        }

        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / inTime)
        {
            whatToRotate.godRayGO.transform.rotation = Quaternion.Lerp(fromAngle, toAngle, t);
            yield return null;
        }
        if(whatToRotate.rotState == QuatLerpInput.RotationStates.rotAway)
        {
            whatToRotate.rotState = QuatLerpInput.RotationStates.setBack;
            whatToRotate.RotEndPos = new Vector3(toAngle.x, toAngle.y, toAngle.z);
        }
        else
        {
            whatToRotate.rotState = QuatLerpInput.RotationStates.rotAway;
        }
        
    }

    public class QuatLerpInput
    {
        public GameObject godRayGO;
        public float deltaRot;
        public float idleRot;
        public Quaternion idleQuat;
        public Quaternion deltaQuat;
        public Vector3 idleEulerAngle;
        public Vector3 deltaEulerAngle;
        public Vector3 RotEndPos;
        public enum RotationStates
        {
            rotAway, setBack
        }
        public RotationStates rotState;
        public QuatLerpInput(GameObject godRayGO)
        {
            this.godRayGO = godRayGO;
            deltaRot = godRayGO.transform.rotation.z;
            idleRot = godRayGO.transform.rotation.z;
            idleQuat = godRayGO.transform.rotation;
            deltaQuat = godRayGO.transform.rotation;
            idleEulerAngle = godRayGO.transform.eulerAngles;
            deltaEulerAngle = godRayGO.transform.eulerAngles;
            rotState = RotationStates.rotAway;
        }
    }
}
