﻿//using UnityEngine;
//using System.Collections;

//public class ChangeDirection : MonoBehaviour
//{
//    CharacterController2D playerController;
//    public bool needToChangeLevelParts = false;
//    public Vector2 forceToAdd = new Vector2();
//    public bool forceNeeded = false;
//    private Transform playerChar;
//    public GameObject wraithGO;
//    [Tooltip("Check if there is an event that has to play before the flip happens.")]
//    private CameraMovement mainCamMovement;
//    private bool alreadyChanged = false;
//    public enum EventsAvailable
//    {
//        zero, wraithEmerge
//    }
//    [Tooltip("The events to chose from. Zero means no event, make changes immediately.")]
//    public EventsAvailable currentEvent = EventsAvailable.wraithEmerge;
//    void Awake()
//    {
//        if(mainCamMovement == null)
//        {
//            mainCamMovement = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraMovement>();
//        }
        
//        if (needToChangeLevelParts)
//        {
//            ActivateLevelParts.LevelPartsInitialize();
//        }
//        if(playerChar == null)
//        {
//            //meaning we haven't initialized it just yet
//            playerChar = GameObject.FindGameObjectWithTag("Player").transform;
//            //then we do so.
//        }
//        playerController = playerChar.gameObject.GetComponent<CharacterController2D>();

//    }

//    void OnTriggerEnter2D(Collider2D other)
//    {
//        if (other.gameObject.CompareTag("Player") && !alreadyChanged)
//        {
//            switch (currentEvent)
//            {
//                case EventsAvailable.zero:
//                    MakeChanges();
//                    break;
//                case EventsAvailable.wraithEmerge:
//                    if (needToChangeLevelParts)
//                    {
//                        ActivateLevelParts.ChangeLevelParts();
//                    }
//                    CharacterController2D.movementDisabled = true;
//                    CharacterController2D.animState = CharacterController2D.CharAnimStates.still;
//                    WraithEmerge();
//                    break;
//            }

            
//        }
//    }
//    public void MakeChanges()
//    {
//        alreadyChanged = true;
//        playerController.ChangeDir = !playerController.ChangeDir;
//        mainCamMovement.SmoothChangeView(playerController.ChangeDir);
//        playerChar.localScale = new Vector3(playerChar.localScale.x * -1.0f,
//            playerChar.localScale.y, playerChar.localScale.z);
//        CharacterController2D.UnFreezeCharAnim();
//        if (forceNeeded)
//        {
//            //meaning we do want to add a force to the player character
//            playerChar.gameObject.GetComponent<Rigidbody2D>().AddForce(forceToAdd);
//        }
//    }
//    void WraithEmerge()
//    {
//        wraithGO.SetActive(true);
//    }
//    //void OnTriggerExit2D(Collider2D other)
//    //{
//    //    if(other.gameObject.CompareTag("Player") && alreadyChanged)
//    //    {
//    //        alreadyChanged = false;
//    //    }
//    //}
//}
