﻿using UnityEngine;
using System.Collections;

public class CharacterMovement : MonoBehaviour
{
    //[HideInInspector]
    public bool jump;

    public float moveForce;
    public float maxSpeed;
    public float jumpForce;
    public Transform groundCheck;
    public Transform wallCheck;

    public bool grounded;
    public bool walled;
    public float groundRadius;
    public float wallRadius;
    public LayerMask whatIsGround;
    public LayerMask whatIsWall;


    private Rigidbody rb;
    public bool colliding;
    public bool movementDisabled;
    public enum MovementStates
    {
        runningOnGround, 
        hitTheWall,
        airborne
    }
    public MovementStates moveState;
	// Use this for initialization
	void Start ()
    {
        movementDisabled = false;
        walled = false;
        colliding = false;
        groundRadius = 0.07f;
        wallRadius = 0.22f;
        moveState = MovementStates.airborne;
        //in dev: setting the pos so I don't have to play it over again.
        //gameObject.transform.position = new Vector3(47.1f, -4.18f);
        //
        jump = false;
        moveForce = 36.50f;
        maxSpeed = 8.0f;
        jumpForce = 1000.0f;
        grounded = false;
        rb = GetComponent<Rigidbody>();

	}
	
	// Update is called once per frame
	void Update ()
    {
        grounded = Physics2D.OverlapCircle(new Vector2(groundCheck.transform.position.x, groundCheck.transform.position.y),
            groundRadius, whatIsGround);
        //walled = Physics2D.OverlapCircle(new Vector2(wallCheck.position.x, wallCheck.position.y),
        //    wallRadius, whatIsWall);
        //walled = Physics2D.OverlapArea(new Vector2(wallCheck.position.x-0.03f, wallCheck.position.y + 0.325f),
        //    new Vector2(wallCheck.position.x + 0.03f, wallCheck.position.y - 0.325f));

        //grounded = Physics.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        //new shit
        if (grounded)
        {
            moveState = MovementStates.runningOnGround;
        }
        
        //end

        //old
        if (grounded && Input.GetButtonDown("Jump"))
        {
            jump = true;
        }
        //end

        //the switch block here updates the 

        //switch (moveState)
        //{
        //    case MovementStates.runningOnGround:
        //        //we are here if the player is on the ground
        //        if (jump)
        //        {
        //            //on the ground and jump pressed, we can jump.
        //            moveState = MovementStates.airborne;
        //        }
        //        break;
        //    case MovementStates.airborne:
        //        //we are here if the character is airborne.

        //        break;
        //}

        //if(Input.GetButton("Jump") && moveState == MovementStates.runningOnGround)
        //{
        //    //meaning we are running on the ground

        //}
    }
    void FixedUpdate()
    {
        //constant movement forward. needs to be updated because of the fall. 

        Moving();

        
    }
    void Moving()
    {
        float h = Input.GetAxis("Horizontal");
        if (!movementDisabled)
        {
            rb.velocity = new Vector3(maxSpeed * h, rb.velocity.y);
        }
        
        //if (h * rb.velocity.x < maxSpeed)
        //{
        //    rb.AddForce(Vector3.right * h * moveForce);
        //}
        //if (Mathf.Abs(rb.velocity.x) > maxSpeed)
        //{
        //    rb.velocity = new Vector3(rb.velocity.x * maxSpeed, rb.velocity.y);
        //}
        if (jump && !movementDisabled)
        {
            //anim.SetTrigger("Jump");
            rb.AddForce(new Vector3(0.0f, jumpForce));
            jump = false;
        }
        if (walled)
        {
            StartCoroutine(HitTheRunnerBack());
        }
        //new from here, experimental

        //switch (moveState)
        //{
        //    case MovementStates.runningOnGround:
        //        //we are here if the player is on the ground
        //        if (h * rb.velocity.x < maxSpeed)
        //        {
        //            rb.AddForce(Vector3.right * h * moveForce);
        //        }
        //        if (Mathf.Abs(rb.velocity.x) > maxSpeed)
        //        {
        //            rb.velocity = new Vector3(rb.velocity.x * maxSpeed, rb.velocity.y);
        //        }
        //        break;
        //    case MovementStates.airborne:
        //        //we are here if the character is airborne.

        //        break;
        //    case MovementStates.hitTheWall:
        //        //we are here if the character touched a collider
        //        //that is marked ground, but the character isn't grounded
        //        //meaning it didn't hit the collider with its feet.
        //        //we just mark the X velocity zero to avoid the sticking.
        //        rb.velocity = new Vector3(0.0f, rb.velocity.y, rb.velocity.z);
        //        break;
        //}

    }
    //void OnCollisionEnter(Collision other)
    //{
    //    colliding = true;
    //    /*
    //     * The shit is that we are standing on the ground firmly only if 
    //     * the character meets a collider and the grounded is true.
    //     * 
    //     * Note that it takes time for the grounded to get updated and it does it at beginning 
    //     * of the Update function.
    //    */
    //    if (other.gameObject.CompareTag("Ground"))
    //    {
    //        //meaning we hit the ground
    //        if (!grounded)
    //        {
    //            //meaning we didn't hit it with our feet
    //            moveState = MovementStates.hitTheWall;

    //        }
    //    }
    //}
    void OnCollisionEnter(Collision other)
    {
        colliding = true;
        
        //we do this whole shit only if we are not walled
        //meaning if we didn't hit any walls. 
        //because if we did, we call this shit another time
        //and we wish to avoid calling it twice.
        //if (!walled)
        //{
        //    if (other.gameObject.CompareTag("Ground") && !grounded)
        //    {
        //        //meaning we hit the ground but we are not grounded
        //        StartCoroutine(HitTheRunnerBack());
        //    }
        //}

    }
    //void OnCollisionExit(Collision other)
    //{
    //    colliding = false;
    //}
    public IEnumerator HitTheRunnerBack()
    {
        movementDisabled = true;
        walled = false;
        colliding = false;
        rb.velocity = new Vector3(0.0f, 0.0f, 0.0f);
        rb.AddForce(new Vector3(-800.0f, 0.0f, 0.0f));
        yield return new WaitForSeconds(1.0f);
        movementDisabled = false;
    }
}
