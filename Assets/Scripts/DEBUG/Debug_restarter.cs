﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.DEBUG
{
    [RequireComponent(typeof(Canvas))]
    public class Debug_restarter : MonoBehaviour 
    {
        private Button restartButton;
        private UnityAction rest;
        public float selectedTimeScale = 1f;
        private void Start()
        {

            foreach (Transform c in transform)
            {
                if(c.GetComponent<Button>() != null)
                {
                    restartButton = c.GetComponent<Button>();
                    break;
                }
            }
            if(restartButton == null)
            {
                throw new MissingComponentException("No debug restart button found.");
            }
            rest += OnRestart;
            restartButton.onClick.AddListener(rest);

        }
        private void OnRestart()
        {
            Debug.Log("RELOAD CURRENT SCENE");
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name, LoadSceneMode.Single);
            //Time.timeScale = 1.0f;
        }
    }
}
