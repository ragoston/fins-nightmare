﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Statistics;

public class DevourScript : MonoBehaviour
{
    /*
     * The purpose of the current script is to control the target 
     * in a way that it resembles a devouring effect.
     * this can either kill the player, can move it, and so on.
     * It also shall be expandable.
     */
    [Header("Assign the devour destination if you want to set it to transport type.")]
    [Header("Give value to the transportDamping only if you wish to use the devour transport.")]
    public float transportDamping = 3.0f;
    [Tooltip("This needn't to be assigned. Assign if the devour type is transport. Needed for destination.")]
    public Transform devourDestination;
    
    public GameObject target;
    [Tooltip("For convenience if we don't want to check the <isTrigger> constantly, we can use this. Don't check this if you have a collider on your object that shall not be a trigger.")]
    public bool initializeTriggers = true;
    [Tooltip("The distance from which if the target is closer to the destination, the moving stops.")]
    public float reachTreshold = 0.1f;
    public enum TargetType
    {
        player
    }
    public TargetType currentTargetType = TargetType.player;

    public enum DevourerType
    {
        killer, transport
    }
    [Tooltip("How shall the devourer object work?")]
    public DevourerType whichDevourer = DevourerType.killer;
    [Tooltip("The speed of the devour. lower value means faster process.")]
    public float damping = 1.0f;
    private Vector3 currentVelocity;
    private bool wasUsed = false;
    public bool getBackNeededWhenTransport = true;
    DataBank bank;
    void Start()
    {
        switch (currentTargetType)
        {
            case TargetType.player:
                target = GameObject.FindGameObjectWithTag("Player");
                if (target == null)
                {
                    Debug.Log("You may want to set up the player character's tag or give a player to the scene.");
                }
                break;
        }

        if (initializeTriggers)
        {
            /*
             * if we are here we might want to get the collisions on the script's holder
             * and set them to trigger.
             */
            GetComponent<Collider2D>().isTrigger = true;

        }


    }
    //public void InitMe(DataBank bank)
    //{
    //    this.bank = bank;
    //    switch (currentTargetType)
    //    {
    //        case TargetType.player:
    //            target = GameObject.FindGameObjectWithTag("Player");
    //            if (target == null)
    //            {
    //                Debug.Log("You may want to set up the player character's tag or give a player to the scene.");
    //            }
    //            break;
    //    }

    //    if (initializeTriggers)
    //    {
    //        /*
    //         * if we are here we might want to get the collisions on the script's holder
    //         * and set them to trigger.
    //         */
    //        GetComponent<Collider2D>().isTrigger = true;

    //    }


    //}
    /*
     * This shit must be event driven, using a collider. 
     * Any type of collider will do, but it shall be a trigger.
     */
    void OnTriggerEnter2D(Collider2D other)
    {

        if (currentTargetType == TargetType.player)
        {
            if (!wasUsed && other.gameObject.CompareTag("Player"))
            {
                wasUsed = true;
                switch (whichDevourer)
                {
                    case DevourerType.killer:
                        /*
                        * we are here if we are using a killer devourer.
                        */
                        Debug.Log("KILLED");
                        other.gameObject.GetComponent<CharacterController2D>().playerSmoke.SetActive(true);
                        //used to be like this
                        //StartCoroutine(WaitThenFailLevel());
                        //until this before using the singleton pattern.
                        StartCoroutine(SceneGameManager.Instance.WaitThenFailLevel());

                        break;
                    case DevourerType.transport:
                        StartCoroutine(Transport(getBackNeededWhenTransport));

                        break;
                }
            } 
        }

    }
    IEnumerator GetDevouredAndDie()
    {
        if (!CharacterController2D.playerDied)
        {
            CharacterController2D.playerDied = true;

            if (CharacterController2D.animState != CharacterController2D.CharAnimStates.fall)
            {
                CharacterController2D.currentImageIndex = 0;
                CharacterController2D.animState = CharacterController2D.CharAnimStates.fall;
            }
            CharacterController2D.MovementDisabledActions();
            //CharacterController2D.movementDisabled = true;
            yield return new WaitForSeconds(0.2f);
            /*
             * when everything else is set, we shall get to the devour part.
             * it means that the target's rigidbody - if has one - gets removed
             * and the target gradually moves towards the devourer.
             */
            StartCoroutine(Devour());
            StartCoroutine(ScaleTime(1.0f, 0.0f, 3.0f));
            StartCoroutine(ScaleSpriteAlpha(1.0f, 0.0f, 0.5f));
        }

    }
    IEnumerator ScaleTime(float start, float end, float time)
    {
        float lastTime = Time.realtimeSinceStartup;
        float timer = 0.0f;

        while (timer < time)
        {
            //scaling down time 
            Time.timeScale = Mathf.Lerp(start, end, timer / time);
            timer += (Time.realtimeSinceStartup - lastTime);
            lastTime = Time.realtimeSinceStartup;

            yield return null;
        }
        Time.timeScale = end;
    }
    IEnumerator ScaleSpriteAlpha(float start, float end, float time)
    {
        float lastTime = Time.realtimeSinceStartup;
        float timer = 0.0f;

        while (timer < time)
        {
            //fading out the character using its sprite renderer alpha
            CharacterController2D.charSpriteRenderer.color = new Color(
                CharacterController2D.charSpriteRenderer.color.r,
                CharacterController2D.charSpriteRenderer.color.g,
                CharacterController2D.charSpriteRenderer.color.b, Mathf.Lerp(start, end, timer / time));
            timer += (Time.realtimeSinceStartup - lastTime);
            lastTime = Time.realtimeSinceStartup;
            yield return null;
        }
    }
    IEnumerator Devour()
    {
        Rigidbody2D targetRB = target.GetComponent<Rigidbody2D>();
        if (targetRB == null)
        {
            //meaning we didn't find a rigidbody on the target
            //then we seek it on its children.
            targetRB = target.GetComponentInChildren<Rigidbody2D>();
        }
        if (targetRB != null)
        {
            //if we found a rigidbody anyway
            targetRB.gravityScale = 0.0f;
            Destroy(targetRB);
        }
        while (target.transform.position != transform.position)
        {
            target.transform.position = Vector3.SmoothDamp(target.transform.position, transform.position, ref currentVelocity, damping);
            yield return null;
        }
    }
    IEnumerator Transport(bool getBackNeeded)
    {
        switch (currentTargetType)
        {
            case TargetType.player:
                CharacterController2D.charMovement.movementDisabled = true;
                break;
        }
        Rigidbody2D targetRB = target.GetComponent<Rigidbody2D>();
        if (targetRB == null)
        {
            //meaning we didn't find a rigidbody on the target
            //then we seek it on its children.
            targetRB = target.GetComponentInChildren<Rigidbody2D>();
        }
        float targetRBGravScaleOriginal = 0.0f;
        if (targetRB != null)
        {
            //if we found a rigidbody anyway
            //we just disable it temporarily
            targetRBGravScaleOriginal = targetRB.gravityScale;
            targetRB.velocity = Vector3.zero;
            targetRB.gravityScale = 0.0f;
        }
        switch (currentTargetType)
        {
            case TargetType.player:
                CharacterController2D.charMovement.movementDisabled = true;
                StartCoroutine(ScaleSpriteAlpha(1.0f, 0.0f, 1.0f));

                break;
        }
        while (Vector3.Distance(target.transform.position, transform.position) > reachTreshold)
        {
            target.transform.position = Vector3.SmoothDamp(target.transform.position, transform.position, ref currentVelocity, damping);
            yield return null;
        }
        //hopefully now the target reached the destination.
        if (devourDestination != null)
        {
            while (Vector3.Distance(target.transform.position, devourDestination.position) > reachTreshold)
            {
                target.transform.position = Vector3.SmoothDamp(target.transform.position, devourDestination.position,
                    ref currentVelocity, transportDamping);
                yield return null;
            }
        }
        if (getBackNeeded)
        {
            StartCoroutine(GetBackFromTransport(0.5f, targetRB, targetRBGravScaleOriginal));
        }

    }
    IEnumerator GetBackFromTransport(float time, Rigidbody2D targetRB, float oldRBGravityScale)
    {
        StartCoroutine(ScaleSpriteAlpha(0.0f, 1.0f, time));
        yield return new WaitForSeconds(time);
        switch (currentTargetType)
        {
            case TargetType.player:
                CharacterController2D.charMovement.movementDisabled = false;               
                break;
        }
        if (targetRB != null)
        {
            targetRB.gravityScale = oldRBGravityScale;
        }
    }
}
