﻿using UnityEngine;
using System.Collections;
using Assets.MainMenuStuff;

public class DoubleJumpTrambouline : MonoBehaviour
{
    public bool didThePlayerJump;
    public bool jumpDone;
	// Use this for initialization
	void Start ()
    {
        didThePlayerJump = false;
	}
	
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && 
            /*Input.GetButton("Jump")*/ JumpInput.JUMP)
        {
            //meaning the player has entered the collider
            //where we can decide if we want to double jump
            //using this trampoine
            
            didThePlayerJump = true;
            if (jumpDone)
            {
                Rigidbody2D otherRB = other.gameObject.GetComponent<Rigidbody2D>();
                otherRB.velocity = new Vector2(otherRB.velocity.x, CharacterController2D.charMovement.jumpSpeed * 1.3f);
                jumpDone = false;
            }
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            didThePlayerJump = false;
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        jumpDone = false;
    }
}
