﻿using UnityEngine;
using System.Collections;

public class ElectricPoles : MonoBehaviour
{
    private SpriteRenderer electricSprite;
    private Sprite[] electricities;    
    private int currentImageIndex;
    private float currentAlpha;
    private Sprite removedSprite;
    [HideInInspector]
    public bool animationEnabled = true;
    private bool alreadyStarted;
    public GameObject ElectricityGO;
	// Use this for initialization
	void Start ()
    {
        currentImageIndex = 0;
        electricities = Resources.LoadAll<Sprite>("Electricity");
        electricSprite = ElectricityGO.GetComponent<SpriteRenderer>();
    }
	
    void LateUpdate()
    {
        if (true)
        {
            AnimateSequenceSetup();
        }
    }
    void AnimateSequenceSetup()
    {
        currentImageIndex = Mathf.RoundToInt(Time.time * 14.0f);
        currentImageIndex = currentImageIndex % electricities.Length;
        electricSprite.sprite = electricities[currentImageIndex];
    }
}