﻿using UnityEngine;
using System.Collections;

public class EnvironmentController : MonoBehaviour
{
    public Transform MainCamTransform;
    public Transform SkyTransform;
    //public GameObject horde;
    public GameObject playerCharacter;
    public float hordeSpeed;

    private Rigidbody2D hordeRB;
    //private Rigidbody2D playerRB;
    private bool playerMovementStatusDisabled;
    private SpriteRenderer hordeCurrentImage;

	// Use this for initialization
    void Start()
    {
        hordeSpeed = 6.0f;
        //hordeRB = horde.GetComponent<Rigidbody2D>();
        //playerRB = playerCharacter.GetComponent<Rigidbody2D>();
        //hordeImageSequence = Resources.LoadAll<Sprite>("HordeGIF");
        //hordeCurrentImage = horde.GetComponentInChildren<SpriteRenderer>();
    }
	// Update is called once per frame
	void Update ()
    {
        //SkyTransform.position = new Vector3(MainCamTransform.position.x, MainCamTransform.position.y, SkyTransform.position.z);
        //currentImageIndex = Mathf.RoundToInt(Time.time * 24.0f);
        //currentImageIndex = currentImageIndex % hordeImageSequence.Length;
        //hordeCurrentImage.sprite = hordeImageSequence[currentImageIndex];
	}
    //void FixedUpdate()
    //{
    //    SkyTransform.position = new Vector3(MainCamTransform.position.x, MainCamTransform.position.y, SkyTransform.position.z);
    //}
    void LateUpdate()
    {
        //we need to modify the horde movement here so
        //it doesn't interefer with the player's movement
        /*
         * There are two states here for the horde.
         * There is one, which just gives the horde the speed of the player. 
         * There is this other, when the play tipped and stopped. 
         * If the horde continued with the same speed, the player would get
         * caught way too easily.
         * Instead we show the horde down when the player is in trouble.
         * Except if we are done. Then they run us over.
         * *** IMPORTANT ***
         * we know exactly when the player is in deep shit. When the movement is disabled.
         * 
        */

        //course we need to constantly update the change of this variable

        //if (CharacterController2D.movementDisabled)
        //{
        //    //if so, we casually move forward, just a wee bit slower.
        //    hordeRB.velocity = Vector2.right * hordeSpeed;
        //}else
        //{
        //    hordeRB.velocity = Vector2.right * playerRB.velocity.x;
        //}
        //horde.transform.position = new Vector2(horde.transform.position.x, MainCamTransform.transform.position.y);

    }
    //IEnumerator WaitThenSwapSprites()
    //{
    //    yield return new WaitForSeconds(0.042f);
    //    hordeCurrentImage.sprite = hordeImageSequence[currentImageIndex];
    //    currentImageIndex++;
    //    if (currentImageIndex >= hordeImageSequence.Length)
    //    {
    //        currentImageIndex = 0;
    //    }
    //}
}
