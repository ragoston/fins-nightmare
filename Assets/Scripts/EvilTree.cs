﻿using UnityEngine;
using System.Collections;

public class EvilTree : MonoBehaviour
{
    public GameObject eyeCover;
    public GameObject mouthCover;
    private bool hadYourFun = false;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && !hadYourFun)
        {
            hadYourFun = true;
            //Debug.Log("Player entered evil tree.");
            int dice = Random.Range(0, 10);
            if(dice >= 7)
            {
                StartCoroutine(WaitThenTreeWakeUp());
            }           
        }
    }
    IEnumerator WaitThenTreeWakeUp()
    {
        //eyeCover
        //11.3
        while((eyeCover.transform.position.y < 11.3f) || (mouthCover.transform.position.y > 6.25f))
        {
            eyeCover.transform.position += Vector3.up * 0.1f;
            mouthCover.transform.position += Vector3.down * 0.13f;
            yield return new WaitForSeconds(0.03f);
        }
    }
}
