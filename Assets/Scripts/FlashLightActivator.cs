﻿using UnityEngine;
using System.Collections;

public class FlashLightActivator : MonoBehaviour
{
    public GameObject flashLight;

    private BoxCollider2D caveEnterCollider;

    FlashLightClass currentLight;

    private bool isEntering;
    // Use this for initialization
    void Start()
    {
        isEntering = true;
        caveEnterCollider = GetComponent<BoxCollider2D>();
        //so we only have one collider to start with.
        caveEnterCollider.enabled = true;
        currentLight = new FlashLightClass(flashLight);
    }
    int enters = 0;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            enters++;
            if (isEntering)
            {
                /*
                 *** SHIT TO DO: ***
                 * - parent the flashlight under the player
                 * - enable the flashlight.
                 * - fade da lil bitch in   
                */

                isEntering = false;
                currentLight.ActivateLightEvents(other.gameObject);
                StartCoroutine(currentLight.FadeLightIn(currentLight.deltaAlpha, 0.55f));
                caveEnterCollider.enabled = false;
                //at the end of this we hopefully
                //parented the light under the player and
                //it should follow the player without any issues.
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if(enters >= 2)
            {
                StartCoroutine(currentLight.FadeLightOut(0.55f));
                StartCoroutine(WaitThenDestroyLightMaster());
            }
        }
    }
    IEnumerator WaitThenDestroyLightMaster()
    {
        yield return new WaitForSeconds(4.0f);
        Destroy(this.gameObject);
    }

}
public class FlashLightClass
{
    public GameObject flashLight;
    public SpriteRenderer lightSprite;
    public float deltaAlpha = 0.03f;

    public FlashLightClass(GameObject inputLight)
    {
        flashLight = inputLight;
        lightSprite = flashLight.GetComponent<SpriteRenderer>();
        lightSprite.color = new Color(lightSprite.color.r, lightSprite.color.g, lightSprite.color.b, 0);
        flashLight.SetActive(false);
    }
    public IEnumerator FadeLightIn(float increment, float upperLimit)
    {
        if(upperLimit <= 0.0f)
        {
            upperLimit = 0.7f;
        }
        while(lightSprite.color.a <= upperLimit)
        {
            lightSprite.color = new Color(lightSprite.color.r, lightSprite.color.g, lightSprite.color.b,
                lightSprite.color.a + increment);
            yield return new WaitForSeconds(0.03f);
        }
    }
    public void ActivateLightEvents(GameObject playerGO)
    {
        flashLight.SetActive(true);
        flashLight.transform.parent = playerGO.transform;
        flashLight.transform.localPosition = Vector3.zero;                
        //StartCoroutine(FadeLightIn(0.017f));
    }
    public IEnumerator FadeLightOut(float decrement)
    {
        while (lightSprite.color.a > 0.0f)
        {
            if(lightSprite.color.a - decrement < 0.0f)
            {
                lightSprite.color = new Color(lightSprite.color.r, lightSprite.color.g, lightSprite.color.b, 0);
            }else
            {
                lightSprite.color = new Color(lightSprite.color.r, lightSprite.color.g, lightSprite.color.b,
                lightSprite.color.a - decrement);
            }           
            yield return new WaitForSeconds(0.03f);
        }
        //we are here if the light has been successfully deactivated.
        flashLight.SetActive(false);
        MonoBehaviour.Destroy(flashLight);
    }
}
