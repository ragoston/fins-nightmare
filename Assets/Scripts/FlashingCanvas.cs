﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FlashingCanvas : MonoBehaviour
{
    public Canvas flashCanvas;
    private Image flashImage;

    public enum DurTypes
    {
        byFrames, bySeconds
    }
    public DurTypes durationTypes = DurTypes.byFrames;
    public float FPS_ifByFrames = 24.0f;

    // Use this for initialization
    void Start ()
    {
        if (flashCanvas == null)
        {
            try
            {
                flashCanvas = this.GetComponent<Canvas>();
            }catch
            {
                Debug.Log("Canvas not found");
                Destroy(this.gameObject);
            }
        }
        
        flashImage = flashCanvas.transform.Find("creatureImage").GetComponent<Image>();

	}
	
    
}
