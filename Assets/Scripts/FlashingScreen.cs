﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class FlashingScreen : MonoBehaviour
{
    public Canvas flashingCanvas;
    private Image creatureImage;

    public float[] imagesDuration;
    public GameObject[] imagesToAppear;
    private bool alreadyUsed = false;
    public enum FlashingType
    {
        regular,canvas
    }
    public FlashingType flashType = FlashingType.regular;
    public enum DurTypes
    {
        byFrames,bySeconds
    }
    public DurTypes durationTypes = DurTypes.byFrames;
    public float FPS_ifByFrames = 24.0f;
    //needed when the user messed up and gave less durations than the images number.
    private float[] overrideImagesDuration;
    public float[] delayBetweenImages;
    private float[] overrideDelayBetweenImages;

    private float canvasImageAlpha;
	void Start ()
    {
        if(flashingCanvas == null && flashType == FlashingType.canvas)
        {
            //meaning we need a canvas but we do not have one
            Debug.Log("Need to specify a canvas on a flashing screen.");
            Destroy(this.gameObject);
        }
        if(flashType == FlashingType.canvas && flashingCanvas != null)
        {
            creatureImage = flashingCanvas.transform.Find("creatureImage").GetComponent<Image>();
            //we use it by setting the desired alpha via the editor.
            //then here we store it and give the colour the exact same alpha to appear.
            canvasImageAlpha = creatureImage.color.a;
            creatureImage.color = new Color(creatureImage.color.r, creatureImage.color.g, creatureImage.color.b, 0.0f);
            flashingCanvas.gameObject.SetActive(false);
            //we also have to hide the images from the scene.
            //they are just stored there.
            if (!creatureImage.enabled)
            {
                creatureImage.enabled = true;
            }
            for(int i = 0; i < imagesToAppear.Length; i++)
            {
                imagesToAppear[i].GetComponent<SpriteRenderer>().color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
            }

        }
        /*
         * from here we just make sure to corrent the user
         * if they put in a smaller value for the images duration
         * than the number of images we have.
         */
        CorrectImageNumbers(imagesDuration, overrideImagesDuration);

        /*
         * Made sure until here.
         */

        //we can input for how many frames we want the images to appear.
        //or we can do it by seconds.
        //the system will convert along the given input.
        switch (durationTypes)
        {
            case DurTypes.byFrames:
                /*
                 * if we want frames, we convert the input to seconds
                 * using the frame rate we put in.
                 */
                if(FPS_ifByFrames <= 0)
                {
                    //making sure the user doesn't fuck this up.
                    FPS_ifByFrames = 24.0f;
                }
                for(int i = 0; i < imagesDuration.Length; i++)
                {
                    imagesDuration[i] = (imagesDuration[i] / FPS_ifByFrames);
                }
                break;
        }
        //now we should have the time delays that show the duration of the images visible.

        //exact same shit for the duration between the images. it just has to be one less longer than the others
        //as it is only BETWEEN them images. but sod it, one value doesn't matter.
        CorrectImageNumbers(delayBetweenImages, overrideDelayBetweenImages);
        foreach(GameObject item in imagesToAppear)
        {
            //setting all the shit to invisible.
            item.SetActive(false);
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (!alreadyUsed)
            {
                alreadyUsed = true;
                switch (flashType)
                {
                    case FlashingType.regular:
                        StartCoroutine(InitiateImageVisibility());

                        break;
                    case FlashingType.canvas:
                        //reactivating the canvas
                        flashingCanvas.gameObject.SetActive(true);
                        //starting fucking around with the images
                        StartCoroutine(InitiateCanvasImageVisibility());
                        break;
                }
                //either way we destroy this shit, but not the canvas.
                //the canvas might be needed again.
                StartCoroutine(WaitThenDestroySelf());
            }
        }
    }

    IEnumerator MakeImageAppear(GameObject objWithImage, float imageVisibleTime)
    {
        if (!CharacterController2D.playerDied)
        {
            objWithImage.SetActive(true);
            yield return new WaitForSeconds(imageVisibleTime);
            objWithImage.SetActive(false);
        }       
    }
    IEnumerator InitiateImageVisibility()
    {
        for (int i = 0; i < imagesToAppear.Length; i++)
        {
            StartCoroutine(MakeImageAppear(imagesToAppear[i],imagesDuration[i]));
            yield return new WaitForSeconds(delayBetweenImages[i]);

        }
    }
    /*
     * now similar shit for the canvas setup
     */ 
    IEnumerator MakeCanvasImageAppear(GameObject objWithImage, float imageVisibleTime)
    {
        if (!CharacterController2D.playerDied)
        {
            //we need to set the canvas' image to the given one.
            creatureImage.sprite = objWithImage.GetComponent<SpriteRenderer>().sprite;
            creatureImage.color = new Color(creatureImage.color.r, creatureImage.color.g, creatureImage.color.b,
                canvasImageAlpha);
            yield return new WaitForSeconds(imageVisibleTime);
            creatureImage.color = new Color(creatureImage.color.r, creatureImage.color.g, creatureImage.color.b,
                0.0f); 
        }
    }
    IEnumerator InitiateCanvasImageVisibility()
    {
        for (int i = 0; i < imagesToAppear.Length; i++)
        {
            StartCoroutine(MakeCanvasImageAppear(imagesToAppear[i], imagesDuration[i]));
            yield return new WaitForSeconds(delayBetweenImages[i]);

        }
    }
    IEnumerator WaitThenDestroySelf()
    {
        float whenToDestroy;
        whenToDestroy = delayBetweenImages[0];
        //we need to destroy only after the longest one finished and we did all.
        for(int i = 1; i < delayBetweenImages.Length; i++)
        {
            if (delayBetweenImages[i] > whenToDestroy)
            {
                whenToDestroy = delayBetweenImages[i];
            }
        }

        yield return new WaitForSeconds(whenToDestroy+1.0f);
        Destroy(this.gameObject);
    }
    void CorrectImageNumbers(float[] array, float[] helperArray)
    {
        if (array.Length < imagesToAppear.Length)
        {
            //meaning we fucked up the size of the array to hold
            //how long for the images to be visible.
            //first we allocate memory:
            helperArray = new float[imagesToAppear.Length];
            //then we put in the values that already exist.
            for (int i = 0; i < array.Length; i++)
            {
                helperArray[i] = array[i];
            }
            for (int j = array.Length/*so the element after last*/;
                j < helperArray.Length/*has to end before the last*/; j++)
            {
                //giving the time duration of one frame.
                helperArray[j] = 0.042f;
            }
            //to be easy on the code, we put the override into the original
            //and free up the override.
            array = null;
            array = new float[helperArray.Length];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = helperArray[i];
            }
            helperArray = null;
        }
        else
        {
            //if not needed we free up the memory.
            helperArray = null;
        }
    }

}
