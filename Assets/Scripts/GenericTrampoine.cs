﻿using UnityEngine;
using System.Collections;
using Assets.MainMenuStuff;
using Assets.JumpAssist.Trampolines;
using System;

public class GenericTrampoine : MonoBehaviour, ITrampolineFunctionality
{
    [Tooltip("Decide if we have to play the occasional animation backwards after finishing it.")]
    public bool backWardsNeeded = true; // decides if we have to play the animation backwards after finishing it.
    private bool hasImagePlayer;
    private ImageSequencePlayer imagePlayer;
    [Tooltip("tell if we need a delay on the trampoine effect.")]
    public bool delayNeeded = false;
    [Tooltip("The time we shall wait for the trampoine to take effect")]
    public float trampoineDelay = 0.0f;
    public float jumpedMultiplier = 1.3f;
    public float idleMultiplier = 1f;

    public Func<Rigidbody2D, Vector2> GetSimpleJump
    {
        get;set;
    }

    public Func<Rigidbody2D, Vector2> GetDoubleJump { get; set; }

    void Start()
    {
        imagePlayer = GetComponent<ImageSequencePlayer>();
        if(imagePlayer == null)
        {
            imagePlayer = GetComponentInChildren<ImageSequencePlayer>();
        }
        if(imagePlayer == null)
        {
            //we didn't find it
            hasImagePlayer = false;
        }else
        {
            hasImagePlayer = true;
        }

        GetSimpleJump = (otherRB) => {
            return new Vector2(otherRB.velocity.x,
            CharacterController2D.charMovement.jumpSpeed * idleMultiplier);
        };

        GetDoubleJump = (otherRB) =>
        {
            return new Vector2(otherRB.velocity.x,
                CharacterController2D.charMovement.jumpSpeed * jumpedMultiplier);
        };
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && !CharacterController2D.charMovement.movementDisabled 
            && !CharacterController2D.playerDied)
        {
            //meaning the player is inside the field that can set our velocity
            Rigidbody2D otherRB = other.gameObject.GetComponent<Rigidbody2D>();
            if (/*Input.GetButton("Jump")*/ JumpInput.JUMP)
            {
                //otherRB.velocity = new Vector2(otherRB.velocity.x, CharacterController2D.jumpSpeed * 1.3f);
                //otherRB.velocity = new Vector2(otherRB.velocity.x, CharacterController2D.charMovement.jumpSpeed * jumpedMultiplier);
                otherRB.velocity = GetDoubleJump.Invoke(otherRB);
            }
            else
            {
                if (delayNeeded)
                {
                    StartCoroutine(WaitThenTakeEffect(otherRB));
                }else
                {
                    //otherRB.velocity = new Vector2(otherRB.velocity.x, CharacterController2D.jumpSpeed);
                    //otherRB.velocity = new Vector2(otherRB.velocity.x, CharacterController2D.charMovement.jumpSpeed * idleMultiplier);
                    otherRB.velocity = GetSimpleJump.Invoke(otherRB);
                }
               
            }
            if (hasImagePlayer && !imagePlayer.occasionCurrentlyInUse)
            {
                imagePlayer.occasionCurrentlyInUse = true;
                //then we need to play the occasional animations.
                imagePlayer.OccasionalAnimation(backWardsNeeded);
            }
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //meaning the player has left the trigger area
            StopAllCoroutines();
            /*
             * it means we do not want the trampoine to take effect anymore.
             * this way we can avoid adding the amount of force we wanted to multiple times.
             */ 
        }
    }
    IEnumerator WaitThenTakeEffect(Rigidbody2D otherRB)
    {
        /*
         * we wait
         * then IF THE PLAYER IS STILL IN THE AREA
         * only then we launch them.
         */
        yield return new WaitForSeconds(trampoineDelay);
        otherRB.velocity = new Vector2(otherRB.velocity.x, CharacterController2D.charMovement.jumpSpeed);
    }
}
