﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;

public class GhostControl : MonoBehaviour
{
    private Transform ghost;
    public Transform to;
	void Start ()
    {
        foreach (GameObject c in transform)
        {
            if(c.GetComponent<SpriteRenderer>().sprite.name == "wraith")
            {
                ghost = c.transform;
                break;
            }
        }
        if(ghost == null) { Debug.Log("found no ghost on the gameobject.");Destroy(this.gameObject); }
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            MoveAction ma = new MoveAction(ghost, to.position, 1f);
        }
        
    }
}
