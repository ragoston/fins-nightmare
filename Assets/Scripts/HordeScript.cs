﻿using UnityEngine;
using System.Collections;

public class HordeScript : MonoBehaviour
{
    
	// Use this for initialization
	void Start ()
    {
	    
	}
	
	// Update is called once per frame
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<CharacterController2D>().playerSmoke.SetActive(true);
            
        }
    }
}
