﻿using UnityEngine;
using System.Collections;

public class ImageSequencePlayer : MonoBehaviour
{
    public delegate void EventDuringAnim(int frameToDoAction);
    public EventDuringAnim animFinished;

    public bool onlyOnce = false;
    private bool alreadyPlayed = false;
    public int currentImageIndex;
    private SpriteRenderer spriteRenderer;
    public Sprite[] spriteSheet;
    [Tooltip("The location of your main spritesheet.")]
    public string spritesLocation;
    public float frameRate = 24.0f;
    public enum PlayMode
    {
        order, random, uponNeeded, randomlyStart
    }
    public PlayMode playMode = PlayMode.order;
    private bool updateEnabled = true;
    [Tooltip("Decides if the random playback can iterate. If no, it doesn't play, it is frozen.")]
    public bool canIterate = true; //if the random playback can iterate. if no, it doesn't play.
    [Tooltip("Is there some non looping animation before the loop? If so, right after it's done, it starts looping.")]
    public bool warmUp = false;
    [Tooltip("If you have a warmup sheet location, put it here.")]
    public string warmUpLocation = "";
    private Sprite[] warmUpSprites;
    [Tooltip("If we need an event after the animation.")]
    public bool animEventNeeded = false;

    public bool occasionCurrentlyInUse = false;
    [Tooltip("The value that defines the least time between possible playbacks when using playmode randomlyStart.")]
    public float randomlyStart = 0.1f;
    [Tooltip("The value that defines the most time between possible playbacks when using playmode randomlyStart.")]
    public float randomlyStop = 1.0f;
	void Start ()
    {
        try
        {
            spriteSheet = null;
            spriteSheet = Resources.LoadAll<Sprite>(spritesLocation);
        }
        catch(MissingSpriteSheetException ex)
        {
            Debug.Log(ex.Message);
        }

        
        spriteRenderer = GetComponent<SpriteRenderer>();
        if(spriteRenderer == null)
        {
            /*
             * we are here if we could not find a sprite renderer on the
             * gameobject that the script is attached to.
             * if so the renderer reference is a null.
             * to solve this we shall find the renderer component
             * among the children.
             */
            spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        }
        
        

        if(warmUp)
        {
            warmUpSprites = Resources.LoadAll<Sprite>(warmUpLocation);
            //meaning we do have something to warm up
        }
        else
        {
            //so if we did want some warmup, we load it,
            //if we didn't, we just free the memory.
            warmUpSprites = null;
            warmUpLocation = null;
        }
        if(playMode == PlayMode.uponNeeded)
        {
            //if we need it occasionally, we just disable everything else.
            warmUp = false;
            updateEnabled = false;
            warmUpLocation = null;
            warmUpSprites = null;
        }
	}
    private void OnDisable()
    {
        occasionCurrentlyInUse = false;
        alreadyPlayed = false;
        updateEnabled = true;
    }
    void LateUpdate ()
    {
        if (warmUp)
        {
            currentImageIndex = Mathf.RoundToInt(Time.time * frameRate);
            currentImageIndex = currentImageIndex % warmUpSprites.Length;
            spriteRenderer.sprite = warmUpSprites[currentImageIndex];
            if(currentImageIndex >= warmUpSprites.Length-1)
            {
                currentImageIndex = 0;
                warmUp = false;
                //now easing on the memory, not another warmup will happen of course:
                warmUpLocation = null;
                warmUpSprites = null;
            }
        }
        if (updateEnabled && !warmUp)
        {
            switch (playMode)
            {
                case PlayMode.order:
                    currentImageIndex = Mathf.RoundToInt(Time.time * frameRate);
                    currentImageIndex = currentImageIndex % spriteSheet.Length;
                    spriteRenderer.sprite = spriteSheet[currentImageIndex];
                    break;
                case PlayMode.random:
                    updateEnabled = false;
                    StartCoroutine(RandomizedPlay());
                    break;
                case PlayMode.randomlyStart:
                    //here the shit starts and plays, but
                    //starts randomly.
                    if (!occasionCurrentlyInUse)
                    {
                        occasionCurrentlyInUse = true;
                        /*
                         * it means we aren't currently playing it.
                         * in that case we dice out a value 
                         * and call a coroutine that will call the event triggered coroutine
                         * after waiting for the given time.
                         */
                         float randomTime = Random.Range(randomlyStart, randomlyStop);
                        StartCoroutine(WaitThenPlayOccasion(false, randomTime));
                    }
                    break;
            } 
        }

    }
    IEnumerator RandomizedPlay()
    {
        int oldIndex = 0;
        int currentIndex;
        int iterNumber = 0;
        while (canIterate)
        {
            currentIndex = Random.Range(0, spriteSheet.Length - 1);
            if(currentIndex == oldIndex)
            {
                while((currentIndex == oldIndex) && (iterNumber < 8))
                {
                    currentIndex = Random.Range(0, spriteSheet.Length - 1);
                    iterNumber++;
                }
            }
            spriteRenderer.sprite = spriteSheet[currentIndex];

            oldIndex = currentIndex;
            iterNumber = 0;
            yield return new WaitForSeconds(1.0f / frameRate);
        }
    }
    public void OccasionalAnimation(bool backWardsNeeded)
    {
        if (!onlyOnce)
        {
            StartCoroutine(OccasionalAnimEnum(backWardsNeeded));
        }else
        {
            if (!alreadyPlayed)
            {
                alreadyPlayed = true;
                StartCoroutine(OccasionalAnimEnum(backWardsNeeded));
            }
        }

    }
    public IEnumerator OccasionalAnimEnum(bool backWardsNeeded)
    {
        SpringTrap trap = null;
        bool shallWeTry = false;
        if (animEventNeeded)
        {
            //this is needed in case the we set an event request somewhere we don't really need and 
            //have no trap script.
            shallWeTry = true;
            try
            {
                //we try to give value to the trap. if we can't, we do nothing afterwards.
                trap = GetComponent<SpringTrap>();
                if (trap == null)
                {
                    trap = GetComponentInChildren<SpringTrap>();
                }

            }
            catch
            {
                trap = null;
                shallWeTry = false;
            }
            if (shallWeTry)
            {
                animFinished += trap.AnimationEvents;
                //if (animFinished != null)
                //{
                //    animFinished(currentImageIndex);
                //}
            }

        }
        currentImageIndex = 0;
        while (currentImageIndex < spriteSheet.Length)
        {
            //meaning while we do have anything to play
            spriteRenderer.sprite = spriteSheet[currentImageIndex];
            currentImageIndex++;
            if(animEventNeeded && shallWeTry)
            {
                //we are here if we have an event during animation tied to a frame.
                if(animFinished != null)
                {
                    animFinished(trap.frameToFireEvent);
                }
            }
            yield return new WaitForSeconds(1.0f / frameRate);
        }
        //if we are here than the towards animation has finished playing.
        if(animFinished != null)
        {
            //this should be edited to have general use.
            //now we add another event trigger for the after anim events.
            animFinished(currentImageIndex);
        }
        if (backWardsNeeded)
        {
            //so we need to play the shit backwards as well, like in the book
            currentImageIndex = spriteSheet.Length - 2;//so we won't repeat the last again.

            while (currentImageIndex >= 0)
            {
                //meaning while we do have anything to play
                spriteRenderer.sprite = spriteSheet[currentImageIndex];
                currentImageIndex--;
                yield return new WaitForSeconds(1.0f / frameRate);
            }
        }
        occasionCurrentlyInUse = false;
    }
    IEnumerator WaitThenPlayOccasion(bool backWardsNeeded, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        OccasionalAnimation(backWardsNeeded);
    }
}
public class MissingSpriteSheetException : System.Exception
{
    public MissingSpriteSheetException()
        :base("The sprite sheet's location is invalid.")
    {
        Time.timeScale = 0.0f;
        Debug.Log("Missing spritesheet. Do something about it.");
    }
}