﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using UnityEngine;

namespace Assets.Scripts.InGameInit
{
    [RequireComponent(typeof(InGameUIController))]
    public class InitMainMenu : InitializeMeIngame
    {
        public override void INITIALIZE(DataBank bank)
        {
            GetComponent<InGameUIController>().InitActions(bank);
        }
    }
}
