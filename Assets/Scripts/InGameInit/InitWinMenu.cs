﻿using System;
using Assets.Scripts.InGameInit;
using Assets.Scripts.Statistics;
using UnityEngine;

namespace Assets.Scripts.AdditionalGameUI
{
    [RequireComponent(typeof(WinCanvas))]
    public class InitWinMenu : InitializeMeIngame
    {
        public override void INITIALIZE(DataBank bank)
        {
            GetComponent<WinCanvas>().Initialize(bank);
        }
    }
}
