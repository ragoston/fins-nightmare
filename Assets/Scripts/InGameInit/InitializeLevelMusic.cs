﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using UnityEngine;
using Assets.MainMenuStuff.Audio;

namespace Assets.Scripts.InGameInit
{
    [RequireComponent(typeof(LevelMusic))]
    public class InitializeLevelMusic : InitializeMeIngame
    {
        public override void INITIALIZE(DataBank bank)
        {
            var m = GetComponent<LevelMusic>();
            m.Initialize();
            m.SetVolume(bank.Stats.Settings.MusicVolume);
            StartCoroutine(m.PlayMusic(m.mySource, m.myClip));
        }
    }
}
