﻿using Assets.MainMenuStuff.MMToolkitConsumers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Statistics;
using UnityEngine;
using Assets.MainMenuStuff.Audio;

namespace Assets.Scripts.InGameInit
{
    [RequireComponent(typeof(MusicPlayer))]
    public class InitializeMMMusic : InitializeMe
    {
        public override void INIT(INITIALIZER pers)
        {
            GetComponent<MusicPlayer>().Initialize();
        }
    }
}
