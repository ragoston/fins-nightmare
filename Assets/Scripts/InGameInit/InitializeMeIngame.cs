﻿using Assets.Scripts.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.InGameInit
{
    public abstract class InitializeMeIngame : MonoBehaviour
    {
        public int order;
        public abstract void INITIALIZE(DataBank bank);
    }
}
