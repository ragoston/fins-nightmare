﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using Assets.Scripts.Statistics;
using Assets.MainMenuStuff.Audio;
using Assets.AdManager;
using Assets.Managers;

public class InGameUIController : MonoBehaviour
{
    public static readonly int PauseTimePriority = 10;
    public Color playergMat;
    public Color midgMat;
    public Color bgMat;
    [HideInInspector]
    public GameObject progressBar; 
    public float blackPanelAlpha_paused = 0.415f;
    public float blackPanelAlpha_ended = 0.66f;
    [HideInInspector]
    public Image blackpanel;
    [HideInInspector]
    public GameObject retryPanel;
    [HideInInspector]
    public Image retryBut;
    [HideInInspector]
    public Image notRetryBut;
    [HideInInspector]
    public GameObject pauseImg;
    public float animTime = 1.0f;
    [HideInInspector]
    public GameObject exitButton;
    [HideInInspector]
    public Canvas mainUICanvas;
    [HideInInspector]
    public Quaternion exitIdleRot;
    [HideInInspector]
    public float exitIdleAlpha;
    public Sprite exitButtonExitSprite;
    public Sprite exitButtonIdleSprite;
    public IMusicProvider musicPlayer;
    public Sprite pausedSprite;
    public Sprite playSprite;
    [HideInInspector]
    public bool canClickExit = true;
    [HideInInspector]
    public GameObject yesnoPanel;
    [HideInInspector]
    public GameObject yesButton;
    [HideInInspector]
    public GameObject noButton;
    [HideInInspector]
    public enum ExitButtonStates
    {
        idle, pressed
    }
    //what to do at the end of a coroutine.
    [HideInInspector]
    public enum ExitButtonEvents
    {
        nothing, swapSpritesToExit, swapSpritesToIdle
    }
    [HideInInspector]
    public ExitButtonEvents exitButtonEvent = ExitButtonEvents.nothing;
    [HideInInspector]
    public ExitButtonStates exitState = ExitButtonStates.idle;
    //[HideInInspector]
    //public Persistence GameData;
    DataBank bank;
    private Text deathCountText;
    private Text currentResultText;
    public Image progressFill;
    ITimeScaler timeScaler;
    public void InitActions(DataBank bank)
    {
        /*
 * this has to be here on top as it works only on active GO-s.
 */
        this.bank = bank;
        //GameData = GameObject.FindGameObjectWithTag("GameData").GetComponent<Persistence>();
        SceneGameManager.Instance.UIRuntime = GameObject.FindGameObjectsWithTag("UIRuntime");
        SceneGameManager.Instance.UIRetry = GameObject.FindGameObjectsWithTag("UIRetry");
        ColorizeUI(SceneGameManager.Instance.UIRuntime, bank.Stats.LastPlayedLevel-1);
        ColorizeUI(SceneGameManager.Instance.UIRetry, bank.Stats.LastPlayedLevel-1);
        try
        {
            musicPlayer = GameObject.FindGameObjectWithTag("MainCamera").transform.
                Find("MusicObject").GetComponent(typeof(IMusicProvider)) as IMusicProvider;
        }
        catch { }
        /*
         * Has to be the same TimeScaler so the Routine is shared, meaning it only 
         * serves one coroutine at a time.
         */ 
        timeScaler = SceneGameManager.Instance.TimeScaler;
        //SceneGameManager.Instance.GetComponent<ProgressTracker>().progressFill = progressFill;

        blackpanel = transform.Find("BlackPanel").gameObject.GetComponent<Image>();
        blackpanel.color = new Color(blackpanel.color.r, blackpanel.color.g,
            blackpanel.color.b, 0.0f);
        blackpanel.gameObject.SetActive(false);
        pauseImg = transform.Find("Pause").gameObject;
        pauseImg.SetActive(false);
        yesnoPanel = transform.Find("yesnoPanel").gameObject;
        yesButton = yesnoPanel.transform.Find("yesBut").gameObject;
        noButton = yesnoPanel.transform.Find("noBut").gameObject;
        exitButton = transform.Find("exitButton").gameObject;
        yesnoPanel.SetActive(false);
        mainUICanvas = this.gameObject.GetComponent<Canvas>();
        exitIdleRot = exitButton.transform.rotation;
        exitIdleAlpha = exitButton.GetComponent<Button>().colors.normalColor.a;
        retryPanel = transform.Find("retryPanel").gameObject;
        retryBut = retryPanel.transform.Find("retryBut").gameObject.GetComponent<Image>();
        notRetryBut = retryPanel.transform.Find("notRetryBut").gameObject.GetComponent<Image>();
        bank.MMTools.DisplayDataTools.DoRecursivelyInGameObject((g) =>
        {
            var cr = g.transform.Find("cr");
            var dc = g.transform.Find("dc");
            if(cr != null && cr.GetComponent<Text>() != null)
            {
                currentResultText = cr.GetComponent<Text>();
            }
            if(dc != null && dc.GetComponent<Text>() != null)
            {
                deathCountText = dc.GetComponent<Text>();
            }
        }, gameObject);
        //currentResultText = transform.Find("cr").GetComponent<Text>();
        //deathCountText = transform.Find("dc").GetComponent<Text>();

        retryBut.color = new Color(retryBut.color.r, retryBut.color.g, retryBut.color.b, 0.0f);
        notRetryBut.color = new Color(notRetryBut.color.r, notRetryBut.color.g, notRetryBut.color.b, 0.0f);
        retryPanel.SetActive(false);
    }
    private void ColorizeUI(IEnumerable<GameObject> objs, int levelNum)
    {
        foreach (var item in objs)
        {
            bank.MMTools.Colorizer.RecolourUI(item, bank.baseColor[levelNum],
                bank.baseColor2[levelNum], bank.outlineColor[levelNum]);
        }
    }
    public void ExitButtonAnim()
    {
        if (canClickExit && !CharacterController2D.playerDied)
        {
            canClickExit = false;
            /*
             * this should gradually scale the button down
             * when the alpha is 0.5 of the idle, we swap the sprites
             * the rotation continues. By the time the alpha is back, the full
             * rotation should be made.
             */

            //the contents of exitactionsnorestriction were here.    

            /*
             * the stuff is modified only so the player can access it solely through the
             * restriction
             * while the game manager can anyhow.
             */

            ExitActionsNoRestriction();
#if DEBUG
            Debug.Log("Current timescale:" + Time.timeScale.ToString());
#endif
        }
    }
    public void ExitActionsNoRestriction()
    {
        YesNoPanelFading();
        //we are here when ingame
        SwitchPauseImg();
        switch (exitState)
        {
            case ExitButtonStates.idle:
                AdManager.ShowBannerAd();
                //StartCoroutine(ScaleTime(bank.MMTools.SceneManager.globalTimeScale, 0.0f, animTime));
                //Time.timeScale = 0f;
                //timeScaler.SetTimeScale(0);

                //the stop time
                timeScaler.AddInitiation(PauseTimePriority,
                    () => { timeScaler.SetTimeScale(0f); });

                timeScaler.ScaleTimeInLine(PauseTimePriority);              
                timeScaler.AddInitiation(PauseTimePriority, () => {
                    timeScaler.ScaleTime(0.0f, 
                        bank.MMTools.SceneManager.GlobalTimeScale, animTime);
                });
                CharacterController2D.MovementDisabledActions();
                break;
            case ExitButtonStates.pressed:
                AdManager.HideBannerAd();
                //timeScaler.ScaleTimeInLine(PauseTimePriority,
                //    0.0f, bank.MMTools.SceneManager.GlobalTimeScale, animTime);
                timeScaler.ScaleTimeInLine(PauseTimePriority);
                CharacterController2D.MovementEnabledActions();
                //StartCoroutine(ScaleTime(0.0f, bank.MMTools.SceneManager.GlobalTimeScale, animTime));
                break;
        }
        StartCoroutine(RotateGradually(exitButton.transform, 179.9f, animTime));
        StartCoroutine(BounceAlphas(exitButton.GetComponent<Image>(), animTime));
        StartCoroutine(WaitThenSwitchExitButton(true, animTime));
    }
    public void UpdateStats(string dcText, string crText)
    {
        deathCountText.text = dcText;
        currentResultText.text = crText;
    }
    public void YesButtonActions()
    {
        //just yet we quit. later on this will be a return to the main menu.
        //UnityEditor.EditorApplication.isPlaying = false;
        //a year later it is time to go to the main menu :) 
        bank.MMTools.SceneManager.LoadMainMenu();
    }
    public void SwitchPauseImg()
    {
        /*
         * setting up the swap and the fading:
         * when the pause is hit, we swap the sprite to the pause
         * set it active
         * then start fading it in until the agreed alpha is reached.
         * 
         * when the play is hit, we immediately swap to play
         * then fade it out gradually.
         * after that we turn it off just in case.
         * 
         */ 
        Image pauseImage = pauseImg.GetComponent<Image>();
        switch (exitState)
        {
            case ExitButtonStates.idle:
                pauseImage.sprite = pausedSprite;
                pauseImg.SetActive(true);
                StartCoroutine(ScaleSpriteAlpha(pauseImage, 0.0f, exitIdleAlpha, animTime));
                break;
            case ExitButtonStates.pressed:
                pauseImage.sprite = playSprite;
                StartCoroutine(ScaleSpriteAlpha(pauseImage, exitIdleAlpha, 0.0f, animTime));
                StartCoroutine(WaitThenSwitchGameObject(false, pauseImg, animTime + 0.2f));
                break;
        }
    }
    public void ShowRetry()
    {
        Image runtimeIMG = null;
        CanvasGroup runtimeCG = null;
        retryPanel.SetActive(true);
        foreach(GameObject UIitem in SceneGameManager.Instance.UIRetry)
        {
            runtimeIMG = null;
            runtimeCG = null;

            runtimeIMG = UIitem.GetComponent<Image>();
            if (runtimeIMG == null)
            {
                runtimeCG = UIitem.GetComponent<CanvasGroup>();
            }
            if (runtimeIMG != null)
            {
                StartCoroutine(ScaleSpriteAlpha(runtimeIMG, 0.0f, 1.0f, animTime));
            }
            else if (runtimeCG != null)
            {
                StartCoroutine(ScaleSpriteAlpha(runtimeCG, 0.0f, 1.0f, animTime));
            }
        }
        //old shit
        //StartCoroutine(ScaleSpriteAlpha(retryBut, 0.0f, 1.0f, animTime));
        //StartCoroutine(ScaleSpriteAlpha(notRetryBut, 0.0f, 1.0f, animTime));
        //StartCoroutine(ScaleSpriteAlpha(exitImg, exitImg.color.a, 0.0f, animTime));
        //StartCoroutine(WaitThenSwitchExitButton(false, animTime + 0.1f));
    }
    public void RetryButtonActions()
    {
        Scene scene = SceneManager.GetActiveScene();
        bank.MMTools.SceneManager.SetTimeScale(
            bank.MMTools.SceneManager.GlobalTimeScale);
        
        SceneManager.LoadScene(scene.name,LoadSceneMode.Single);      
    }
    public void NoButtonActions()
    {
        //we call the exit button anim, because we want exactly the same to happen when we
        //press the exit anim again.
        //StartCoroutine(ScaleTime(0.0f, 1.0f, animTime));
        ExitButtonAnim();
    }
    public void YesNoPanelFading()
    {
        /*
         * this shit should know if we want to fade it in or out.
         * the buttons themselves are the ones to get faded actually.
         * maybe a for loop can go through them.
         * either that or just note which buttons are active.
         */

        switch (exitState)
        {
            case ExitButtonStates.idle:
                yesnoPanel.SetActive(true);
                //stopping music
                musicPlayer.Pause();
                StartCoroutine(ScaleSpriteAlpha(yesButton.GetComponent<Image>(), 0.0f, exitIdleAlpha, animTime));
                StartCoroutine(ScaleSpriteAlpha(noButton.GetComponent<Image>(), 0.0f, exitIdleAlpha, animTime));
                //scale the black panel as well.
                blackpanel.gameObject.SetActive(true);
                StartCoroutine(ScaleSpriteAlpha(blackpanel, 0.0f, blackPanelAlpha_paused, animTime));
                break;
            case ExitButtonStates.pressed:
                musicPlayer.Continue();
                StartCoroutine(ScaleSpriteAlpha(yesButton.GetComponent<Image>(), exitIdleAlpha, 0.0f, animTime));
                StartCoroutine(ScaleSpriteAlpha(noButton.GetComponent<Image>(), exitIdleAlpha, 0.0f, animTime));
                StartCoroutine(WaitThenSwitchGameObject(false, yesnoPanel, animTime));
                StartCoroutine(ScaleSpriteAlpha(blackpanel, blackpanel.color.a, 0.0f, animTime));
                StartCoroutine(WaitThenSwitchGameObject(false, blackpanel.gameObject, animTime + 0.1f));
                break;
        }
    }
    public IEnumerator WaitThenSwitchGameObject(bool toWhat, GameObject go, float delay)
    {
        //waiting for a given time WITHOUT timescale.
        yield return WaitForRealSeconds(delay);
        go.SetActive(toWhat);
    }
    public IEnumerator WaitThenSwitchExitButton(bool toWhat, float waitTime)
    {
        yield return WaitForRealSeconds(waitTime);
        canClickExit = toWhat;
    }

    public IEnumerator ScaleSpriteAlpha(Image img,float start, float end, float time)
    {
        float lastTime = Time.unscaledTime;
        float timer = 0.0f;

        while (timer < time)
        {
            img.color = new Color(img.color.r, img.color.g, img.color.b, Mathf.Lerp(start, end, timer / time));
            timer += (Time.unscaledTime - lastTime);
            lastTime = Time.unscaledTime;

            yield return null;
        }
    }
    public IEnumerator ScaleSpriteAlpha(CanvasGroup cg, float start, float end, float time)
    {
        float lastTime = Time.unscaledTime;
        float timer = 0.0f;

        while (timer < time)
        {
            cg.alpha = Mathf.Lerp(start, end, timer / time);
            timer += (Time.unscaledTime - lastTime);
            lastTime = Time.unscaledTime;

            yield return null;
        }
    }
    public IEnumerator ScaleTime(float start, float end, float time)
    {
        float lastTime = Time.unscaledTime;
        float timer = 0.0f;

        while (timer < time)
        {
            //scaling down time 
            Time.timeScale = Mathf.Lerp(start, end, timer / time);
            timer += (Time.unscaledTime - lastTime);
            lastTime = Time.unscaledTime;

            yield return null;
        }
        Time.timeScale = end;
    }
    public IEnumerator BounceAlphas(Image img,float fullTime)
    {
        /*
         * gradually scale alpha down
         * when at the bottom, we might fire off an event
         * then we scale the alpha gradually back up
         */
        StartCoroutine(ScaleSpriteAlpha(img, exitIdleAlpha, 0.0f, fullTime/2.0f));
        switch (exitState)
        {
            case ExitButtonStates.idle:
                StartCoroutine(SwapSprites(img, exitButtonExitSprite, animTime/2.0f));
                exitState = ExitButtonStates.pressed;
                break;
            case ExitButtonStates.pressed:
                StartCoroutine(SwapSprites(img, exitButtonIdleSprite, animTime/2.0f));
                exitState = ExitButtonStates.idle;
                break;
        }
        //this used to be yield return new WaitForSeconds
        yield return WaitForRealSeconds(0.5f);
        StartCoroutine(ScaleSpriteAlpha(img, 0.0f, exitIdleAlpha, fullTime/2.0f));
    }
    IEnumerator SwapSprites(Image img, Sprite toWhat, float delay)
    {
        yield return WaitForRealSeconds(delay);
        img.sprite = toWhat;
    }

    public IEnumerator RotateGradually(Transform tr, float howManyDegreesFromCurrent, float time)
    {
        Quaternion baseRot = exitIdleRot;
        Quaternion newRot;
        newRot = Quaternion.Euler(exitIdleRot.x,
        exitIdleRot.y, exitIdleRot.z + howManyDegreesFromCurrent);
        switch (exitState)
        {
            case ExitButtonStates.pressed:
                baseRot = Quaternion.Euler(exitIdleRot.x,
                    exitIdleRot.y, exitIdleRot.z + howManyDegreesFromCurrent);
                newRot = exitIdleRot;
                break;
            case ExitButtonStates.idle:
                baseRot = exitIdleRot;
                break;
        }
        //this should make a rotation gradually.

        float lastTime = Time.unscaledTime;
        float timer = 0.0f;

        while (timer < time)
        {
            tr.rotation = Quaternion.Slerp(baseRot, newRot, timer / time);
            timer += (Time.unscaledTime - lastTime);
            lastTime = Time.unscaledTime;
            yield return null;
        }
    }
    /*
     * A coroutine that ignores timescale.
     */ 
    public IEnumerator _WaitForRealSeconds(float aTime)
    {
        while (aTime > 0.0f)
        {
            aTime -= Mathf.Clamp(Time.unscaledDeltaTime, 0, 0.2f);
            yield return null;
        }
    }
    public Coroutine WaitForRealSeconds(float aTime)
    {
        return StartCoroutine(_WaitForRealSeconds(aTime));
    }
    /*
     *until here. 
     */
}
