﻿using UnityEngine;
using System.Collections;

public class KillObject : MonoBehaviour
{
    public enum KillObjectTypes
    {
        killer, tipper
    }
    public KillObjectTypes WhichTypeIsThis;
    public float forceAdded = 300.0f;
	
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            CharacterController2D.currentImageIndex = 0;
            CharacterController2D.animState = CharacterController2D.CharAnimStates.fall;           
            switch (WhichTypeIsThis)
            {
                case KillObjectTypes.killer:
                    //Debug.Log("KILLED");
                    /*
                     * Some features:
                     * time gradually slows down along 2 seconds until it stops when the player dies.
                     * screen fades out after 1 sec, gradually along for 2 secs as well.
                     * the player input is immediately disabled.
                     * 
                    */
                    other.gameObject.GetComponent<CharacterController2D>().playerSmoke.SetActive(true);
                    //used to be like this
                    //StartCoroutine(WaitThenFailLevel());
                    //until this before using the singleton pattern.
                    StartCoroutine(SceneGameManager.Instance.WaitThenFailLevel());

                    break;
                case KillObjectTypes.tipper:
                    //Debug.Log("TIPPED");
                    other.gameObject.GetComponent<CharacterController2D>().playerSmoke.SetActive(true);
                    StartCoroutine(SceneGameManager.Instance.WaitThenFailLevel());
                    #region old
                    //old:
                    ///*
                    // * First we disable all player inputs.
                    // * if the player is in front of the tipper, we put the player upon the tipper
                    // * and give it the force. If it isn't, then we just give it the force.
                    //*/
                    //CharacterController2D.MovementDisabledActions();
                    //CharacterController2D.tippings++;
                    //if(CharacterController2D.tippings >= 3)
                    //{
                    //    //the third tip is death
                    //    other.gameObject.GetComponent<CharacterController2D>().playerSmoke.SetActive(true);
                    //    //this is the other place we used the waitthenfaillevel.
                    //    //used to be like this:
                    //    //StartCoroutine(WaitThenFailLevel());
                    //    //until this.

                    //    //but now we use the singleton:
                    //    StartCoroutine(SceneGameManager.Instance.WaitThenFailLevel());
                    //}
                    ////CharacterController2D.movementDisabled = true;

                    //if(other.gameObject.transform.position.x < this.transform.position.x)
                    //{
                    //    //we use 0.6f on the Y because the player is 1.0f tall, its half is 0.5f. Same for the collider. We give 0.1f
                    //    //only as safety.
                    //    other.gameObject.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + 1.1f);
                    //}
                    //other.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.right * forceAdded);
                    //StartCoroutine(SceneGameManager.Instance.WaitThenEnableMovement());
                    #endregion
                    break;
            }
        }
    }

    //public IEnumerator ScaleTime(float start, float end, float time)
    //{
    //    float lastTime = Time.realtimeSinceStartup;
    //    float timer = 0.0f;

    //    while(timer < time)
    //    {
    //        //scaling down time 
    //        Time.timeScale = Mathf.Lerp(start, end, timer / time);
    //        timer += (Time.realtimeSinceStartup - lastTime);
    //        lastTime = Time.realtimeSinceStartup;
            
    //        yield return null;
    //    }
    //    Time.timeScale = end;
    //}
    public IEnumerator ScaleSpriteAlpha(float start, float end, float time)
    {
        float lastTime = Time.realtimeSinceStartup;
        float timer = 0.0f;

        while (timer < time)
        {
            //fading out the character using its sprite renderer alpha
            CharacterController2D.charSpriteRenderer.color = new Color(0.0f, 0.0f, 0.0f, Mathf.Lerp(start, end, timer / time));
            timer += (Time.realtimeSinceStartup - lastTime);
            lastTime = Time.realtimeSinceStartup;
            yield return null;
        }
    }
}
