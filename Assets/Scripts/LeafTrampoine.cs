﻿using UnityEngine;
using System.Collections;
using Assets.MainMenuStuff;
using Assets.JumpAssist.Trampolines;
using System;

public class LeafTrampoine : MonoBehaviour, ITrampolineFunctionality
{
    private Rigidbody2D hingeRB;
    private HingeJoint2D ownHingeJoint;
    public float jumpedMultiplier = 1.3f;
    public float idleMultiplier = 1f;

    public Func<Rigidbody2D, Vector2> GetSimpleJump
    {
        get;set;
    }

    public Func<Rigidbody2D, Vector2> GetDoubleJump { get; set; }

    //private HingeJoint2D leafHinge;
    //public enum LeafOrientation
    //{
    //    standard, mirrored
    //}
    //public LeafOrientation currentLeafOrientation;
    // Use this for initialization
    void Awake ()
    {
        hingeRB = GetComponentInChildren<Rigidbody2D>();
        ownHingeJoint = GetComponentInChildren<HingeJoint2D>();
        //leafHinge = GetComponentInChildren<HingeJoint2D>();
        hingeRB.gravityScale = 0.0f;
        hingeRB.Sleep();
        ownHingeJoint.enabled = false;

        GetSimpleJump = (otherRB) => {
            return new Vector2(otherRB.velocity.x,
                CharacterController2D.charMovement.jumpSpeed * idleMultiplier);
        };

        GetDoubleJump = (otherRB) =>
        {
            return new Vector2(otherRB.velocity.x,
                CharacterController2D.charMovement.jumpSpeed * jumpedMultiplier);
        };
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && !CharacterController2D.charMovement.movementDisabled
            && !CharacterController2D.playerDied)
        {
            if (hingeRB.IsSleeping())
            {
                hingeRB.WakeUp();
                ownHingeJoint.enabled = true;
            }
            //meaning the player is inside the field that can set our velocity
            hingeRB.gravityScale = 3.0f;
            Rigidbody2D otherRB = other.gameObject.GetComponent<Rigidbody2D>();
            if (/*Input.GetButton("Jump")*/ JumpInput.JUMP)
            {
                //otherRB.velocity = new Vector2(otherRB.velocity.x, CharacterController2D.charMovement.jumpSpeed * jumpedMultiplier);
                otherRB.velocity = GetDoubleJump.Invoke(otherRB);
            }
            else
            {
                //otherRB.velocity = new Vector2(otherRB.velocity.x, CharacterController2D.charMovement.jumpSpeed * idleMultiplier);
                otherRB.velocity = GetSimpleJump.Invoke(otherRB);
            }
            StartCoroutine(WaitThenBounceBack());
            /*
             * Sidenote: we can disable this shit right at the end because
             * the player will only use them once. 
             * however, for the safety, if the shit is not active, the
             * code enables it again. 
             * so we can just disable it at the end of its use.
            */
            StartCoroutine(WaitThenDisableThisShit());
            //DisableThisShit();
        }
    }
    IEnumerator WaitThenBounceBack()
    {
        yield return new WaitForSeconds(0.1f);
        BounceBack();
    }
    void BounceBack()
    {
        hingeRB.gravityScale = -3.0f;
    }
    IEnumerator WaitThenDisableThisShit()
    {
        yield return new WaitForSeconds(0.3f);
        DisableThisShit(); 
    }
    void DisableThisShit()
    {
        hingeRB.gravityScale = 0.0f;
        ownHingeJoint.enabled = false;
        hingeRB.Sleep();
    }
}
