﻿using UnityEngine;
using System.Collections;

public class MovingSky : MonoBehaviour
{
    public float skyScrollSpeed = 10.0f;
    public float moveLimit = 68.58f;
    public float damping = 1.0f;
    Vector3 currentVelocity;
    Vector3 target;
    Vector3 newPos;
    private GameObject spriteInSquel;
    private GameObject playerCharacter;
    public GameObject lastSpriteIndicator;
    public Sprite BGSprite;

    private Transform[] movingSpritePositions;
    private GameObject[] movingGO;
	void Start ()
    {
        playerCharacter = GameObject.FindGameObjectWithTag("Player");
        movingGO = GameObject.FindGameObjectsWithTag("MovingBGSprite");
        movingSpritePositions = new Transform[movingGO.Length];
        for(int i = 0; i < movingSpritePositions.Length; i++)
        {
            movingSpritePositions[i] = movingGO[i].transform;
        }
        movingGO = null;
        for(int i = 0; i < movingSpritePositions.Length - 1; i++)
        {
            for(int j = i + 1; j < movingSpritePositions.Length; j++)
            {
                if(movingSpritePositions[i].position.x < movingSpritePositions[j].position.x)
                {
                    Transform temp = movingSpritePositions[i];
                    movingSpritePositions[i] = movingSpritePositions[j];
                    movingSpritePositions[j] = temp;
                }
            }
        }
        CameraMovement.currentSkyState = CameraMovement.SkyStates.moving;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (DistanceWithToleranceTooClose(playerCharacter.transform, lastSpriteIndicator.transform,10.0f))
        {
            //this shit happens when the need arises for a new image plane.
            NewBGSprite newLast = new NewBGSprite(BGSprite, transform.lossyScale);
            //Instantiate(newLast.newLastSprite);
            newLast.newLastSprite.transform.position = new Vector3(lastSpriteIndicator.transform.position.x + 16.33f * 2.0f * transform.lossyScale.x,
                transform.position.y, transform.position.z);
            lastSpriteIndicator.transform.position = new Vector3(lastSpriteIndicator.transform.position.x + 16.33f * transform.lossyScale.x,
                transform.position.y, transform.position.z);
            newLast.newLastSprite.transform.parent = transform;

            InsertWithShift(movingSpritePositions, newLast.newLastSprite.transform);

            //so we have the new one. now to delete the old one:
            /*
             * the old one here is the one farthest to the player.
             * lucky we stored their transforms in an array.
             * we just have to 
            */
            
        }

	}
    public static bool DistanceWithToleranceTooClose(Transform first, Transform second, float XTolerance)
    {
        if(Mathf.Abs(first.position.x - second.position.x) < XTolerance)
        {
            return true;
        }
        return false;
    }
    public static void InsertWithShift(Transform[] array, Transform newItem)
    {
        //first we move the elements WITH overwriting the latest
        //the array must be in order for this to work.
        //order by: descending.
        Destroy(array[array.Length - 1].gameObject);
        for(int i = array.Length-1; i > 0; i--)
        {
            array[i] = array[i - 1];
        }
        array[0] = newItem;
    }
}
public class NewBGSprite
{
    public GameObject newLastSprite;
    public SpriteRenderer newLastSpriteRend;
    public NewBGSprite(Sprite BGSprite, Vector3 parentGlobalScale)
    {
        newLastSprite = new GameObject();
        newLastSprite.name = "HauntedHouseBG";
        newLastSprite.AddComponent<SpriteRenderer>();
        newLastSpriteRend = newLastSprite.GetComponent<SpriteRenderer>();
        newLastSpriteRend.sortingLayerName = "BGFar";
        newLastSpriteRend.sprite = BGSprite;
        newLastSprite.transform.localScale = parentGlobalScale;
        newLastSprite.tag = "MovingBGSprite";
    }
}
