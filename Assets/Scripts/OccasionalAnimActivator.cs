﻿using UnityEngine;
using System.Collections;

public class OccasionalAnimActivator : MonoBehaviour
{
    private Collider2D collisionOnGO;
    private ImageSequencePlayer imagePlayer;
    public bool backWardsNeeded = false;
	// Use this for initialization
	void Start ()
    {
        //if we do have the collider, use it as we should.
        //if we don't, deactivate the script.
        collisionOnGO = GetComponent<Collider2D>();
        if(collisionOnGO == null)
        {
            collisionOnGO = GetComponentInChildren<Collider2D>();
        }
        if(collisionOnGO == null)
        {
            this.enabled = false;
        }

        //same for the image sequence player.
        imagePlayer = GetComponent<ImageSequencePlayer>();
        if(imagePlayer == null)
        {
            imagePlayer = GetComponentInChildren<ImageSequencePlayer>();
        }
        if(imagePlayer == null)
        {
            this.enabled = false;
        }
	}
	
	void OnCollisionEnter2D(Collision2D other)
    {
        if (!collisionOnGO.isTrigger && other.gameObject.CompareTag("Player"))
        {
            CallTheAnim();
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (collisionOnGO.isTrigger && other.gameObject.CompareTag("Player"))
        {
            CallTheAnim();
        }
    }
    void CallTheAnim()
    {
        imagePlayer.OccasionalAnimation(backWardsNeeded);
    }
}
