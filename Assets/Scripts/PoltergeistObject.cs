﻿using UnityEngine;
using System.Collections;

public class PoltergeistObject : MonoBehaviour
{
    private bool shakingContinues = true;
    public float frameRate = 24.0f;
    private Vector3 basePosition;
    private Quaternion baseRotation;
    public float ghostForceApplied = 200.0f;
    public GameObject spriteThatMoves;
    private Transform flames;
    private BoxCollider2D box;
	// Use this for initialization
	void Start ()
    {
        basePosition = spriteThatMoves.transform.position;
        baseRotation = spriteThatMoves.transform.rotation;
        box = GetComponent<BoxCollider2D>();
        flames = GetChildByNameRecursive(transform, "CandleFlames");

	}
	private Transform GetChildByNameRecursive(Transform parent, string name)
    {
        foreach (Transform child in parent)
        {
            if(child.childCount > 0)
            {
                return GetChildByNameRecursive(child, name);
            }
            if (child.gameObject.name == name)
                return child;
        }
        return null;
    }
	void OnTriggerEnter2D(Collider2D other)
    {
        /*
         * the idea: giving random transforms for the gameobject
         * as well as random rotations. We won't animate, just pop the locations and rotations.
        */
        StartCoroutine(Shaking());
        
    }
    void OnTriggerExit2D(Collider2D other)
    {
        /*
         * here we initiate the throwing.
         * the poltergeist object will need a rigidbody to be given a force.
         * the direction will be the player's actual direction.
         * so it will be easy to jump over - given that the player jumps just right. 
        */
        shakingContinues = false;
        Destroy(box);
        if (flames != null)
            flames.gameObject.SetActive(false);
        if(spriteThatMoves.GetComponent<Rigidbody2D>() == null )
                spriteThatMoves.AddComponent<Rigidbody2D>();

        Rigidbody2D rb = spriteThatMoves.GetComponent<Rigidbody2D>();
        rb.AddForce((other.gameObject.transform.position - transform.position) * ghostForceApplied);

        StartCoroutine(WaitThenDisableDatShit());
    }
    IEnumerator Shaking()
    {
        while (shakingContinues)
        {
            Vector3 newPos = basePosition + new Vector3(Random.Range(-0.2f, 0.2f),Random.Range(-0.2f, 0.2f));
            Quaternion newRot = Quaternion.Euler(baseRotation.x, 
                baseRotation. y + Random.Range(-10.0f, 10.0f),baseRotation.z + Random.Range(-10.0f, 10.0f));

            spriteThatMoves.transform.position = newPos;
            spriteThatMoves.transform.rotation = newRot;

            yield return new WaitForSeconds(1.0f / frameRate);
        }
    }
    IEnumerator WaitThenDisableDatShit()
    {
        yield return new WaitForSeconds(3.0f);
        Destroy(this.gameObject);
    }
}
