﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class ProgressTracker : MonoBehaviour
{
    /*
     * when we know about this shit, we should notify the user interface about it.
     */
    [HideInInspector]
    public Image progressFill;
    [Tooltip("The game objects that determine the points of the track")]
    public GameObject[] trackingPoints;
    private GameObject player;
    private Transform winObjectTransform;
    private float overallDistance;
    [HideInInspector]
    private int currentNextWayPoint = 1;
    private float normalizedDistance = 0;
    public float NormalizedDistance
    {
        get
        {
            float returnValue = normalizedDistance / overallDistance;
            if(returnValue > 1)
            {
                return 1;
            }else
            {
                return returnValue;
            }
        }
        set
        {
            normalizedDistance = value;
        }
    }
    public int CurrentNextWayPoint
    {
        get
        {
            return currentNextWayPoint;
        }
        set
        {
            if(value >= trackingPoints.Length)
            {
                currentNextWayPoint = value - 1;
            }else
            {
                currentNextWayPoint = value;
            }

        }
    }
    [HideInInspector]
    public float localDistance = 0.0f;
    // Use this for initialization
    //private void OnLevelWasLoaded(int level)
    //{
    //    player = GameObject.FindGameObjectWithTag("Player");
    //    winObjectTransform = GameObject.Find("WinObject").transform;
    //    if(player != null && winObjectTransform != null)
    //    {
    //        trackingPoints[0].transform.position = player.transform.position;
    //        trackingPoints[1].transform.position = winObjectTransform.position;
    //        overallDistance = OverallDistance();
    //        currentNextWayPoint = 1;
    //    }       
    //}
    private float currentProgress;
    public float CurrentProgress { get { return currentProgress; } }
    void Start()
    {
        //some failsafe to start with:
        player = GameObject.FindGameObjectWithTag("Player");
        winObjectTransform = GameObject.Find("WinObject").transform;
        progressFill = GameObject.Find("ProgressFill").GetComponent<Image>();
        if (trackingPoints != null)
        {
            if (trackingPoints[0] != null)
            {
                //so we set its point to the player. DEPRICATED. now we let it be where it was placed.
                //trackingPoints[0].transform.position = player.transform.position;
                trackingPoints[0].GetComponent<BoxCollider2D>().enabled = false;
            }
            try
            {
                trackingPoints[trackingPoints.Length - 1].transform.position =
                    winObjectTransform.position;
            }
            catch (IndexOutOfRangeException)
            {
                //we are here if the array wasn't large enough.
                Array.Resize<GameObject>(ref trackingPoints, trackingPoints.Length + 1);
                //after this we hopefully resized it to be large enough.

            }
            catch (NullReferenceException)
            {
                //so we didn't assign a gameobject to the ending part of the tracker.
                //then maybe it's high time we did it.
                trackingPoints[trackingPoints.Length - 1] = new GameObject();
                trackingPoints[trackingPoints.Length - 1].transform.position = winObjectTransform.position;
                trackingPoints[trackingPoints.Length - 1].name = "point" + trackingPoints.Length.ToString();

            }
        }
        /*
         * after this shit we should have at least the starting and the ending point of the tracker.
         */
        //getting the overall distance
        overallDistance = OverallDistance();
        currentNextWayPoint = 1;
    }

    void LateUpdate()
    {
        MeasureProgressToNextWayPoint();
    }

    public float OverallDistance()
    {
        float distance = 0.0f;
        /*
         * we calculate the overall X distance between 
         * the trackers.
         * we use only the absolute values of course.
         * and as mentioned, only along the X axis. 
         * because the game moves only along the X exis.
         */ 
        for(int i = 0; i < trackingPoints.Length-1; ++i)
        {
            distance += Mathf.Abs(trackingPoints[i].transform.position.x -
                trackingPoints[i + 1].transform.position.x);
        }
        return distance;
    }
    void MeasureProgressToNextWayPoint()
    {
        localDistance = Mathf.Abs(player.transform.position.x -
            trackingPoints[CurrentNextWayPoint - 1].transform.position.x); 
        currentProgress = NormalizedDistance + (localDistance/overallDistance);
        progressFill.fillAmount = currentProgress;
    }
    public void ChangeWayPointsActions()
    {
        CurrentNextWayPoint++;
        normalizedDistance += localDistance;
        localDistance = 0;
    }
}
