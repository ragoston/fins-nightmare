﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Assets.Scripts.InGameInit;
using Assets.Scripts.Statistics;
using Assets.Scripts.AdditionalGameUI;
using UnityEngine.PostProcessing;
using Assets.MainMenuStuff.Audio;
using System.Collections.Generic;
using Assets.AdManager;
using UnityEngine.UI;
using Assets.Scripts;
using Assets.ExceptionHandlers;
using Assets.MainMenuStuff;
using Assets.HighlightDanger.Code;
using Assets.Managers;
using System;
using System.Linq;
using Assets.Checkpoints;

public class SceneGameManager : MonoBehaviour, IGameInitializer,
    ILevelFailer, ILevelWinner, IGameManager
{
    /*
     * implementing the singleton pattern
     * 
     * note that this solution is not persistent through levels but tracks and removes
     * itself should we create two of it.
     */ 
    private static SceneGameManager _instance;
    public static SceneGameManager Instance { get { return _instance; } }

    public Dictionary<int, Action> TimeScaleInitiations
    {
        get; private set;
    }

    public IEnumerator Routine
    {
        get; private set;
    }
    public ITimeScaler TimeScaler { get; private set; }
    //a GO that contains all the runtime UI elements
    //so we can iterate through them when hiding the UI.
    public GameObject[] UIRuntime;
    public GameObject[] UIRetry;
    public GameObject mainUI;
    private InGameUIController UIScript;
    private WinCanvas winCanvas;
    public DataBank bank;
    private IMusicProvider musicProvider;
    private ISFXProvider cameraSfxprovider;
    [SerializeField]
    private GameObject DangerIndicator;
    AudioSource camSource;
	// Use this for initialization
	void Awake ()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        ExceptionUtility.SetupExceptionHandling();
        bank = GameObject.FindGameObjectWithTag("DataBank").GetComponent<DataBank>();
        TimeScaleInitiations = new Dictionary<int, Action>();
        Resources.UnloadUnusedAssets();
        TimeScaler = new PriorityTimeManager(this);
        //getting the highlight - if needed.
        if (bank.Stats.GamePlaySettings.highlights)
        {
            if(DangerIndicator != null)
            {
                foreach (var item in GameObject.FindGameObjectsWithTag("DANGER"))
                {
                     DangerIndicator.InstantiateHighlightDanger(item.transform, 
                         GetComponent<DangerHighlightEffectColor>());                    
                }
            }

        }
        //turning on or off jump assist.
        if (!bank.Stats.GamePlaySettings.jumpAssist)
        {
            //GameObject.FindGameObjectWithTag("JumpAids").SetActive(false);
            Destroy(GameObject.FindGameObjectWithTag("JumpAids"));
        }
        #region old init
        //foreach (var c in GameObject.FindGameObjectsWithTag("InGameInit"))
        //{
        //    //c.GetComponent<InitializeMeIngame>().INITIALIZE(bank);
        //    foreach (var g in c.GetComponents<InitializeMeIngame>())
        //    {
        //        g.INITIALIZE(bank);
        //    }
        //    if (c.GetComponent<WinCanvas>() != null)
        //    {
        //        winCanvas = c.GetComponent<WinCanvas>();
        //        winCanvas.winObject.LEVEL_COMPLETE += WinLevel;
        //        winCanvas.gameObject.SetActive(false);
        //    }
        //    if(c.GetComponent(typeof(IMusicProvider)) != null)
        //    {
        //        musicProvider = c.GetComponent(typeof(IMusicProvider)) as
        //            IMusicProvider;
        //    }
        //    if(c.GetComponent(typeof(ISFXProvider)) != null)
        //    {
        //        cameraSfxprovider = c.GetComponent(typeof(ISFXProvider)) as
        //            ISFXProvider;
        //    }

        //}
        #endregion
        LaunchInitialization("InGameInit");
        //setting up the character trail renderer
        var trailRend = GameObject.FindGameObjectWithTag("Player")
                          .transform.Find("CharSpriteSheet").GetComponent<TrailRenderer>();
        if(trailRend != null)
        {
            trailRend.enabled = bank.Stats.Settings.CharacterOutline_value;
        }
        //setting up post processing:
        var postProc = GameObject.FindGameObjectWithTag("MainCamera")
            .GetComponent<PostProcessingBehaviour>();
        if(postProc != null)
        {
            postProc.enabled = bank.Stats.Settings.PostProc_value;
            try
            {
                Camera.main.GetComponent<FlareLayer>().enabled = bank.Stats.Settings.PostProc_value;
            }
            catch { }
            
        }


#if DEBUG
        Debug.Log("Current timescale: "+Time.timeScale.ToString());
#endif
        

        
    }
    void Start()
    {
        //SceneManager.UnloadSceneAsync(0);
        UIScript = mainUI.GetComponent<InGameUIController>();

        if (!mainUI.activeSelf) mainUI.SetActive(true);
        bank.MMTools.SceneManager.SetTimeScale(
            bank.MMTools.SceneManager.GlobalTimeScale);
        
#if DEBUG
        Debug.Log("Current timescale: " + Time.timeScale.ToString());
#endif

        
        try
        {
           camSource = Camera.main.transform.Find("MusicObject").GetComponent<AudioSource>();
        }
        catch { }

        //now to request some banners:
        AdManager.DestroyAllAds();
        AdManager.GetSomeAdRequests();
        //for now.

        AdManager.RequestInterstitial();
        AdManager.RequestBanner();
        AdManager.OnInterstitialAdditional = () =>
        {
            try
            {
                //force load ads
                AdManager.DestroyAllAds();
                AdManager.GetSomeAdRequests();
                AdManager.RequestBanner();
                AdManager.RequestInterstitial();
                //basically forcing the retry menu to load.
                ForceRetry();
                //so it means we WEREN'T JUMPING.
                JumpInput.JUMP = false;
            }catch
            {
                Debug.Log("Couldn't load the RETRY ui after interstitial ad.");
            }

        };//lambda expression ends here.

        //initializing level checkpoints
        var levelChkpnts = GameObject.FindGameObjectsWithTag(
            bank.CheckpointProvider.GetTag());

        if (levelChkpnts != null)
        {
            if (bank.CheckpointProvider == null
                 || bank.CheckpointProvider.GetCheckpointData() == null)
            {
                //then disable the checkpoints.
                foreach (var item in levelChkpnts)
                {
                    item.gameObject.SetActive(false);
                }
            }
            LevelCheckpoint[] levelCheckpoints = new LevelCheckpoint[
                levelChkpnts.Length];

            var i = 0;
            foreach (var item in levelChkpnts.OrderBy(x => x.transform.position.x)
                .ToList())
            {
                var chkpnt = item.GetComponent<LevelCheckpoint>();
                if (chkpnt != null)
                {
                    chkpnt.UPDATE_CHECKPOINT += bank.CheckpointProvider.CheckpointWrapper;
                    //needed cuz we'll have to drop the player to the selected checkpoint on load.
                    levelCheckpoints[i++] = chkpnt;
                }
                else { print("No checkpoint found on a levelCheckpoint tagged object."); }
            }
            //disable the first N pieces (where N is the count of unlocked ones).

            //because latestCheckpoint is an index and starts from 0. have to make up for it.
            #region disabling the used checkpoints.
            var pieces = bank.CheckpointProvider.GetCheckpointData()
                [bank.Stats.LastPlayedLevel - 1].latestCheckpoint + 1;

            Debug.Log(pieces);
            foreach (var item in levelCheckpoints.Take(pieces).ToList())
            {
                //item.isEnabled = false;
                //try
                //{
                //    Destroy(item);
                //}
                //catch { }
                item.isUnlocked = true;
            }
            #endregion
            PlacePlayerOnCheckpoint(levelCheckpoints.Skip(bank.CheckpointProvider.GetCheckpointData()
                [bank.Stats.LastPlayedLevel - 1].selectedCheckpoint).FirstOrDefault()
                .transform.position);

            //TODO: finish initializing checkpoints. the first on the level mustn't have a collider, 
            //or if it has, the player mustn't enter it. 

        }
        else
        {
            print("Couldn't find any GOs with the tag " + bank.CheckpointProvider.GetTag());
        }

    }

    private void PlacePlayerOnCheckpoint(Vector3 pos)
    {
        var player = GameObject.FindGameObjectWithTag("Player");
        if(player != null)
        {
            player.transform.position = pos;



            Camera.main.gameObject.transform.position = new Vector3(
                pos.x, pos.y, Camera.main.gameObject.transform.position.z);
            
        }
        else { print("Couldn't find player character in Scene manager."); }
    }

    public void WinLevel()
    {
        UIScript.gameObject.SetActive(false);
        winCanvas.gameObject.SetActive(true);
        //AdManager.ShowBannerAd();
        musicProvider.ShutDownMusicImmediately();
        cameraSfxprovider.PlaySFX(0);
        UIScript.StopAllCoroutines();
        try
        {
            winCanvas.WinObject_LEVEL_COMPLETE();
            try
            {
                camSource.loop = false;
            }
            catch { }
            
        }catch
        {
            winCanvas.WinObject_LevelComplete_NoCoroutine();
            winCanvas.SetDeathCountText();
        }

        
    }
    public IEnumerator WaitThenFailLevel()
    {

        if (!CharacterController2D.playerDied)
        {
            try
            {
                CharacterController2D.playerDied = true;
                if (CharacterController2D.animState != CharacterController2D.CharAnimStates.fall)
                {
                    CharacterController2D.currentImageIndex = 0;
                    CharacterController2D.animState = CharacterController2D.CharAnimStates.fall;
                }
                CharacterController2D.MovementDisabledActions();
            }
            catch
            {
                ForceRetry();
            }
            


            //update death count and write best percent:

            try
            {
                //stop music looping.
                try
                {
                    camSource.loop = false;
                }
                catch { }
                

                bank.MMTools.StatisticsUtility.GetLevelDatas[bank.Stats.LastPlayedLevel - 1].DeathCount++;
                string toDC = "Death count: " +
                    bank.MMTools.StatisticsUtility.GetLevelDatas[bank.Stats.LastPlayedLevel - 1].DeathCount;
                string toCR;
                //updating the best percent:
                var tracker = GetComponent<ProgressTracker>();
                int currentProgressInt = Mathf.RoundToInt(tracker.CurrentProgress * 100);
                if (bank.MMTools.StatisticsUtility.GetLevelDatas[bank.Stats.LastPlayedLevel - 1].BestPercent
                    < currentProgressInt)
                {
                    bank.MMTools.StatisticsUtility
                        .GetLevelDatas[bank.Stats.LastPlayedLevel - 1].BestPercent = currentProgressInt;
                    toCR = "New record with " + currentProgressInt.ToString() + "%!";
                }
                else
                {
                    toCR = "Current result: " + currentProgressInt.ToString() + "%";
                }
                //turn off music.
                musicProvider.ShutDownMusicImmediately();
                //play lost
                cameraSfxprovider.PlaySFX(1);
                bank.Save(JsonUtility.ToJson(bank.Stats), DataBank.STATS_NAME);
                UIScript.UpdateStats(toDC, toCR);

            }
            catch
            {
                Debug.Log("EXCEPTION WHEN RETRY BUTTON");
                ForceRetry();
            }

            //ADDING THE AD.
            try
            {
                if (AdManager.InGameVideoCounter == 0)
                {
                    AdManager.ShowVideo();
                }
                else
                {
                    AdManager.ShowBannerAd();
                }
            }
            catch { }

            yield return WaitForRealSeconds(0.2f);
            try
            {              

                //CharacterController2D.movementDisabled = true;
                //used to have this outside of the try.
                //

                /*
                 * now we need to stop the active coroutines from the UI script 
                 * in order to avoid any malfunctioning due to buttons just pressed.
                 */

                TimeScaler.ScaleTime(bank.MMTools.SceneManager.GlobalTimeScale, 0.0f, 3.0f);
                StartCoroutine(ScaleSpriteAlpha(1.0f, 0.0f, 0.5f));

                //we also have to manage the UI system when the level is failed
                /*
                 * to do this we need to take those into consideration:
                 * if the pause menu is active, disable it
                 * if disabled, fade in the retry options.
                 */
                UIScript.StopAllCoroutines();

                //now we shall fade da lil bitch out.
                //no matter if it is already faded out, we can still do that, from 0 to 0.
                UIScript.canClickExit = false;
                UIScript.exitState = InGameUIController.ExitButtonStates.idle;
                UIScript.blackpanel.gameObject.SetActive(true);


                StartCoroutine(UIScript.ScaleSpriteAlpha(UIScript.blackpanel, 0.0f,
        UIScript.blackPanelAlpha_ended, UIScript.animTime));

                //new one, using the UIRuntime tag:
                /*
                 * the idea here is to use either the Image property or
                 * the canvas group to scale the alpha.
                 */
                UnityEngine.UI.Image runtimeIMG = null;
                CanvasGroup runtimeCG = null;
                foreach (GameObject UIitem in UIRuntime)
                {
                    runtimeIMG = null;
                    runtimeCG = null;

                    runtimeIMG = UIitem.GetComponent<UnityEngine.UI.Image>();
                    if (runtimeIMG == null)
                    {
                        runtimeCG = UIitem.GetComponent<CanvasGroup>();
                    }
                    if (runtimeIMG != null)
                    {
                        AlphaAction a = new AlphaAction(runtimeIMG, runtimeIMG.color.a, 0f, UIScript.animTime);
                        //StartCoroutine(UIScript.ScaleSpriteAlpha(runtimeIMG, runtimeIMG.color.a, 0.0f, UIScript.animTime));
                        a.NEXT_FUNCTION += (mono) =>
                        {
                            mono.StartCoroutine(UIScript.WaitThenSwitchGameObject(false, UIitem, 0));
                        };
                        a.ScaleSpriteAlpha(this);
                        //StartCoroutine(UIScript.WaitThenSwitchGameObject(false, UIitem, UIScript.animTime));
                    }
                    else if (runtimeCG != null)
                    {
                        AlphaAction b = new AlphaAction(runtimeCG, runtimeCG.alpha, 0f, UIScript.animTime);
                        //StartCoroutine(UIScript.ScaleSpriteAlpha(runtimeIMG, runtimeIMG.color.a, 0.0f, UIScript.animTime));
                        b.NEXT_FUNCTION += (mono) =>
                        {
                            mono.StartCoroutine(UIScript.WaitThenSwitchGameObject(false, UIitem, 0));
                        };
                        b.LerpAlpha(this);
                        //StartCoroutine(UIScript.ScaleSpriteAlpha(runtimeCG, runtimeCG.alpha, 0.0f, UIScript.animTime));
                        //StartCoroutine(UIScript.WaitThenSwitchGameObject(false, UIitem, UIScript.animTime));
                    }
                }

                UIScript.ShowRetry();
            }
            catch
            {
                ForceRetry();

            }
            
        }

    }
    void ForceHideUIElements()
    {
        foreach(GameObject go in UIRuntime)
        {
            go.SetActive(false);
        }
    }
    public void ForceRetry()
    {
        Time.timeScale = 0f;
        try
        {
            UIScript.StopAllCoroutines();
            UIScript.retryPanel.SetActive(true);

            foreach (var e in UIRuntime)
            {
                e.SetActive(false);
            }
            foreach (var item in UIRetry)
            {
                item.SetActive(true);
                //now taking care of the alphas
                if (item.GetComponent<Image>() != null)
                {
                    var im = item.GetComponent<Image>();
                    im.color = new Color(im.color.r, im.color.g, im.color.b, 1);
                }
                if (item.GetComponent<CanvasGroup>() != null)
                {
                    var cg = item.GetComponent<CanvasGroup>();
                    cg.alpha = 1;
                }
            }
        }
        catch { //nothing to do.
        }



    }
    //private IEnumerator _ScaleTime(float start, float end, float time)
    //{
    //    float lastTime = Time.realtimeSinceStartup;
    //    float timer = 0.0f;

    //    while (timer < time)
    //    {
    //        //scaling down time 
    //        Time.timeScale = Mathf.Lerp(start, end, timer / time);
    //        timer += (Time.realtimeSinceStartup - lastTime);
    //        lastTime = Time.realtimeSinceStartup;

    //        yield return null;
    //    }
    //    Time.timeScale = end;
    //}
    public static IEnumerator ScaleSpriteAlpha(float start, float end, float time)
    {
        float lastTime = Time.realtimeSinceStartup;
        float timer = 0.0f;

        while (timer < time)
        {
            //fading out the character using its sprite renderer alpha
            CharacterController2D.charSpriteRenderer.color = new Color(0.0f, 0.0f, 0.0f, Mathf.Lerp(start, end, timer / time));
            timer += (Time.realtimeSinceStartup - lastTime);
            lastTime = Time.realtimeSinceStartup;
            yield return null;
        }
    }
    IEnumerator _WaitForRealSeconds(float aTime)
    {
        while (aTime > 0.0f)
        {
            aTime -= Mathf.Clamp(Time.unscaledDeltaTime, 0, 0.2f);
            yield return null;
        }
    }
    Coroutine WaitForRealSeconds(float aTime)
    {
        return StartCoroutine(_WaitForRealSeconds(aTime));
    }
    public IEnumerator WaitThenEnableMovement()
    {
        yield return WaitForRealSeconds(1.0f);
        if (!CharacterController2D.playerDied)
        {
            CharacterController2D.MovementEnabledActions();
            //CharacterController2D.movementDisabled = false;
            CharacterController2D.currentImageIndex = 0;
            CharacterController2D.animState = CharacterController2D.CharAnimStates.getUp;
        }
    }

    public void LaunchInitialization(string tagOfGameObjects)
    {
        foreach (var c in GameObject.FindGameObjectsWithTag(tagOfGameObjects))
        {
            //c.GetComponent<InitializeMeIngame>().INITIALIZE(bank);
            foreach (var g in c.GetComponents<InitializeMeIngame>())
            {
                g.INITIALIZE(bank);
            }
            if (c.GetComponent<WinCanvas>() != null)
            {
                winCanvas = c.GetComponent<WinCanvas>();
                winCanvas.winObject.LEVEL_COMPLETE += WinLevel;
                winCanvas.gameObject.SetActive(false);
            }
            if (c.GetComponent(typeof(IMusicProvider)) != null)
            {
                musicProvider = c.GetComponent(typeof(IMusicProvider)) as
                    IMusicProvider;
            }
            if (c.GetComponent(typeof(ISFXProvider)) != null)
            {
                cameraSfxprovider = c.GetComponent(typeof(ISFXProvider)) as
                    ISFXProvider;
            }

        }
    }

    public void LaunchInitialization<T>()
    {
        //not needed for now.
    }

    //public bool ScaleTimeInLine(int priority)
    //{
    //    if (TimeScaleInitiations.Count <= 0)
    //    {
    //        print("No elements in TimeScaleInitiations and ScaleTimeInLine was called.");
    //        return false;
    //    }
    //    if (Routine != null)
    //        StopCoroutine(Routine);
    //    KeyValuePair<int, Action> myCor = new KeyValuePair<int, Action>();
    //    myCor = TimeScaleInitiations.OrderByDescending(x => x.Key).FirstOrDefault();
    //    if(priority == myCor.Key)
    //    {
    //        myCor.Value.Invoke();
    //        //TimeScaleInitiations.Remove(myCor.Key);
    //        TimeScaleInitiations.Clear();
    //        return true;
    //    }
    //    return false;
    //}

    //public void SetTimeScale(float time)
    //{
    //    if (Routine != null)
    //        StopCoroutine(Routine);
    //    Time.timeScale = time;
    //}

    //public void AddInitiation(int priority, Action action)
    //{
    //    if (!TimeScaleInitiations.ContainsKey(priority))
    //    {
    //        TimeScaleInitiations.Add(priority, action);
    //    }
    //}

    //public void RemoveInitiation(int priority)
    //{
    //    if (TimeScaleInitiations.ContainsKey(priority))
    //    {
    //        TimeScaleInitiations.Remove(priority);
    //    }        
    //}

    //public void ScaleTime(float start, float end, float time)
    //{
    //    if (Routine != null)
    //        StopCoroutine(Routine);
    //    Routine = _ScaleTime(start, end, time);
    //    StartCoroutine(Routine);
    //}
}