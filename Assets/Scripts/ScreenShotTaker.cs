﻿using Assets.Scripts.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class ScreenShotTaker : MonoBehaviour
    {
        public KeyCode takeScreenshotKey = KeyCode.S;
        public int screenshotCount = 0;
        public DataBank bank;
        private void Start()
        {
            bank = GameObject.FindGameObjectWithTag("DataBank")
                .GetComponent<DataBank>();
        }
        private void Update()
        {
            if (Input.GetKeyDown(takeScreenshotKey))
            {
                ScreenCapture.CaptureScreenshot("Screenshots/" + bank.Stats.levelIdentifiers[bank.Stats.LastPlayedLevel]
                     + "_" + screenshotCount + "_"+ Screen.width + "X" + Screen.height + "" + ".png");
                Debug.Log("Screenshot taken.");
            }
        }
    }
}
