﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyFollow : MonoBehaviour
{
    public GameObject mainCam;
    [HideInInspector]
    public Vector3 difference;
    void Update ()
    {
        transform.position = new Vector3(
            difference.x + mainCam.transform.position.x,
            difference.y + mainCam.transform.position.y, transform.position.z);
	}
    public void Initialize()
    {
        difference = transform.position - mainCam.transform.position;
    }
}
