﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LayerOrder : MonoBehaviour, IComparable<LayerOrder>
{
    public int order = 1;
    public enum SortingLayers
    {
        foreground,playerGround,midground,background,bgFar
    }
    public SortingLayers currentSortingLayer = SortingLayers.playerGround;

    public int CompareTo(LayerOrder other)
    {
        if (this.currentSortingLayer > other.currentSortingLayer) return 1;
        else if (this.currentSortingLayer < other.currentSortingLayer) return -1;
        else if (this.currentSortingLayer == other.currentSortingLayer)
        {
            if (this.order > other.order) return 1;
            else if (this.order < other.order) return -1;
            else return 0;
        }
        else return 0;
    }
}
