﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[ExecuteInEditMode]
public class SortingLayer : MonoBehaviour
{
	private new Renderer renderer;

	public enum SortingLayers
	{
		foreground,playerGround,midground,background,BGfar
	}
    [SerializeField]
    private SortingLayers currentLayer = SortingLayers.playerGround;
    public SortingLayerList sortingLayerList = new SortingLayerList();
    public SortingLayers CurrentLayer
    {
        get
        {
            return currentLayer;
        }

        set
        {
            currentLayer = value;
            sortingLayerList.AddNewLayer();
        }
    }
    public int order = 1;

    private void UpdateOrderInLayer()
    {
        #region Checking for the components to modify

        if ((renderer = this.GetComponent<MeshRenderer>()) == null)
        {
            if ((renderer = this.GetComponent<SpriteRenderer>()) == null)
            {
                Debug.Log("The component " + this.name + "contains no Sprite- or MeshRenderer.");
            }
        }
        #endregion

    }
    public class SortingLayerList
    {
        public const int defaultRange = 10;
        List<SortingLayerNode> layers;
        private int GetEnumLength<T>() where T : struct
        {
            return Enum.GetNames(typeof(T)).Length;
        }

        public void AddNewLayer()
        {
            int i = GetEnumLength<SortingLayers>();
            Add(i - 1, i - 1, defaultRange);
        }
        private void Add(int prevStarter,int prevRange,int desiredRange)
        {
            layers.Add(new SortingLayerNode(layers[prevStarter - 1].Starter, layers[prevRange - 1].Range, desiredRange));
        }

        public SortingLayerList()
        {
            layers = new List<SortingLayerNode>();
            for (int i = 1; i < GetEnumLength<SortingLayers>(); i++)
            {
                //for all my enums, add a node. 
                Add(i - 1, i - 1, defaultRange);
            }
        } 
        private void ResizeLayersUp(int indexOfLayer)
        {
            //from the given layer starting, we have to move the layers.
            layers[indexOfLayer].Range += 10;
            for (int i = indexOfLayer+1; i < GetEnumLength<SortingLayers>(); i++)
            {
                if(layers[i].Starter+10 >= Int16.MaxValue) { throw new IndexOutOfRangeException(); }
                layers[i].Starter += 10;
            }
        }
        private void ResizeLayersDown(int indexOfLayer)
        {
            layers[indexOfLayer].Range -= 10;
            for (int i = indexOfLayer - 1; i > 0; i--)
            {
                if (layers[i].Starter - 10 <= Int16.MinValue) { throw new IndexOutOfRangeException(); }
                layers[i].Starter -= 10;
            }
        }
    }
    public class SortingLayerNode
    {
        private int starter;
        public int Starter
        {
            get { return starter; }
            set { starter = value; }
        }

        private int range;
        public int Range
        {
            get { return range; }
            set { range = value; }
        }
        public SortingLayerNode()
        {
            this.starter = 0;
            this.range = SortingLayerList.defaultRange;
        }
        public SortingLayerNode(int prevStarter, int prevRange, int myRange)
        {
            Starter = prevStarter + prevRange;
            Range = myRange;
        }
    }
    [ExecuteInEditMode]
    void OnValidate()
    {
        if (renderer == null) renderer = this.GetComponent<MeshRenderer>();
        //runs everytime a property changes in the inspector.
        //in fact it is used to validate the given value, but can be used to anything else. 
        renderer.sortingOrder = order;
        Debug.Log("Sorting order modified.");
    }
}
