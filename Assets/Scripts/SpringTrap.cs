﻿using UnityEngine;
using System.Collections;

public class SpringTrap : MonoBehaviour
{
    private Transform objectToMove;
    // Use this for initialization
    Vector3 currentVelocity;
    private ImageSequencePlayer imagePlayer;
    public enum TypeOfTrap
    {
        springOut, animatedWithEvent
    }
    [Tooltip("Set the type of trap here to use.")]
    public TypeOfTrap trapType = TypeOfTrap.springOut;
    public float starterAlphaIfAnimated = 0.0f;
    public float springDistance;
    public float timeToFinish;
    private bool alreadyUsed = false;
    //bool startMoving = false;
    [Tooltip("The image that triggers the event when using animated traps.")]
    public int frameToFireEvent = 0;
    public GameObject trapToActivate;
    [Tooltip("If using an animated trap, this indicates the fade in time of the trap.")]
    public float fadeInTime = 0.5f;
    private SpriteRenderer trapSpriteRenderer;

	void Start ()
    {
        objectToMove = transform.Find("ObjectsToAnimate");
        switch(trapType)
        {
            case TypeOfTrap.animatedWithEvent:
                try
                {
                    imagePlayer = GetComponent<ImageSequencePlayer>();
                    if (imagePlayer == null)
                    {
                        imagePlayer = GetComponentInChildren<ImageSequencePlayer>();
                    }
                }
                catch
                {
                    Debug.Log("A sequence player is needed.");
                }
                trapToActivate.SetActive(false);
                trapSpriteRenderer = GetComponent<SpriteRenderer>();
                if (trapSpriteRenderer == null)
                {
                    trapSpriteRenderer = GetComponentInChildren<SpriteRenderer>();
                }
                trapSpriteRenderer.color = new Color(1.0f, 1.0f, 1.0f, starterAlphaIfAnimated);

                break;
        }
        
	}
	void OnTriggerEnter2D(Collider2D other)
    {
        if (!alreadyUsed && other.gameObject.CompareTag("Player"))
        {
            //if the player has entered the danger zone
            switch (trapType)
            {
                case TypeOfTrap.springOut:
                    StartCoroutine(SpringOut(springDistance, timeToFinish));
                    //startMoving = true;
                    alreadyUsed = true;
                    break;
                case TypeOfTrap.animatedWithEvent:
                    alreadyUsed = true;
                    ActivateTrapAtGivenFrame(frameToFireEvent);
                    break;

            }
        }
    }

    IEnumerator SpringOut(float springDistance, float timeToFinish)
    {
        /*
         * we also have to eliminate the current collider to 
         * avoid any conflicts between others in the scene.
         */
        Destroy(GetComponent<Collider2D>());
        //self collider eliminated.
        Vector3 objectIdlePos = objectToMove.position;
        while(Mathf.Abs(objectToMove.position.y - objectIdlePos.y) < springDistance)
        {
            objectToMove.position = Vector3.SmoothDamp(objectToMove.position,
            objectToMove.position + Vector3.up * springDistance, ref currentVelocity, timeToFinish);
            yield return null;
        }
        
    }

    void ActivateTrapAtGivenFrame(int givenFrame)
    {
        StartCoroutine(ScaleSpriteAlpha(0.0f, 1.0f, fadeInTime));        
    }

    IEnumerator ScaleSpriteAlpha(float start, float end, float time)
    {
        float lastTime = Time.realtimeSinceStartup;
        float timer = 0.0f;

        while (timer < time)
        {
            //fading out the character using its sprite renderer alpha
            trapSpriteRenderer.color = new Color(1.0f, 1.0f, 1.0f, Mathf.Lerp(start, end, timer / time));
            timer += (Time.realtimeSinceStartup - lastTime);
            lastTime = Time.realtimeSinceStartup;
            yield return null;
        }
    }
    IEnumerator WaitThenFadeTrapOut(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        StartCoroutine(ScaleSpriteAlpha(1.0f, 0.0f, fadeInTime));
        //trapToActivate.SetActive(false);
    }
    public void AnimationEvents(int frameToActivate)
    {
        if(frameToActivate >= imagePlayer.spriteSheet.Length - 1)
        {
            //meaning we need the last
            AfterAnimEvents();
        }else
        {
            DuringAnimEvents();
        }
        
    }
    void AfterAnimEvents()
    {
        StartCoroutine(WaitThenFadeTrapOut(0.1f));
        StartCoroutine(WaitThenDestroySelf(2.0f));
    }
    void DuringAnimEvents()
    {
        trapToActivate.SetActive(true);
    }
    IEnumerator WaitThenDestroySelf(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Destroy(this.gameObject);
    }
}
