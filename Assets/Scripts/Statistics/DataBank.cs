﻿using Assets.Checkpoints;
using Assets.MainMenuStuff;
using Assets.MainMenuStuff.DataStructures;
using Assets.MainMenuStuff.MMToolkitConsumers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;

namespace Assets.Scripts.Statistics
{
    /// <summary>
    /// Persistent class to hold data and "class libraries" for other scenes' functionality.
    /// </summary>
    public class DataBank : MonoBehaviour, ILoader
    {
        public static readonly string STATS_NAME = "/playerInfo_finsnightmare.txt";
        public static readonly string CHECKPNT_NAME = "/checkpoint_finsnightmare.txt";
        public string PERSISTENT_DATA_PATH { get; set; }
        
        //private LinkedList<GameObject> mainMenuGOs;
        //public LinkedList<GameObject> MainMenuGOs { get { return mainMenuGOs; } }
        public string currentBuildNumber;
        private MMtoolBundle mmTools;
        public MMtoolBundle MMTools { get { return mmTools; } }
        public CircularList<Color> baseColor;
        public CircularList<Color> baseColor2;
        public CircularList<Color> outlineColor;
        private Statistics_v2 stats;
        public Statistics_v2 Stats { get { return stats; } }

        private ICheckpointProvider checkpointProvider;
        public ICheckpointProvider CheckpointProvider { get { return checkpointProvider; } }

        private IDataValidator dataValidator;
        public T Load<T>(string nameOfFile)
            where T:class
        {
            string file = Application.persistentDataPath + nameOfFile;
            if (File.Exists(file))
            {
                string saveData;
                using (StreamReader sr = new StreamReader(file))
                {
                    saveData = sr.ReadToEnd();
                }
                return JsonUtility.FromJson<T>(saveData);
            }
            return null;
        }
        public void LoadDataFromINIT(Color[] baseColor, Color[] baseColor2,Color[] outlineColor)
        {
            /*
             * give values and load/check integrity.
             */
            this.baseColor = new CircularList<Color>();
            this.baseColor.ReadEnumerable(baseColor);

            this.baseColor2 = new CircularList<Color>();
            this.baseColor2.ReadEnumerable(baseColor2);

            this.outlineColor = new CircularList<Color>();
            this.outlineColor.ReadEnumerable(outlineColor);
            dataValidator = new Stats_v2_validator();
                        this.mmTools = new MMtoolBundle(this);
            //NOTE: STILL HAVE TO GIVE STATS UTILITY A STATS IF WE DIDN'T FIND ONE!!
            try
            {
                var readStats = Load<Statistics_v2>(STATS_NAME);
                dataValidator.ValidateEditorData(readStats);
                stats = readStats;
                MMTools.StatisticsUtility.Stats = stats;
            }
            catch
            {
                stats = null;
            }
            
            if (stats == null)
            {
                try
                {
                    stats = new Statistics_v2();
                    dataValidator.ValidateEditorData(stats);
                    MMTools.StatisticsUtility.Stats = stats;
                    MMTools.DisplayDataTools.CheckFileIntegrity(this, dataValidator.EditorLevelNames.ToArray(),
                        dataValidator.EditorLevelNumbers.ToArray());
                }
                catch { print("Couldn't verify save data."); }

                try
                {
                    Save(JsonUtility.ToJson(stats), STATS_NAME);
                }
                catch { print("COULDN'T SAVE AFTER CREATING NEW STATS."); }

            }

            this.checkpointProvider = new CheckpointUtility(this);

        }
        public void SaveAsync(string data, string name)
        {
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                Save(data, name);
            }).Start();
        }
        public void Save(string data, string name)
        {
            using (StreamWriter sw = new StreamWriter(PERSISTENT_DATA_PATH
                 + name))
            {
                sw.Write(data);
            }
        }
    }
}
