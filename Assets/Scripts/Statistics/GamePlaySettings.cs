﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Statistics
{
    [Serializable]
    public class GamePlaySettings
    {
        public bool highlights;
        public bool jumpAssist;
        public enum GameSpeed
        {
            Slow, Casual, Original
        }
        public GameSpeed gameSpeed;
        public GamePlaySettings()
        {
            highlights = true;
            gameSpeed = GameSpeed.Casual;
        }
    }
}
