﻿using System.Collections.Generic;

namespace Assets.Scripts.Statistics
{
    internal interface IDataValidator
    {
        List<int> EditorLevelNumbers { get; set; }
        List<string> EditorLevelNames { get; set; }
        void ValidateEditorData(Statistics_v2 loaded);
    }
}