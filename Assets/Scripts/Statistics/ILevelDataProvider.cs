﻿namespace Assets.Scripts.Statistics
{
    public interface ILevelDataProvider
    {
        int LevelNumber { get; }
        string Name { get; }
        int BestPercent { get; }
        uint DeathCount { get; }
    }
}