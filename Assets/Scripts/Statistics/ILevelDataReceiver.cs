﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Statistics
{
    public interface ILevelDataReceiver
    {
        int LevelNumber { set; }
        string Name { set; }
    }
}
