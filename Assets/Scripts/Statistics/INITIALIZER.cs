﻿using Assets.MainMenuStuff;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using System.Reflection;
using Assets.MainMenuStuff.MMToolkitConsumers;
using Assets.MainMenuStuff.DataStructures;
using Assets.MainMenuStuff.UInavigation;
using UnityEngine.SceneManagement;
using Assets.MainMenuStuff.Prefabs;
using System.Collections;
using Assets.ExceptionHandlers;

namespace Assets.Scripts.Statistics
{
    public sealed class INITIALIZER : MonoBehaviour
    {
        private LinkedList<GameObject> mainMenuGOs;
        /// <summary>
        /// A container that has all the gameobjects in the main menu.
        /// </summary>
        /// 
        [SerializeField]
        private DataBank DATA_blueprint;
        [HideInInspector]
        public DataBank DATA;
        public LinkedList<GameObject> MainMenuGOs { get { return mainMenuGOs; } }
        //private Statistics stats;
        //public Statistics Stats { get { return stats; } }
        //private MMtoolBundle mmTools;
        //public MMtoolBundle MMTools { get { return mmTools; } }
        public GameObject[] LevelBackgrounds;
        [HideInInspector]
        public CircularList<GameObject> CL_LevelBackgrounds;
        //public delegate void LoadData(Statistics data, Persistence pers);
        //public static event LoadData LOAD_DEPENDENCIES;
        ////public static UnityEvent<IPersistentData> loadNecessaryData;
        //public LevelDataDisplay levelData;
        public string TagToRecolor = "Recolorable";
        [HideInInspector]
        public List<Transform> camTargets;
        //[SerializeField]
        //private Material[] _playergMats;
        //[HideInInspector]
        //public CircularList<Material> playergMats;
        public CameraMovement mainCam;
        //[SerializeField]
        //private Material[] _midgMats;
        //[HideInInspector]
        //public CircularList<Material> midgMats;
        FillListContent scrollerFiller;
        public FillListContent ScrollerFiller { get { return scrollerFiller; } }
        //[SerializeField]
        //private Material[] _bgMats;
        //[HideInInspector]
        //public CircularList<Material> bgMats;
        public List<ISettingsReceiver> settingsReceivers;
        public Color[] _baseColor; 
        //public CircularList<Color> baseColor;

        public Color[] _baseColor2;
        //public CircularList<Color> baseColor2;

        public Color[] _outlineColor;
        //public CircularList<Color> outlineColor;

        private void Awake()
        {
            var d = GameObject.FindGameObjectWithTag("DataBank");
            if (d == null)
            {
                DATA = GameObject.Instantiate<DataBank>(DATA_blueprint);
                DATA.currentBuildNumber = Application.version;
                DATA.PERSISTENT_DATA_PATH = Application.persistentDataPath;
                DATA.LoadDataFromINIT(_baseColor, _baseColor2, _outlineColor);
                DontDestroyOnLoad(DATA);
            }else
            {
                this.DATA = d.GetComponent<DataBank>();
            }
            scrollerFiller = new FillListContent(this);
            //CRITICAL!!
            AdManager.AdManager.InitializeAds();

            ExceptionUtility.SetupExceptionHandling();
            
            if (CL_LevelBackgrounds == null)
            {
                CL_LevelBackgrounds = new CircularList<GameObject>();
                CL_LevelBackgrounds.ReadEnumerable(LevelBackgrounds);
                LevelBackgrounds = null;
            }
            mainCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraMovement>();
            //if (mmTools == null)
            //{
            //    mmTools = new MMtoolBundle(this);
            //}
            if (MainMenuGOs == null)
            {
                mainMenuGOs = new LinkedList<GameObject>(GameObject.FindObjectsOfType<GameObject>());
            }

            if (camTargets == null || camTargets.Count == 0)
            {
                camTargets = new List<Transform>();
                foreach (var item in MainMenuGOs
                    .Where(x => x.CompareTag("MM_cam_pos")))
                {
                    camTargets.Add(item.transform);
                }
            }
            /*
             * Now do what has to be done to use the main menu.
             * 1. all objects have to be active.
             * 2. find the scrollviewer inside leveldata and load it with stats.
             * 3. enable last used level background and disable everything else.
             * 4. recolor EVERYTHING according to the last played level.
             * 5. after that only the level selector needs recoloring, and when exiting that. 
             * 6. hide unnecessary canvases.
             */
            //1.
            foreach (var o in MainMenuGOs)
            {
                if (!o.activeSelf)
                {
                    o.SetActive(true);
                }
            }
            //2.
            /*
             * initializer will get all gameobjects with InitializeMe on them,
             * with the order specified in them. 
             * then we go through all of them and call the given methods.
             */
            foreach (var item in MainMenuGOs
                  .Where(x => x.GetComponent<InitializeMe>() != null)
                  .OrderBy(y => y.GetComponent<InitializeMe>().orderNumber))
            {
                //item.GetComponent<InitializeMe>().INIT(this);
                foreach (var i in item.GetComponents<InitializeMe>())
                {
                    i.INIT(this);
                }
            }

            //3. activate only the last played level bg
            DATA.MMTools.DisplayDataTools.ActivateOnlyOneOfThem(
                CL_LevelBackgrounds, DATA.Stats.LastPlayedLevel - 1);
            //4. recolour everything
            foreach (var item in MainMenuGOs
                .Where(x => x.CompareTag(TagToRecolor)))
            {
                DATA.MMTools.Colorizer.RecolourUI(item, DATA.baseColor[DATA.Stats.LastPlayedLevel - 1],
                   DATA.baseColor2[DATA.Stats.LastPlayedLevel - 1], DATA.outlineColor[DATA.Stats.LastPlayedLevel - 1]);
            }
            //5. setting up the UINavigation.
            /*
             * Basically subscribing to all the Origin events.
             */
            foreach (var item in MainMenuGOs
                .Where(x => x.GetComponent<Origin>() != null))
            {
                item.GetComponent<Origin>().BroadcastClick += DATA.MMTools.UINavTools.OnNavigate;
            }

            foreach (var c in MainMenuGOs
                .Where(x => x.GetComponent<Canvas>() != null))
            {
                if (!c.CompareTag("FirstMenu"))
                {
                    c.SetActive(false);
                }
            }
            //settingsReceivers = new List<ISettingsReceiver>();
            //foreach (var i in MainMenuGOs
            //    .Where(x => x.GetComponent(typeof(ISettingsReceiver)) != null))
            //{
            //    settingsReceivers.Add(i.GetComponent(typeof(ISettingsReceiver))
            //        as ISettingsReceiver);
            //}

            DATA.MMTools.SceneManager.SetTimeScale(DATA.MMTools.SceneManager.GlobalTimeScale);
            //finally, setting the main menu music volume.
            try
            {
                Camera.main.GetComponent<AudioSource>()
                    .volume = DATA.Stats.Settings.MusicVolume;
            }
            catch { }

            Resources.UnloadUnusedAssets();
        }
        private void Start()
        {
            //to be on the safe side.
            DATA.MMTools.SceneManager.GlobalTimeScale = 1;
            DATA.MMTools.SceneManager.SetTimeScale(DATA.MMTools.SceneManager.GlobalTimeScale);
            AdManager.AdManager.DestroyAllAds();
            AdManager.AdManager.GetSomeAdRequests();
            AdManager.AdManager.RequestInterstitial();
        }
        //private void Awake()
        //{
        //    stats = (Statistics)Load();
        //    if (stats == null)
        //    {
        //        stats = new Statistics();
        //        Save(Stats);
        //    }
        //}
        public void GetGeneratedObjects(GameObject go)
        {
            MainMenuGOs.AddLast(go);
            foreach (Transform c in go.transform)
            {
                MainMenuGOs.AddLast(c.gameObject);
            }
        }
        public void GetSettings(SettingsValues input)
        {
            DATA.Stats.Settings = input;
            
        }
    }
}
