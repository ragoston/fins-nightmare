﻿namespace Assets.Scripts.Statistics
{
    public interface IPersistentData
    {
        int LastPlayedLevel { get; set; }
        float MusicVolume { get; set; }
        float SFXVolume { get; set; }
        System.Collections.Generic.List<LevelResult> LevelDatas { get; set; }
    }
}