﻿namespace Assets.Scripts.Statistics
{
    public interface ISaver
    {
        void Save(Statistics_v2 data);
    }
}