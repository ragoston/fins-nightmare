﻿namespace Assets.Scripts.Statistics
{
    internal interface ITotalDeathCount
    {
        uint TotalDeathCount { get; }
        uint GetTotalDeathCount(System.Collections.Generic.
            IEnumerable<ILevelDataProvider> collection);
    }
}