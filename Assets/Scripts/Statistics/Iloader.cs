﻿namespace Assets.Scripts.Statistics
{
    internal interface ILoader
    {
        T Load<T>(string nameOfFile) where T : class;
    }
}