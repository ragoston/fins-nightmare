﻿//using Assets.MainMenuStuff.DataStructures;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using UnityEngine;
//using UnityEngine.Events;
//using UnityEngine.UI;
//using UnityEngine.SceneManagement;
//using Assets.MainMenuStuff;

//namespace Assets.Scripts.Statistics
//{
//    /// <summary>
//    /// View model to input the level data from, and to serialize that to a common database.
//    /// </summary>
//    [Serializable]
//    public class LevelDataDisplay : MonoBehaviour
//    {
//        Persistence pers;
//        public Statistics data;
//        //[SerializeField]
//        //private GameObject[] levelBGs;
//        //private CircularList<GameObject> LevelBGs = new CircularList<GameObject>();
//        [SerializeField]
//        private Text _levelName;
//        public void SetLevelName(string name)
//        {
//            _levelName.text = name;
//        }
//        [SerializeField]
//        private Text _progress;
//        public void SetProgress(int percent)
//        {
//            _progress.text = "Progress: " + percent.ToString() + "%";
//        }
//        [SerializeField]
//        private Image fill;
//        public void SetFill(int percent)
//        {
//            var _percent = (float)(percent / 100f);
//            fill.fillAmount = _percent;
//        }
//        [SerializeField]
//        private Text _deathCount;
//        public void SetDeathCount(uint deathCount)
//        {
//            _deathCount.text = "Death count: " + deathCount.ToString();
//        }
//        public int[] levelNumbers;
//        public string[] levelNames;
//        //intended for display so the player can scroll over.
//        private CircularList<LevelData> datas = new CircularList<LevelData>();
//        [InvocationOrder(3)]
//        private void FetchData(Statistics data, Persistence pers)
//        {
//            this.data = data;
//            this.pers = pers;
//            datas.ReadEnumerable(CorrectLevelDatas(data, pers));

//            Init(data);
//        }
//        public void Init(Statistics data)
//        {
//            //try
//            //{
//                foreach (var item in LevelBGs)
//                {
//                    if(item != null)item.SetActive(false);
//                }
//                index = data.LastPlayedLevel - 1;
//                LevelBGs[index].SetActive(true);
//                int res = OnTurn(0);
//                pers.colorize.RecolourUI(res, true);
//            //}
//            //catch (Exception ex) { Debug.Log(ex.Message); }

//        }
//        ///// <summary>
//        ///// Check for new level, check for renamed levels or changed level numbers.
//        ///// </summary>
//        ///// <param name="data">Input loaded from file to perform check on.</param>
//        //private List<LevelData> CorrectLevelDatas(Statistics data, ISaver saver)
//        //{
//        //    //but it won't be, checked in Persistence.
//        //    if (data == null) return null;
//        //    //but it won't be either.
//        //    if (data.LevelDatas == null) return null;
//        //    for (int i = 0; i < levelNumbers.Length; i++)
//        //    {
//        //        try
//        //        {
//        //            if(data.LevelDatas[i].Name != levelNames[i])
//        //            {
//        //                data.LevelDatas[i].Name = levelNames[i];
//        //            }
//        //            if(data.LevelDatas[i].LevelNumber != levelNumbers[i])
//        //            {
//        //                data.LevelDatas[i].LevelNumber = levelNumbers[i];
//        //            }
//        //        }
//        //        catch
//        //        {
//        //            //then new level.
//        //            data.LevelDatas.Add(new LevelData(levelNumbers[i], levelNames[i]));
//        //        }
//        //    }
//        //    //saving the modified data.
//        //    saver.Save(data);
//        //    return data.LevelDatas;
//        //}
//        //private void Awake ()
//        //{
//        //    Persistence.LOAD_DEPENDENCIES += FetchData;

//        //    LevelBGs.ReadEnumerable(levelBGs);
//        //}
//        //private void OnDisable()
//        //{
//        //    Init(data);
//        //}
//        private int index;
//        public int Index { get { return index; } }
//        public void Turn(int change)
//        {
//            //if(change > 0)
//            //{
//            //    //middleTarget.position = middlePos;
//            //    MoveAction mLeft = new MoveAction(middleTarget,middlePos, leftSideTarget.position,
//            //        0.15f);
//            //    MoveAction mIn = new MoveAction(middleTarget, rightSideTarget.position,middlePos,
//            //        0.15f);
//            //    mLeft.NEXT_FUNCTION += mIn.Move;
//            //    mLeft.MoveSwapPos(this);
//            //}else if (change < 0)
//            //{
//            //    //middleTarget.position = middlePos;
//            //    MoveAction mRight = new MoveAction(middleTarget,middlePos, rightSideTarget.position,
//            //        0.15f);
//            //    MoveAction mIn = new MoveAction(middleTarget, leftSideTarget.position,middlePos,
//            //        0.15f);
//            //    mRight.NEXT_FUNCTION += mIn.Move;
//            //    mRight.MoveSwapPos(this);
//            //}
//            StartCoroutine(_Turn(change));
//        }
//        IEnumerator _Turn(int change)
//        {
//            yield return new WaitForEndOfFrame();
//            OnTurn(change);
//        }
//        int OnTurn(int change)
//        {
//            LevelBGs[index].SetActive(false);
//            index = index + change;
//            SetLevelName(datas[index].Name);
//            SetProgress(datas[index].BestPercent);
//            SetFill(datas[index].BestPercent);
//            SetDeathCount(datas[index].DeathCount);
//            //LevelBGs[index - change].SetActive(false);
//            LevelBGs[index].SetActive(true);
//            //MainMenuStuff.Threading.RecolorUIElements_JOB job =
//            //    new MainMenuStuff.Threading.RecolorUIElements_JOB(index, pers.colorize.RecolourUI);
//            //job.Start();
//            //pers.colorize.RecolourUI(index);

//            return index;
//        }
//        ///// <summary>
//        ///// Setting the main menu background theme (while level to display).
//        ///// </summary>
//        ///// <param name="levelNumber">The levelnumber</param>
//        //private void SetMenuBackground(int levelNumber)
//        //{
//        //    foreach (var item in LevelBGs)
//        //    {
//        //        if (item.activeSelf) item.SetActive(false);
//        //    }
//        //    LevelBGs[levelNumber].SetActive(true);
//        //    //LevelBGs[levelNumber].transform.Find()
//        //}
//        public void LoadGivenLevel()
//        {
//            SceneManager.LoadScene(index+1, LoadSceneMode.Single);
//        }
        
//    }
//}
