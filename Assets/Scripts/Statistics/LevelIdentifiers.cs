﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Statistics
{
    [Serializable]
    public class LevelIdentifiers
    {
        public string Name;
        public int LevelNumber;
        public LevelIdentifiers(string name, int levelNumber)
        {
            Name = name;
            LevelNumber = levelNumber;
        }
    }
}
