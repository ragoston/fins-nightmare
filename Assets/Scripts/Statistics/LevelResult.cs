﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Statistics
{
    [Serializable]
    public class LevelResult
    {
        public LevelResult(int levelNumber)
        {
            this.BestPercent = 0;
            this.DeathCount = 0;
            this.levelNumber = levelNumber;
        }
        public int levelNumber;
        public int BestPercent;
        public uint DeathCount;
    }
}
