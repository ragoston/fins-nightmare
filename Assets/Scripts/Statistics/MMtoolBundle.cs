﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Text;
using Assets.MainMenuStuff;
using Assets.MainMenuStuff.Prefabs;
using System.Reflection;

namespace Assets.Scripts.Statistics
{
    /// <summary>
    /// Holds tools that are needed in UI management.
    /// </summary>
    public class MMtoolBundle
    {
        ColorizeUI colorizer;
        public ColorizeUI Colorizer { get { return colorizer; } }
        //FillListContent scrollerFiller;
        //public FillListContent ScrollerFiller { get { return scrollerFiller; } }
        DisplayLevelData displayDataTools;
        public DisplayLevelData DisplayDataTools { get { return displayDataTools; } }
        UINavigationTools uiNavTools;
        public UINavigationTools UINavTools { get { return uiNavTools; } }
        private UnifiedSceneManager sceneManager;
        public UnifiedSceneManager SceneManager { get { return sceneManager; } }

        public StatisticsUtility StatisticsUtility
        {
            get
            {
                return statisticsUtility;
            }
        }

        private StatisticsUtility statisticsUtility;

        public MMtoolBundle(DataBank bank)
        {
            colorizer = new ColorizeUI();
            //scrollerFiller = new FillListContent(pers);
            displayDataTools = new DisplayLevelData();
            uiNavTools = new UINavigationTools(bank);
            sceneManager = new UnifiedSceneManager(bank);
            statisticsUtility = new StatisticsUtility(
                (p) =>
                {
                    p = bank.Stats.GamePlaySettings;
                    if(p.gameSpeed == GamePlaySettings.GameSpeed.Original
                    && !p.highlights)
                    {
                        return true;
                    }
                    return false;
                });
        }

    }
}
