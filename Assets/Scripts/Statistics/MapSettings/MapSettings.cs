﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Statistics.MapSettings
{
    public class MapSettings
    {
        public static float MapValues<T>(T key, float beginning, float step)
        {
            Dictionary<T, float>
                dict;
            dict =
                new Dictionary<T, float>();
            foreach (T item in
                Enum.GetValues(typeof(T)))
            {
                dict.Add(item, beginning);
                beginning += step;
            }
            return dict[key];
        }
    }
}
