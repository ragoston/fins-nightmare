﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Statistics
{
    class ProgressBar : MonoBehaviour
    {
        private int bestPercent;
        public Text bestPercentText;
        public Text levelName;
        public Image fill;
        private void Awake()
        {
            SetBestPercent(70);
        }
        public void SetBestPercent(int v)
        {
            bestPercent = v;
            bestPercentText.text = "Progress: " + bestPercent.ToString() + "%";
            fill.fillAmount = (float)(v/100f);
            
        }
    }
}
