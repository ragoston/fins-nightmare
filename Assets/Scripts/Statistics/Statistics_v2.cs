﻿using Assets.MainMenuStuff.MMToolkitConsumers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Statistics
{
    [Serializable]
    public class Statistics_v2
    {
        /// <summary>
        /// Getting the last played level to initialize the main menu. This is NOT an index.
        /// </summary>
        public int LastPlayedLevel;
        public Statistics_v2()
        {
            originalLevelResults = new List<LevelResult>();
            assistedLevelResults = new List<LevelResult>();
            levelIdentifiers = new List<LevelIdentifiers>();
            LastPlayedLevel = 1;
            Settings = new SettingsValues();
            GamePlaySettings = new GamePlaySettings();
        } 
        public GamePlaySettings GamePlaySettings;
        public SettingsValues Settings;
        //first: original
        //second: assisted
        public List<LevelResult> originalLevelResults;
        public List<LevelResult> assistedLevelResults;
        public List<LevelIdentifiers> levelIdentifiers;
        public string buildNumber;
    }
}
