﻿using Assets.MainMenuStuff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Statistics
{
    internal class Stats_v2_validator : IDataValidator
    {

        public List<int> EditorLevelNumbers { get; set; }

        public List<string> EditorLevelNames { get; set; }
        private LevelSelectHelper editorData;
        public Stats_v2_validator()
        {
            editorData = GameObject.FindObjectOfType<LevelSelectHelper>();
            EditorLevelNames = editorData.levelNames.ToList();
            EditorLevelNumbers = editorData.levelNumbers.ToList();
        }
        public void ValidateEditorData(Statistics_v2 loaded)
        {    
            if (editorData == null)
                return;
            for (int i = 0; i < loaded.levelIdentifiers.Count; i++)
            {
                //going to the end of the smaller.
                //cuz if I added new level, then save data is less.
                //the editor is the most up to date.
                if(loaded.levelIdentifiers[i].Name !=
                    editorData.levelNames[i])
                {
                    //if first name doesn't match, we have a new first.
                    //so we have to shift all levels to the right by one for the
                    //loaded value.
                    loaded.levelIdentifiers.Add(new LevelIdentifiers("newLevel",
                        loaded.levelIdentifiers.Count));
                    loaded.originalLevelResults.Add(new LevelResult(
                        loaded.levelIdentifiers.Count));
                    loaded.assistedLevelResults.Add(new LevelResult(
                        loaded.levelIdentifiers.Count));

                    //shifting the data
                    for (int j = loaded.levelIdentifiers.Count-2;
                        j >= i; j--)
                    {
                        loaded.levelIdentifiers[j + 1].Name
                            = loaded.levelIdentifiers[j].Name;
                        loaded.levelIdentifiers[j + 1].LevelNumber
                            = loaded.levelIdentifiers[j].LevelNumber;

                        loaded.originalLevelResults[j + 1].BestPercent
                            = loaded.originalLevelResults[j].BestPercent;
                        loaded.originalLevelResults[j + 1].DeathCount
                            = loaded.originalLevelResults[j].DeathCount;

                        loaded.assistedLevelResults[j + 1].BestPercent
                            = loaded.assistedLevelResults[j].BestPercent;
                        loaded.assistedLevelResults[j + 1].DeathCount
                            = loaded.assistedLevelResults[j].DeathCount;

                    }
                    loaded.levelIdentifiers[i].Name =
                        editorData.levelNames[i];
                    loaded.levelIdentifiers[i].LevelNumber =
                        editorData.levelNumbers[i];
                    loaded.originalLevelResults[i].BestPercent = 0;
                    loaded.originalLevelResults[i].DeathCount = 0;
                    loaded.assistedLevelResults[i].BestPercent = 0;
                    loaded.assistedLevelResults[i].DeathCount = 0;
                }
            }
        }
    }
}
