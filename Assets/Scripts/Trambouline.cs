﻿using UnityEngine;
using System.Collections;

public class Trambouline : MonoBehaviour
{
    //we want all of our trampoines to behave as a simple collider 
    //if the player is hit or died. 
    //hence the static.
    //but if the player gets hit while INSIDE the collider
    //then we don't want him/her to fall through.
    public static bool behaveAsCollider;
    private bool alreadyUsed;
    private static Sprite linenIdle;
    private static Sprite linenPulled;
    private SpriteRenderer linenSpRend;
    public GameObject LinenGO;
    public float doubleJumpFactor = 1.5f;
    private DoubleJumpTrambouline checkDoubleJump;
	// Use this for initialization
    void Start()
    {
        behaveAsCollider = false;
        alreadyUsed = false;
        linenIdle = null;
        linenPulled = null;
        linenIdle = Resources.Load<Sprite>("Trambouline_sprites/trambouline_linen");
        linenPulled = Resources.Load<Sprite>("Trambouline_sprites/trambouline_linen_pulled");
        linenSpRend = LinenGO.GetComponent<SpriteRenderer>();
        checkDoubleJump = GetComponentInChildren<DoubleJumpTrambouline>();
    }
    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.CompareTag("Player"))
        {
            linenSpRend.sprite = linenPulled;
            Rigidbody2D otherRB = other.gameObject.GetComponent<Rigidbody2D>();
            if (!alreadyUsed && !CharacterController2D.playerDied)
            {

                otherRB.velocity = new Vector2(otherRB.velocity.x, 0.0f);
                if (checkDoubleJump.didThePlayerJump)
                {
                    otherRB.velocity = new Vector2(otherRB.velocity.x, CharacterController2D.charMovement.jumpSpeed * 1.5f);              
                    //otherRB.AddForce(Vector2.up * CharacterController2D.jumpForce * doubleJumpFactor);
                }
                else
                {
                    otherRB.velocity = new Vector2(otherRB.velocity.x, CharacterController2D.charMovement.jumpSpeed);
                    //otherRB.AddForce(Vector2.up * CharacterController2D.jumpForce);
                }
                alreadyUsed = true;
                checkDoubleJump.didThePlayerJump = false;
            }
            StartCoroutine(ChangeSpritesBack());
            checkDoubleJump.jumpDone = true;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        alreadyUsed = false;
        
    }
    IEnumerator ChangeSpritesBack()
    {
        yield return new WaitForSeconds(0.12f);
        linenSpRend.sprite = linenIdle;
    }
}
