﻿using UnityEngine;
using System.Collections;
using Assets.MainMenuStuff;
using Assets.JumpAssist.Trampolines;
using System;

public class TramboulineEnhanced : MonoBehaviour, ITrampolineFunctionality
{
    private Sprite linenIdle;
    private Sprite linenPulled;
    private SpriteRenderer linenSpRend;
    public GameObject LinenGO;
    public float jumpedMultiplier = 1.3f;
    public float nonJumpMultiplier = 1f;

    public Func<Rigidbody2D, Vector2> GetSimpleJump { get; set; }

    public Func<Rigidbody2D, Vector2> GetDoubleJump { get; set; }

    // Use this for initialization
    void Start ()
    {
        linenIdle = Resources.Load<Sprite>("Trambouline_sprites/trambouline_linen");
        linenPulled = Resources.Load<Sprite>("Trambouline_sprites/trambouline_linen_pulled");
        linenSpRend = LinenGO.GetComponent<SpriteRenderer>();

        GetSimpleJump = (otherRB) => { return new Vector2(otherRB.velocity.x,
            CharacterController2D.charMovement.jumpSpeed * nonJumpMultiplier); };

        GetDoubleJump = (otherRB) =>
        {
            return new Vector2(otherRB.velocity.x,
                CharacterController2D.charMovement.jumpSpeed * jumpedMultiplier);
        };
    }
	
	// Update is called once per frame
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && !CharacterController2D.charMovement.movementDisabled
            && !CharacterController2D.playerDied)
        {
            linenSpRend.sprite = linenPulled;
            //meaning the player is inside the field that can set our velocity
            Rigidbody2D otherRB = other.gameObject.GetComponent<Rigidbody2D>();
            if (/*Input.GetButton("Jump")*/ JumpInput.JUMP)
            {
                //originally:
                //otherRB.velocity = new Vector2(otherRB.velocity.x, CharacterController2D.jumpSpeed * 1.3f);

                //otherRB.velocity = new Vector2(otherRB.velocity.x, CharacterController2D.charMovement.jumpSpeed * jumpedMultiplier);
                otherRB.velocity = GetDoubleJump.Invoke(otherRB);

            }
            else
            {
                //originally
                //otherRB.velocity = new Vector2(otherRB.velocity.x, CharacterController2D.jumpSpeed);
                //otherRB.velocity = new Vector2(otherRB.velocity.x, CharacterController2D.charMovement.jumpSpeed * nonJumpMultiplier);
                otherRB.velocity = GetSimpleJump.Invoke(otherRB);
            }
            StartCoroutine(ChangeSpritesBack());
        }
    }
    IEnumerator ChangeSpritesBack()
    {
        yield return new WaitForSeconds(0.12f);
        linenSpRend.sprite = linenIdle;
    }

}
