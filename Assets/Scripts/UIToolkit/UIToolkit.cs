﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
namespace Assets.Scripts
{
    public static class StopGivenCoroutines
    {
        public static void StopCoroutines(ref IEnumerator cor, MonoBehaviour mono)
        {
            if (cor != null && mono != null)
            {
                mono.StopCoroutine(cor);
            }            
        }
        public static void StopCoroutines(ref IEnumerator[] cors, MonoBehaviour mono)
        {
            for (int i = 0; i < cors.Length; i++)
            {
                if (cors[i] != null && mono != null)
                {
                    mono.StopCoroutine(cors[i]);
                }              
            }
        }
    }
    public static class InvokeMultipleAnimations
    {
        
        public static void InvokeAnim(float timeBeforeStart, List<Action<MonoBehaviour>> FunctionsToInvokeTogether,
    IEnumerator[] coroutinesOfAnimations, MonoBehaviour mono)
        {
            if(coroutinesOfAnimations.Length != FunctionsToInvokeTogether.Count)
            {
                return;
                //throw new WrongNumbersException("Trying to invoke with improper number of inputs.");
            }
            for (int i = 0; i < coroutinesOfAnimations.Length; i++)
            {
                if(coroutinesOfAnimations[i] != null && mono != null)
                {
                    mono.StopCoroutine(coroutinesOfAnimations[i]);
                }
                coroutinesOfAnimations[i] = _InvokeAnim(timeBeforeStart, FunctionsToInvokeTogether, mono);
                try
                {
                    mono.StartCoroutine(coroutinesOfAnimations[i]);
                }catch
                {
                    
                }
                
            }
        }
        static IEnumerator _InvokeAnim(float timeBeforeStart, List<Action<MonoBehaviour>> FunctionsToInvokeTogether,
            MonoBehaviour mono)
        {
            if (mono != null)
            {
                yield return new WaitForRealSecondsClass().WaitForRealSeconds(timeBeforeStart, mono);
                for (int i = 0; i < FunctionsToInvokeTogether.Count; i++)
                {
                    FunctionsToInvokeTogether[i](mono);
                }
            }

        }
    }
    public class WrongNumbersException : Exception
    {
        public WrongNumbersException(string message)
            :base(message)
        {
            Debug.Log(message);
        }
    }
    public class WaitForRealSecondsClass
    {
        #region Wait for real seconds
        /// <summary>
        /// Excludes the time scale: always real-time.
        /// </summary>
        /// <param name="aTime"></param>
        /// <returns></returns>
        public Coroutine WaitForRealSeconds(float aTime, MonoBehaviour mono)
        {
            return mono.StartCoroutine(_WaitForRealSeconds(aTime));
        }
        private IEnumerator _WaitForRealSeconds(float aTime)
        {
            while (aTime > 0.0f)
            {
                aTime -= Mathf.Clamp(Time.unscaledDeltaTime, 0, 0.2f);
                //aTime -= Time.unscaledDeltaTime;
                yield return null;
            }
        }
        #endregion
    }
    [Serializable]
    public class MoveAction : UIAnimation
    {
        #region Move
        public MoveAction()
        {

        }
        public void InvokeAnim(float timeBeforeStart, Action<MonoBehaviour> MoveFunction,
            MonoBehaviour mono)
        {
            if (moveRoutine != null && mono != null)
            {
                mono.StopCoroutine(moveRoutine);
            }
            moveRoutine = _InvokeAnim(timeBeforeStart, MoveFunction, mono);
            if (mono != null) mono.StartCoroutine(moveRoutine);
            else if (NEXT_FUNCTION != null && mono != null) NEXT_FUNCTION(mono);
        }
        IEnumerator _InvokeAnim(float timeBeforeStart, Action<MonoBehaviour> MoveFunction,
            MonoBehaviour mono)
        {
            if (mono != null)
            {
                yield return new WaitForRealSecondsClass().WaitForRealSeconds(timeBeforeStart, mono);
                MoveFunction(mono);
            }
            else yield return null;

        }

        //to ensure only one mover coroutine can be active.
        IEnumerator moveRoutine = null;
        public IEnumerator MoveRoutine
        {
            get { return moveRoutine; }
        }
        /// <summary>
        /// Moves a UnityEngine.GameObject from position A to position B over timeToReachDestination.
        /// Uses Coroutines.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="overTime"></param>
        /// 
        #region Solution 2: using fields and not parameters
        Transform from;
        List<Transform> froms;
        Vector3 to;
        List<Vector3> tos;
        Vector3 fromWhere;
        float overTime;
        public delegate void UIchain(MonoBehaviour mono);
        public event UIchain NEXT_FUNCTION;
        
        public MoveAction(Transform from, Vector3 to, float overTime)
        {
            this.from = from;
            this.to = to;
            this.overTime = overTime;

        }
        public MoveAction(Transform target, Vector3 from, Vector3 to, float overTime)
        {
            fromWhere = from;
            this.from = target;
            this.to = to;
            this.overTime = overTime;
        }
        public MoveAction(List<Transform> froms, List<Vector3> tos, float overTime)
        {
            if(!(froms.Count == tos.Count))
            {
                return;
                //throw new WrongNumbersException("Wrong number of inputs - Move.");
            }
            this.froms = froms;
            this.tos = tos;
            this.overTime = overTime;
        }
        public void MoveSwapPos(MonoBehaviour mono)
        {
            if (moveRoutine != null && mono != null)
            {
                mono.StopCoroutine(moveRoutine);
            }
            moveRoutine = _MoveSwapPos(from,fromWhere, to, overTime, mono);
            try
            {
                mono.StartCoroutine(moveRoutine);
            }catch
            {
                if(NEXT_FUNCTION != null && mono != null)
                {
                    NEXT_FUNCTION(mono);
                }
            }
            
        }
        IEnumerator _MoveSwapPos(Transform target, Vector3 from, Vector3 to, 
            float overTime, MonoBehaviour mono)
        {
            Vector2 original = from;
            float timer = 0.0f;
            while (timer < overTime)
            {
                float step = Vector2.Distance(original, to) * (Time.deltaTime / overTime);
                target.position = Vector2.MoveTowards(target.position, to, step);
                timer += Time.deltaTime;
                yield return null;
            }
            if (NEXT_FUNCTION != null && mono != null)
            {
                NEXT_FUNCTION(mono);
            }
        }
        public void Move(MonoBehaviour mono)
        {
            if (moveRoutine != null && mono != null)
            {
                mono.StopCoroutine(moveRoutine);
            }
            moveRoutine = _Move(from, to, overTime, mono);
            try
            {
                mono.StartCoroutine(moveRoutine);
            }catch
            {
                if(NEXT_FUNCTION != null && mono != null)
                {
                    NEXT_FUNCTION(mono);
                }
            }
            
        }
        IEnumerator _Move(Transform from, Vector3 to, float overTime, MonoBehaviour mono)
        {
            Vector2 original = from.position;
            float timer = 0.0f;
            while (timer < overTime)
            {
                float step = Vector2.Distance(original, to) * (Time.deltaTime / overTime);
                from.position = Vector2.MoveTowards(from.position, to, step);
                timer += Time.deltaTime;
                yield return null;
            }
            if(NEXT_FUNCTION != null && mono != null)
            {
                NEXT_FUNCTION(mono);
            }
        }
        //now for a list of objects
        public void MoveMultiple(MonoBehaviour mono)
        {
            if (moveRoutine != null && mono != null)
            {
                mono.StopCoroutine(moveRoutine);
            }
            moveRoutine = _MoveMultiple(froms, tos, overTime, mono);
            try
            {
                mono.StartCoroutine(moveRoutine);
            }catch
            {
                if(NEXT_FUNCTION != null && mono != null)
                {
                    NEXT_FUNCTION(mono);
                }
            }
            
        }
        IEnumerator _MoveMultiple(List<Transform> froms, List<Vector3> tos, float overTime, MonoBehaviour mono)
        {
            Vector2[] originals = new Vector2[froms.Count];
            for (int i = 0; i < froms.Count; i++)
            {
                originals[i] = froms[i].position;
            }
            float timer = 0.0f;
            while (timer < overTime)
            {
                for (int i = 0; i < froms.Count; i++)
                {

                    float step = Vector2.Distance(originals[i], tos[i]) * (Time.deltaTime / overTime);
                    froms[i].position = Vector2.MoveTowards(froms[i].position, tos[i], step);
                }
                //from.position = Vector2.MoveTowards(from.position, to.position, step);
                timer += Time.deltaTime;
                yield return null;
            }
            if (NEXT_FUNCTION != null && mono != null)
            {
                NEXT_FUNCTION(mono);
            }
        }
        #endregion
        #endregion
    }
    [Serializable]
    public class RotateAction : UIAnimation
    {
        #region Rotations
        public delegate void UIchain(MonoBehaviour mono);
        public event UIchain NEXT_FUNCTION;
        IEnumerator rotateRoutine = null;
        public IEnumerator RotateRoutine
        {
            get { return rotateRoutine; }
        }
        #region Using fields instead of parameters
        Transform whatToRotate;
        List<Transform> whatsToRotate;
        Vector3 byAngles;
        List<Vector3> byAnglesS;
        List<Vector3> fromWheres;
        Vector3 fromWhere;
        float inTime;

        public void InvokeAnim(float timeBeforeStart, Action<MonoBehaviour> RotateFunction,
    MonoBehaviour mono)
        {
            if (rotateRoutine != null)
            {
                mono.StopCoroutine(rotateRoutine);
            }
            rotateRoutine = _InvokeAnim(timeBeforeStart, RotateFunction, mono);
            mono.StartCoroutine(rotateRoutine);
        }
        IEnumerator _InvokeAnim(float timeBeforeStart, Action<MonoBehaviour> MoveFunction,
            MonoBehaviour mono)
        {
            yield return new WaitForRealSecondsClass().WaitForRealSeconds(timeBeforeStart, mono);
            MoveFunction(mono);
        }

        public RotateAction(Transform whatToRotate, Vector3 fromWhere, 
            Vector3 byAngles, float inTime)
        {
            this.whatToRotate = whatToRotate;
            this.byAngles = byAngles;
            this.fromWhere = fromWhere;
            this.inTime = inTime;
        }
        public RotateAction(List<Transform> whatsToRotate, List<Vector3> fromWheres,
    List<Vector3> byAnglesS, float inTime)
        {
            if(!(whatsToRotate.Count == fromWheres.Count && fromWheres.Count == byAnglesS.Count))
            {
                throw new WrongNumbersException("Wong number of input - Rotate.");
            }
            this.whatsToRotate = whatsToRotate;
            this.byAnglesS = byAnglesS;
            this.fromWheres = fromWheres;
            this.inTime = inTime;
        }
        public void RotateByGiven(MonoBehaviour mono)
        {
            Quaternion fromWhereQuat = Quaternion.Euler(fromWhere);
            if (rotateRoutine != null)
            {
                mono.StopCoroutine(rotateRoutine);
            }
            rotateRoutine = RotateMe(whatToRotate, fromWhereQuat, byAngles, inTime, mono);
            //StartCoroutine(RotateMe(rotThis, fromWhereQuat, rotateBythis, time, chain));
            mono.StartCoroutine(rotateRoutine);
        }
        IEnumerator RotateMe(Transform whatToRotate, Quaternion fromWhere, Vector3 byAngles, float inTime, MonoBehaviour mono)
        {
            for (float t = 0.0f; t < (inTime - (Time.deltaTime / 2.0f)); t += Time.deltaTime)
            {
                whatToRotate.rotation = Quaternion.Euler(
        new Vector3(whatToRotate.eulerAngles.x, whatToRotate.eulerAngles.y, whatToRotate.eulerAngles.z + (byAngles.z /
        (inTime / (Time.deltaTime)))));
                //whatToRotate.rotation = Quaternion.Slerp(fromAngle, toAngle, t);
                yield return null;
                #region Da fuq to do
                /*
                 * instead of calling a function that rotates it, rather 
                 * rotate it by meself. how? 
                 * We need the amount it would rotate and rotate by that. 
                 * That is: 
                 * 1. how many times will the loop run? X = inTime / (deltaTime/inTime). 
                 * 2. so how many times can I rotate? exact same. 
                 * 3. I have an amount to rotate: say 120 degrees.
                 * 4. divide it to X pieces - so rotate 120/X times.
                 * that's that. 
                */
                #endregion
            }
            #region Why it isn't accurate and how to get around it?
            /*
             * So it rotated, fine. But was it accurate? No. 
             * This is probably due to the last step which calculates from the previous
             * frame's delay time. to get around this, we just need to
             * _set the rotation of the given gameobject to the desired euler angles. 
            */
            #endregion
            float desired = byAngles.z;
            while (desired - 360 >= 0)
            {
                desired -= 360;
            }
            whatToRotate.rotation = Quaternion.Euler(new Vector3(0, 0, desired));
            if (NEXT_FUNCTION != null)
            {
                NEXT_FUNCTION(mono);
            }
        }
        //for multiple:
        public void RotateByGivenMultiple(MonoBehaviour mono)
        {
            //Quaternion fromWhereQuat = Quaternion.Euler(fromWhere);
            List<Quaternion> fromWheresQuat = new List<Quaternion>();
            for (int i = 0; i < fromWheresQuat.Count; i++)
            {
                fromWheresQuat[i] = Quaternion.Euler(fromWheres[i]);
            }
            if (rotateRoutine != null)
            {
                mono.StopCoroutine(rotateRoutine);
            }
            rotateRoutine = RotateMeMultiple(whatsToRotate, fromWheresQuat, byAnglesS, inTime, mono);
            //StartCoroutine(RotateMe(rotThis, fromWhereQuat, rotateBythis, time, chain));
            mono.StartCoroutine(rotateRoutine);
        }
        IEnumerator RotateMeMultiple(List<Transform> whatsToRotate, List<Quaternion> fromWheres, List<Vector3> byAnglesS, float inTime, MonoBehaviour mono)
        {
            for (float t = 0.0f; t < (inTime - (Time.deltaTime / 2.0f)); t += Time.deltaTime)
            {
                for (int i = 0; i < whatsToRotate.Count; i++)
                {
                    whatsToRotate[i].rotation = Quaternion.Euler(
new Vector3(whatsToRotate[i].eulerAngles.x, whatsToRotate[i].eulerAngles.y, whatsToRotate[i].eulerAngles.z + (byAngles.z /
(inTime / (Time.deltaTime)))));
                }
                yield return null;
                #region Da fuq to do
                /*
                 * instead of calling a function that rotates it, rather 
                 * rotate it by meself. how? 
                 * We need the amount it would rotate and rotate by that. 
                 * That is: 
                 * 1. how many times will the loop run? X = inTime / (deltaTime/inTime). 
                 * 2. so how many times can I rotate? exact same. 
                 * 3. I have an amount to rotate: say 120 degrees.
                 * 4. divide it to X pieces - so rotate 120/X times.
                 * that's that. 
                */
                #endregion
            }
            #region Why it isn't accurate and how to get around it?
            /*
             * So it rotated, fine. But was it accurate? No. 
             * This is probably due to the last step which calculates from the previous
             * frame's delay time. to get around this, we just need to
             * _set the rotation of the given gameobject to the desired euler angles. 
            */
            #endregion

            for (int i = 0; i < whatsToRotate.Count; i++)
            {
                float desired = byAnglesS[i].z;
                while (desired - 360 >= 0)
                {
                    desired -= 360;
                }
                whatsToRotate[i].rotation = Quaternion.Euler(new Vector3(0, 0, desired));
            }
            //whatToRotate.rotation = Quaternion.Euler(new Vector3(0, 0, desired));
            if (NEXT_FUNCTION != null)
            {
                NEXT_FUNCTION(mono);
            }
        }
        #endregion
        /// <summary>
        /// Rotate to the desired angle from the given angle over time.
        /// </summary>
        /// <param name="whatToRotate"></param>
        /// <param name="rotateToThis"></param>
        /// <param name="overTime"></param>
        public void RotateToGiven(MonoBehaviour mono)
        {
            #region Needed but solved
            /*
             * HINT: INSTEAD OF USING THE BUILT-IN FUNCTIONS, JUST 
             * INCREASE THE ROTATION BY SMALL INCREMENTS.
             * SAY 0.5 DEGREES, OR DEGREES CALCULATED LIKE THE
             * STEP IN MOVEMENT. 
             * THEN JUST NEW QUATERNION FROM EULER ANGLES
             * IN WHOM WE JUST ADD THE SMALL INCREMENTS.
             */
            #endregion
            if(rotateRoutine != null && mono != null)
            {
                mono.StopCoroutine(rotateRoutine);
            }
            rotateRoutine = _RotateMe(whatToRotate, byAngles, inTime, mono);
            //StartCoroutine(_RotateMe(whatToRotate, rotateToThis, overTime, chain));
            try
            {
                mono.StartCoroutine(rotateRoutine);
            }catch
            {
                if(NEXT_FUNCTION != null && mono != null)
                {
                    NEXT_FUNCTION(mono);
                }
            }
            
        }
        IEnumerator _RotateMe(Transform whatToRotate, Vector3 rotateToThis, float overTime, MonoBehaviour mono)
        {
            Vector3 byAngles = new Vector3(rotateToThis.x - whatToRotate.eulerAngles.x,
                rotateToThis.y - whatToRotate.eulerAngles.y, rotateToThis.z - whatToRotate.eulerAngles.z);
            for (float t = 0.0f; t < (overTime - (Time.deltaTime / 2.0f)); t += Time.deltaTime)
            {
                whatToRotate.rotation = Quaternion.Euler(
        new Vector3(whatToRotate.eulerAngles.x, whatToRotate.eulerAngles.y, whatToRotate.eulerAngles.z + (byAngles.z /
        (overTime / (Time.deltaTime)))));
                //whatToRotate.rotation = Quaternion.Slerp(fromAngle, toAngle, t);
                yield return null;
            }
            float desired = rotateToThis.z;
            while (desired - 360 >= 0)
            {
                desired -= 360;
            }
            whatToRotate.rotation = Quaternion.Euler(new Vector3(0, 0, desired));
            if(NEXT_FUNCTION != null && mono != null)
            {
                NEXT_FUNCTION(mono);
            }
        }
        //now for multiple:
        public void RotateToGivenMultiple(MonoBehaviour mono)
        {
            #region Needed but solved
            /*
             * HINT: INSTEAD OF USING THE BUILT-IN FUNCTIONS, JUST 
             * INCREASE THE ROTATION BY SMALL INCREMENTS.
             * SAY 0.5 DEGREES, OR DEGREES CALCULATED LIKE THE
             * STEP IN MOVEMENT. 
             * THEN JUST NEW QUATERNION FROM EULER ANGLES
             * IN WHOM WE JUST ADD THE SMALL INCREMENTS.
             */
            #endregion
            if (rotateRoutine != null && mono != null)
            {
                mono.StopCoroutine(rotateRoutine);
            }
            rotateRoutine = _RotateMeMultiple(whatsToRotate, byAnglesS, inTime, mono);
            //StartCoroutine(_RotateMe(whatToRotate, rotateToThis, overTime, chain));
            mono.StartCoroutine(rotateRoutine);
        }
        IEnumerator _RotateMeMultiple(List<Transform> whatsToRotate, List<Vector3> rotateToThisS, float overTime, MonoBehaviour mono)
        {
            for (float t = 0.0f; t < (overTime - (Time.deltaTime / 2.0f)); t += Time.deltaTime)
            {
                for (int i = 0; i < whatsToRotate.Count; i++)
                {
                    Vector3 byAngles = new Vector3(rotateToThisS[i].x - whatsToRotate[i].eulerAngles.x,
   rotateToThisS[i].y - whatsToRotate[i].eulerAngles.y, rotateToThisS[i].z - whatsToRotate[i].eulerAngles.z);

                    whatsToRotate[i].rotation = Quaternion.Euler(
            new Vector3(whatsToRotate[i].eulerAngles.x, whatsToRotate[i].eulerAngles.y, whatsToRotate[i].eulerAngles.z + (byAngles.z /
            (overTime / (Time.deltaTime)))));
                }
                //whatToRotate.rotation = Quaternion.Slerp(fromAngle, toAngle, t);
                yield return null;
            }

            for (int i = 0; i < whatsToRotate.Count; i++)
            {
                float desired = rotateToThisS[i].z;
                while (desired - 360 >= 0)
                {
                    desired -= 360;
                }
                whatsToRotate[i].rotation = Quaternion.Euler(new Vector3(0, 0, desired));
            }
            //whatToRotate.rotation = Quaternion.Euler(new Vector3(0, 0, desired));
            if (NEXT_FUNCTION != null && mono != null)
            {
                NEXT_FUNCTION(mono);
            }
        }
        #endregion
    }
    [Serializable]
    public class AlphaAction : UIAnimation
    {
        #region Alpha
        Image img;
        float start;
        float end;
        float time;
        List<Image> imgs;
        List<float> starts;
        List<float> ends;
        List<CanvasGroup> cgs;
        CanvasGroup cg;
        IEnumerator alphaRoutine = null;
        public IEnumerator AlphaRoutine
        {
            get { return alphaRoutine; }
        }

        public void InvokeAnim(float timeBeforeStart, Action<MonoBehaviour> AlphaFunction,
MonoBehaviour mono)
        {
            if (alphaRoutine != null && mono != null)
            {
                mono.StopCoroutine(alphaRoutine);
            }
            alphaRoutine = _InvokeAnim(timeBeforeStart, AlphaFunction, mono);
            try
            {
                mono.StartCoroutine(alphaRoutine);
            }catch
            {
                if(NEXT_FUNCTION != null && mono != null)
                {
                    NEXT_FUNCTION(mono);
                }
            }
            
        }
        IEnumerator _InvokeAnim(float timeBeforeStart, Action<MonoBehaviour> MoveFunction,
            MonoBehaviour mono)
        {
            if (mono != null)
            {
                yield return new WaitForRealSecondsClass().WaitForRealSeconds(timeBeforeStart, mono);
                MoveFunction(mono);
            }
            else yield return null;

        }

        public AlphaAction(Image img, float start, float end, float inTime)
        {
            this.img = img;
            this.start = start;
            this.end = end;
            this.time = inTime;
        }
        public AlphaAction(CanvasGroup cg, float start, float end, float inTime)
        {
            this.cg = cg;
            this.start = start;
            this.end = end;
            this.time = inTime;
        }

        public AlphaAction(List<Image> imgs, List<float> starts, List<float> ends, float inTime)
        {
            if(!(imgs.Count == starts.Count && starts.Count == ends.Count))
            {
                return;
                //meaning it will never work
                //throw new WrongNumbersException("The number of the effected objects doesn't match with the number of effectors. - ScaleImage.");
            }
            this.imgs = imgs;
            this.starts = starts;
            this.ends = ends;
            this.time = inTime;
        }
        public AlphaAction(List<CanvasGroup> cgs, List<float> starts, List<float> ends, float inTime)
        {
            if (!(cgs.Count == starts.Count && starts.Count == ends.Count))
            {
                return;
                //meaning it will never work
                //throw new WrongNumbersException("The number of the effected objects doesn't match with the number of effectors. - ScaleCG");
            }
            this.cgs = cgs;
            this.starts = starts;
            this.ends = ends;
            this.time = inTime;
        }
        public delegate void UIchain(MonoBehaviour mono);
        public event UIchain NEXT_FUNCTION;
        /// <summary>
        /// Lerp a UnityEngine.Ui.Image's sprite's alpha over time.
        /// </summary>
        /// <param name="img"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="time"></param>
        public void ScaleSpriteAlpha(MonoBehaviour mono)
        {
            if(alphaRoutine != null && mono != null)
            {
                mono.StopCoroutine(alphaRoutine);
            }
            alphaRoutine = _ScaleSpriteAlpha(img, start, end, time, mono);
            try
            {
                mono.StartCoroutine(alphaRoutine);
            }catch
            {
                if(NEXT_FUNCTION != null && mono != null)
                {
                    NEXT_FUNCTION(mono);
                }
            }
            
        }
        IEnumerator _ScaleSpriteAlpha(Image img, float start, float end, float time, MonoBehaviour mono)
        {
            float lastTime = Time.unscaledTime;
            float timer = 0.0f;

            while (timer < time)
            {
                img.color = new Color(img.color.r, img.color.g, img.color.b, Mathf.Lerp(start, end, timer / time));
                timer += (Time.unscaledTime - lastTime);
                lastTime = Time.unscaledTime;
                yield return null;
            }
            //to be sure.
            img.color = new Color(img.color.r, img.color.g, img.color.b, end);
            if(NEXT_FUNCTION != null && mono != null)
            {
                NEXT_FUNCTION(mono);
            }
        }
        //now scale alpha for multiple:
        /// <summary>
        /// Same as the other but for multiple objects at once.
        /// </summary>
        /// <param name="mono"></param>
        public void ScaleSpriteAlphaMultiple(MonoBehaviour mono)
        {
            if (alphaRoutine != null && mono != null)
            {
                mono.StopCoroutine(alphaRoutine);
            }
            alphaRoutine = _ScaleSpriteAlphaMultiple(imgs, starts, ends, time, mono);
            try
            {
                mono.StartCoroutine(alphaRoutine);
            }catch
            {
                if (NEXT_FUNCTION != null && mono != null)
                {
                    NEXT_FUNCTION(mono);
                }
            }
            
        }
        IEnumerator _ScaleSpriteAlphaMultiple(List<Image> imgs, List<float> starts, List<float> ends, float time, MonoBehaviour mono)
        {
            float lastTime = Time.unscaledTime;
            float timer = 0.0f;
            while (timer < time)
            {
                //img.color = new Color(img.color.r, img.color.g, img.color.b, Mathf.Lerp(start, end, timer / time));
                //timer += (Time.unscaledTime - lastTime);
                //lastTime = Time.unscaledTime;
                for (int i = 0; i < imgs.Count; i++)
                {
                    imgs[i].color = new Color(imgs[i].color.r, imgs[i].color.g, 
                        imgs[i].color.b, Mathf.Lerp(starts[i], ends[i], timer / time));
                }
                timer += (Time.unscaledTime - lastTime);
                lastTime = Time.unscaledTime;
                yield return null;
            }
            //to be sure.
            for (int i = 0; i < imgs.Count; i++)
            {
                imgs[i].color = new Color(imgs[i].color.r, imgs[i].color.g, imgs[i].color.b, ends[i]);
            }
            //img.color = new Color(img.color.r, img.color.g, img.color.b, end);
            if (NEXT_FUNCTION != null && mono != null)
            {
                NEXT_FUNCTION(mono);
            }
        }

        /// <summary>
        /// Lerping a UnityEngine.CanvasGroup's alpha over time, from the
        /// desired scale to the desired scale.
        /// </summary>
        /// <param name="cg"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="time"></param>
        public void LerpAlpha(MonoBehaviour mono)
        {
            if(alphaRoutine != null && mono != null)
            {
                mono.StopCoroutine(alphaRoutine);
            }
            alphaRoutine = _LerpAlpha(cg, start, end, time, mono);
            try
            {
                mono.StartCoroutine(alphaRoutine);
            }catch
            {
                if(NEXT_FUNCTION != null && mono != null)
                {
                    NEXT_FUNCTION(mono);
                }
            }
           
        }
        public IEnumerator _LerpAlpha(CanvasGroup cg, float start, float end, float time, MonoBehaviour mono)
        {
            float lastTime = Time.unscaledTime;
            float timer = 0.0f;

            while (timer < time)
            {
                try
                {
                    cg.alpha = Mathf.Lerp(start, end, timer / time);
                    timer += (Time.unscaledTime - lastTime);
                    lastTime = Time.unscaledTime;
                }
                catch { }

                yield return null;
            }
            if(NEXT_FUNCTION != null && mono != null)
            {
                NEXT_FUNCTION(mono);
            }
        }
        //for multiple:

        public void LerpAlphaMultiple(MonoBehaviour mono)
        {
            if (alphaRoutine != null && mono != null)
            {
                mono.StopCoroutine(alphaRoutine);
            }
            if(mono != null)
            {
                alphaRoutine = _LerpAlphaMultiple(cgs, starts, ends, time, mono);
                mono.StartCoroutine(alphaRoutine);
            }

        }
        public IEnumerator _LerpAlphaMultiple(List<CanvasGroup> cgs, List<float> starts, List<float> ends, float time, MonoBehaviour mono)
        {
            float lastTime = Time.unscaledTime;
            float timer = 0.0f;

            while (timer < time)
            {
                //cg.alpha = Mathf.Lerp(start, end, timer / time);

                for (int i = 0; i < cgs.Count; i++)
                {
                    cgs[i].alpha = Mathf.Lerp(starts[i], ends[i], timer / time);
                }
                timer += (Time.unscaledTime - lastTime);
                lastTime = Time.unscaledTime;
                yield return null;
            }
            //cg.alpha = end;
            for (int i = 0; i < cgs.Count; i++)
            {
                cgs[i].alpha = ends[i];
            }
            if (NEXT_FUNCTION != null && mono != null)
            {
                NEXT_FUNCTION(mono);
            }
        }
        #endregion
    }
    [Serializable]
    public class ScaleAction : UIAnimation
    {
        #region Scale
        public delegate void UIchain(MonoBehaviour mono);
        public event UIchain NEXT_FUNCTION;
        IEnumerator scaleRoutine = null;
        public IEnumerator ScaleRoutine
        {
            get { return scaleRoutine; }
        }
        Transform whatToScale;
        float from;
        float to;
        float overTime;
        List<Transform> whatsToScale;
        List<float> froms;
        List<float> tos;

        public void InvokeAnim(float timeBeforeStart, Action<MonoBehaviour> ScaleFunction,
MonoBehaviour mono)
        {
            if (scaleRoutine != null && mono != null)
            {
                mono.StopCoroutine(scaleRoutine);
            }
            scaleRoutine = _InvokeAnim(timeBeforeStart, ScaleFunction, mono);
            try
            {
                mono.StartCoroutine(scaleRoutine);
            }catch
            {
                if(NEXT_FUNCTION != null && mono != null)
                {
                    NEXT_FUNCTION(mono);
                }
            }
            
        }
        IEnumerator _InvokeAnim(float timeBeforeStart, Action<MonoBehaviour> MoveFunction,
            MonoBehaviour mono)
        {
            yield return new WaitForRealSecondsClass().WaitForRealSeconds(timeBeforeStart, mono);
            MoveFunction(mono);
        }

        public ScaleAction(Transform whatToScale, float from, float to, float overTime)
        {
            this.whatToScale = whatToScale;
            this.from = from;
            this.to = to;
            this.overTime = overTime;
        }
        public ScaleAction(List<Transform> whatsToScale, List<float> froms, List<float> tos, float overTime)
        {
            if(!(whatsToScale.Count == froms.Count && froms.Count == tos.Count))
            {
                throw new WrongNumbersException("Wrong number of objects and effectors - scale.");
            }
            this.whatsToScale = whatsToScale;
            this.froms = froms;
            this.tos = tos;
            this.overTime = overTime;
        }
        /// <summary>
        /// Scale a UnityEngine.GameObject over time while keeping its proportions.
        /// </summary>
        /// <param name="whatToScale"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="overTime"></param>
        public void UniformScale(MonoBehaviour mono)
        {
            if(scaleRoutine != null && mono != null)
            {
                mono.StopCoroutine(scaleRoutine);
            }
            scaleRoutine = _UniformScale(whatToScale, from, to, overTime, mono);
            try
            {
                mono.StartCoroutine(scaleRoutine);
            }catch
            {
                if(NEXT_FUNCTION != null && mono != null)
                {
                    NEXT_FUNCTION(mono);
                }
            }
            
        }
        IEnumerator _UniformScale(Transform whatToScale, float from, float to, float overTime, MonoBehaviour mono)
        {
            float lastTime = Time.unscaledTime;
            float timer = 0.0f;
            while (timer < overTime)
            {
                whatToScale.localScale = new Vector3(Mathf.Lerp(from, to, timer / overTime),
                    Mathf.Lerp(from, to, timer / overTime), Mathf.Lerp(from, to, timer / overTime));
                timer += (Time.unscaledTime - lastTime);
                lastTime = Time.unscaledTime;
                yield return null;
            }
            if(NEXT_FUNCTION != null && mono != null)
            {
                NEXT_FUNCTION(mono);
            }
        }
        //for multiples:
        public void UniformScaleMultiple(MonoBehaviour mono)
        {
            if (scaleRoutine != null && mono != null)
            {
                mono.StopCoroutine(scaleRoutine);
            }
            scaleRoutine = _UniformScaleMultiple(whatsToScale, froms, tos, overTime, mono);
            try
            {
                mono.StartCoroutine(scaleRoutine);
            }catch
            {
                if(NEXT_FUNCTION != null && mono != null)
                {
                    NEXT_FUNCTION(mono);
                }
            }
            
        }
        IEnumerator _UniformScaleMultiple(List<Transform> whatsToScale, List<float> froms, List<float> tos, float overTime, MonoBehaviour mono)
        {
            float lastTime = Time.unscaledTime;
            float timer = 0.0f;
            while (timer < overTime)
            {
                for (int i = 0; i < whatsToScale.Count; i++)
                {
                    whatsToScale[i].localScale = new Vector3(Mathf.Lerp(froms[i], tos[i], timer / overTime),
                   Mathf.Lerp(froms[i], tos[i], timer / overTime), Mathf.Lerp(froms[i], tos[i], timer / overTime));
                }
                timer += (Time.unscaledTime - lastTime);
                lastTime = Time.unscaledTime;
                yield return null;
            }
            if (NEXT_FUNCTION != null && mono != null)
            {
                NEXT_FUNCTION(mono);
            }
        }
        #endregion
    }
    public class TimeAction : UIAnimation
    {
        public float from;
        public float to;
        public float overRealTime;
        public delegate void UIchain(MonoBehaviour mono);
        public event UIchain NEXT_FUNCTION;

        public TimeAction(float from, float to, float overRealTime)
        {
            this.from = from;
            this.to = to;
            this.overRealTime = overRealTime;
        }
        IEnumerator scaleRoutine = null;
        public IEnumerator ScaleRoutine
        {
            get { return scaleRoutine; }
        }
        public void ScaleTime(MonoBehaviour mono)
        {
            if (scaleRoutine != null && mono != null)
            {
                mono.StopCoroutine(scaleRoutine);
            }
            scaleRoutine = _ScaleTime(mono, from, to, overRealTime);
            try
            {
                mono.StartCoroutine(scaleRoutine);
            }catch
            {
                if(NEXT_FUNCTION != null && mono != null)
                {
                    NEXT_FUNCTION(mono);
                }
            }
            
        }
        private IEnumerator _ScaleTime( MonoBehaviour mono, float start, float end, float time)
        {
            float lastTime = Time.unscaledTime;
            float timer = 0.0f;

            while (timer < time)
            {
                //scaling down time 
                Time.timeScale = Mathf.Lerp(start, end, timer / time);
                timer += (Time.unscaledTime - lastTime);
                lastTime = Time.unscaledTime;

                yield return null;
            }
            Time.timeScale = end;
            if (NEXT_FUNCTION != null && mono != null)
            {
                NEXT_FUNCTION(mono);
            }
        }
    }
}
