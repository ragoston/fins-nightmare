﻿using UnityEngine;
using System.Collections;
using Assets.JumpAssist;

public class WallHitCheckerBasic : MonoBehaviour, IWallHitFunctionality
{
    public GameObject Player { get; set; }

    public void OnWallHit(Collider2D other)
    {
        if (other.gameObject.CompareTag("Wall"))
        {
            //meaning we hit the ground with the side
            //and not the feet
            //player.GetComponent<CharacterController2D>().facePalm = true;
            var charC = Player.GetComponent<CharacterController2D>();
            charC.playerSmoke.SetActive(true);
            StartCoroutine(charC.HitTheRunnerBack());

        }
    }


    // Use this for initialization
    void OnTriggerEnter2D(Collider2D other)
    {
        OnWallHit(other);
    }
}
