﻿using UnityEngine;
using System.Collections;

public class WallObject : MonoBehaviour
{

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("WallCheck"))
        {
            StartCoroutine(GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterController2D>().HitTheRunnerBack());
        }
    }
}
