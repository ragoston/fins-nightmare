﻿using UnityEngine;
using System.Collections;

public class WarmUpChildren : MonoBehaviour
{
    public string childTagToStartSleeping;
	// Use this for initialization
	void Start ()
    {
        //at first all the children is inactive.
	    foreach(Transform child in transform)
        {
            if (child.CompareTag(childTagToStartSleeping))
            {
                child.gameObject.SetActive(false);
            }
        }
	}
	//this requires a collider to be active on the parent gameobject.
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            foreach(Transform child in transform)
            {
                if (child.CompareTag(childTagToStartSleeping))
                {
                    child.gameObject.SetActive(true);
                }
            }
        }
    }
    
}
