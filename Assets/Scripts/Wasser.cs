﻿using UnityEngine;
using System.Collections;

public class Wasser : MonoBehaviour
{
    private SpriteRenderer wasserSpriteRenderer;
    private Sprite[] wasserSprites;
    private int currentImageIndex;
	// Use this for initialization
	void Start ()
    {
        currentImageIndex = 0;
        wasserSpriteRenderer = GetComponent<SpriteRenderer>();
        wasserSprites = Resources.LoadAll<Sprite>("Wasser");
	}
	
	void LateUpdate ()
    {
        currentImageIndex = Mathf.RoundToInt(Time.time * 6.0f);
        currentImageIndex = currentImageIndex % wasserSprites.Length;
        wasserSpriteRenderer.sprite = wasserSprites[currentImageIndex];
    }
}
