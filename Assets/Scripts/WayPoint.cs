﻿using UnityEngine;
using System.Collections;

public class WayPoint : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            /*
             * a progress tracker has a vector of waypoints.
             * this one here will call a static function to notify the 
             * tracker to set the waypoint to another 
             */
            SceneGameManager.Instance.gameObject.GetComponent<ProgressTracker>().ChangeWayPointsActions();
            //then we destroy the collider
            Destroy(this.GetComponent<BoxCollider2D>());
        }
    }
}
