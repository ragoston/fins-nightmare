﻿using UnityEngine;
using System.Collections;

public class WinObject : MonoBehaviour
{
    public GameObject MainCam;
    public delegate void LevelComplete();
    public event LevelComplete LEVEL_COMPLETE; 
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            CharacterController2D.charMovement.movementDisabled = true;
            StartCoroutine(WaitThenMove(other.gameObject));
            Debug.Log("Grats, you won the level.");
            if (LEVEL_COMPLETE != null)
                LEVEL_COMPLETE();
        }
    }
    IEnumerator WaitThenStopPlayer(GameObject other)
    {
        yield return new WaitForSeconds(4.0f);
        Rigidbody2D otherRB;
        if (otherRB = other.GetComponent<Rigidbody2D>())
        {
            other.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }        
    }
    IEnumerator WaitThenMove(GameObject other)
    {
        yield return new WaitForSeconds(0.3f);
        if(MainCam != null)
        {
            MainCam.GetComponent<CameraMovement>().enabled = false;
            other.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0.0f;
            other.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.right * CharacterController2D.charMovement.maxSpeed;
        }
             
        StartCoroutine(WaitThenStopPlayer(other));
    }
}
