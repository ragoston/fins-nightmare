﻿//using UnityEngine;
//using System.Collections;

//public class Wraith : MonoBehaviour
//{
//    private SpriteRenderer spr;
//    private bool stateStarted = false;
//    private Vector3 currentVelocity;
//    public ChangeDirection changeDir;
//    public enum State
//    {
//        emerge, chasing
//    }
//    public State wraithState = State.emerge;
//    private Transform target;
//	// Use this for initialization
//	void Start ()
//    {
//        target = GameObject.FindGameObjectWithTag("Player").transform;
//        spr = GetComponent<SpriteRenderer>();
//        if(spr == null)
//        {
//            spr = GetComponentInChildren<SpriteRenderer>();
//        }
//        spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, 0.0f);
//	}
	
//	// Update is called once per frame
//	void LateUpdate ()
//    {
//        switch (wraithState)
//        {
//            case State.emerge:
//                if (!stateStarted)
//                {
//                    stateStarted = true;
//                    StartCoroutine(FadeInRegular(0.0f, 1.0f, 2.0f));
//                }
//                break;
//            case State.chasing:
//                FollowPlayer();
//                break;
//        }
//	}
//    public IEnumerator FadeInRegular(float start, float end, float time)
//    {
//        float lastTime = Time.realtimeSinceStartup;
//        float timer = 0.0f;

//        while (timer < time)
//        {
//            //fading out the character using its sprite renderer alpha
//            spr.color = new Color(0.0f, 0.0f, 0.0f, Mathf.Lerp(start, end, timer / time));
//            timer += (Time.realtimeSinceStartup - lastTime);
//            lastTime = Time.realtimeSinceStartup;
//            yield return null;
//        }
//        StartCoroutine(WaitForChase(3.0f));
//    }
//    public IEnumerator WaitForChase(float chaseWait)
//    {
//        yield return new WaitForSeconds(chaseWait);
//        EnablePlayerMovement();
//        wraithState = State.chasing;
//        changeDir.MakeChanges();
//    }
//    public void FollowPlayer()
//    {
//        //similar to the camera's smooth follow, check the player's location and lerp to it.
//        transform.position = Vector3.SmoothDamp(transform.position, target.position, ref currentVelocity, 1);
//    }
//    public void EnablePlayerMovement()
//    {
//        CharacterController2D.movementDisabled = false;
//    }
//}
