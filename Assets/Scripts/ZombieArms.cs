﻿using UnityEngine;
using System.Collections;

public class ZombieArms : MonoBehaviour
{
    private SpriteRenderer zHandRenderer;
    private static Sprite[] handsSpriteSheet;
    private int currentSpriteIndex;
    public float frameRate = 24.0f;

    private int frames = 0;
    public enum ZombieHandStates
    {
        emerge, fuckAround, hide, hidden
    }
    public ZombieHandStates handStates;
	// Use this for initialization
	void Start ()
    {
        handsSpriteSheet = Resources.LoadAll<Sprite>("ZombieArms/FuckingAround");
        zHandRenderer = GetComponent<SpriteRenderer>();
        handStates = ZombieHandStates.hidden;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if(handStates != ZombieHandStates.hidden)
        {
            ZombieHandAnimationController();
        }
        
	}
    void ZombieHandAnimationController()
    {
        float handSpeed = 0.4f;
        switch (handStates)
        {
            case ZombieHandStates.emerge:
                transform.position += Vector3.up * handSpeed;
                frames++;              
                if(frames >= 3)
                {
                    // -6.42 -5.23
                    //meaning we automatically enter the next state.
                    handStates = ZombieHandStates.fuckAround;
                }
                
                break;
            case ZombieHandStates.fuckAround:

                ZombieHandAnimationPlayer(handsSpriteSheet);

                break;
            case ZombieHandStates.hide:
                //we can only be here if the emerge animation has already played.
                //this means the frames, as the class's variable is 3.

                transform.position -= Vector3.up * handSpeed;
                frames--;

                if (frames <= 0)
                {
                    // -6.42 -5.23
                    //meaning we automatically enter the next state.
                    handStates = ZombieHandStates.hidden;
                }
                break;
            case ZombieHandStates.hidden:

                break;
        }
    }
    void ZombieHandAnimationPlayer(Sprite[] wantedSpriteSheet)
    {
        currentSpriteIndex = Mathf.RoundToInt(Time.time * frameRate);
        currentSpriteIndex = currentSpriteIndex % wantedSpriteSheet.Length;
        zHandRenderer.sprite = wantedSpriteSheet[currentSpriteIndex];
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            handStates = ZombieHandStates.emerge;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            handStates = ZombieHandStates.hide;
            StartCoroutine(WaitThenDestroyGO());
        }
    }
    IEnumerator WaitThenDestroyGO()
    {
        yield return new WaitForSeconds(6.0f);
        Destroy(this.gameObject);
    }
}
