﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CarriageTrap : MonoBehaviour
{
    //the wheel that will hit the player.
    [SerializeField]
    private Rigidbody2D rb2dWheel;
    [SerializeField]
    private Rigidbody2D rb2dCarriage;

    public float chanceOfTrap;
    public float forceMultiplier;
    public float carriageGravityFactor;
	void Start ()
    {
        var res = Random.Range(0f, 100f);
        if (res > chanceOfTrap)
        {
            //remove all rigidbodies and colliders.
            Destroy(rb2dWheel);
            Destroy(rb2dCarriage);
            Destroy(this);
        }
        if (rb2dWheel == null || rb2dCarriage == null)
        {
            Debug.Log(gameObject.name + " has no wheelObj or carriage assigned to it.");
            Destroy(this);
        }
        rb2dCarriage.gravityScale = 0f;
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {

            //the player is in harm's way
            rb2dWheel.AddForce((collision.transform.position - rb2dWheel.transform.position) * forceMultiplier);
            rb2dCarriage.gravityScale = carriageGravityFactor;
        }
    }

}
