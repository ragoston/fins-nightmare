﻿//using System;
//using System.Reflection;
//using UnityEditor;
//using UnityEditorInternal;
//using UnityEngine;

//public class SetSortingLayers : EditorWindow
//{
//    private string[] sortingLayersList;
//    private void OnFocus()
//    {
//        sortingLayersList = GetSortingLayerNames();

//    }
//    private string sortingLayer;
//    public string[] GetSortingLayerNames()
//    {
//        Type internalEditorUtilityType = typeof(InternalEditorUtility);
//        PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
//        return (string[])sortingLayersProperty.GetValue(null, new object[0]);
//    }
//    [MenuItem("Window/Set sorting layers")]
//    public static void ShowWindow()
//    {
//        EditorWindow.GetWindow(typeof(SetSortingLayers));
//    }
//    private void OnGUI()
//    {
//        EditorGUILayout.LabelField("Select the objects you with to set the sorting layer.");
//        sortingLayer = EditorGUILayout.TextField("Sorting layer:", sortingLayer);
//        EditorGUILayout.Separator();
//        if (GUILayout.Button("GO"))
//        {
//            ChangeSortingLayer();
//        }
//    }

//    private void ChangeSortingLayer()
//    {
//        if (!sortingLayersList.Contains(sortingLayer)) { Debug.Log("Invalid sorting layer name."); return; }
//        if (Selection.gameObjects == null) { Debug.Log("Select some objects first."); return; }
//        foreach (var go in Selection.gameObjects)
//        {
//            var rend = go.GetComponent<Renderer>();
//            if (rend != null)
//            {
//                rend.sortingLayerName = sortingLayer;
//            }
//        }
//    }
//}
