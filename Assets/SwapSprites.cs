﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using UnityEditor;
//using UnityEngine;

//public class SwapSprites : EditorWindow
//{
//    private Sprite _from;
//    private Sprite _to;

//    [MenuItem("Window/Swap sprites")]
//    public static void ShowWindow()
//    {
//        EditorWindow.GetWindow(typeof(SwapSprites));
//    }
//    private void OnGUI()
//    {
//        EditorGUILayout.LabelField("From:");
//        _from = (Sprite)EditorGUILayout.ObjectField(_from, typeof(Sprite));
//        EditorGUILayout.LabelField("To:");
//        _to = (Sprite)EditorGUILayout.ObjectField(_to, typeof(Sprite));
//        EditorGUILayout.Separator();
//        if (GUILayout.Button("GO"))
//        {
//            Swap();
//        }
//    }
    
//    private void Swap()
//    {
//        /*
//         * getting all GameObjects with the given sprite on them
//         * finding out the difference between the sprites' sizes
//         * scale the GameObjects accordingly.
//         */
//        var objs = GameObject.FindObjectsOfType<GameObject>()
//            .Where(g =>
//            {
//                var sr = g.GetComponent<SpriteRenderer>();
//                if (sr != null)
//                {
//                    if (sr.sprite != null && sr.sprite.name.Equals(_from.name)) { return true; }
//                    return false;
//                }
//                return false;
//            }).AsEnumerable();

//        foreach (var obj in objs)
//        {
//            float xScaleFactor = _from.texture.width*(1f) / _to.texture.width*(1f);
//            float yScaleFactor = _from.texture.height * (1f) / _to.texture.height * (1f);
//            obj.GetComponent<SpriteRenderer>().sprite = _to;
//            if(!(xScaleFactor == 1 && yScaleFactor == 1))
//            {
//                obj.transform.localScale = new Vector3(
//                    obj.transform.localScale.x * xScaleFactor,
//                    obj.transform.localScale.y * yScaleFactor,
//                    obj.transform.localScale.z);
//            }
//        }
//        objs = null;
//        Resources.UnloadAsset(_from);
//    }
//}
