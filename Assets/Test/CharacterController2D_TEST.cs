﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Statistics;
using Assets.MainMenuStuff;
using Assets.JumpAssist;

public class CharacterController2D_TEST : MonoBehaviour
{
    //public delegate void ChangingDirections(float localDist);
    //public event ChangingDirections PROGRESS_CHANGE_DIR;

    //bool changeDir;
    //public bool ChangeDir
    //{
    //    get
    //    {
    //        return changeDir;
    //    }
    //    set
    //    {
    //        //so we can notify other scripts of the change.
    //        //LIKE THE BLOODY TRACKER SCRIPT
    //        //if(PROGRESS_CHANGE_DIR != null)
    //        //{
    //        //    PROGRESS_CHANGE_DIR(MainGameManager.Instance.GetComponent<ProgressTracker>().localDistance);
    //        //}

    //        changeDir = value;
    //    }
    //}
    //public bool jump;
    //public static int tippings;
    //public float moveForce;
    //public static float maxSpeed;
    //public static float jumpForce;
    public Transform groundCheck;
    public Transform wallCheck;
    //public static float jumpSpeed = 23.0f;
    //public bool grounded;
    //public bool walled;
    //public float groundRadius;
    //public float wallRadius;
    public LayerMask whatIsGround;
    public LayerMask whatIsWall;

    private Animator anim;
    private Rigidbody2D rb2d;
    public bool facePalm;
    //the movement disabled used to be not static.
    //public static bool movementDisabled;
    private static Sprite[] runSprites;
    private static Sprite[] jumpStartSprites;
    private static Sprite[] jumpAirSprites;
    private static Sprite[] fallSprites;
    private static Sprite[] getUpSprites;
    public static SpriteRenderer charSpriteRenderer;
    public static int currentImageIndex;
    public GameObject playerSmoke;
    public enum CharAnimStates
    {
        running,
        jumpStart,
        jumpAir,
        fall,
        getUp,
        still
    }
    public DataBank bank;
    public static CharAnimStates animState;
    public static bool playerDied;
    //COMPONENT BASED MODEL FOR JUMP FUNCTIONALITY
    public static ICharacterMovementFunctionality charMovement;
    private IWallHitFunctionality wallHitFunc;
    // Use this for initialization
    void Start()
    {
        try
        {
            var b = GameObject.FindGameObjectWithTag("DataBank");
            bank = b.GetComponent<DataBank>();
            //just to be safe.
            if (bank != null)
            {
                bank.MMTools.SceneManager.SetTimeScale(bank.MMTools.SceneManager.GlobalTimeScale);
            }
        }
        catch { }
        

        //tippings = 0;
        //moveForce = 36.50f;
        ////MAXSPEED IS ORIGINALLY 8.0f
        //maxSpeed = 8.0f;
        //jumpForce = 1100.0f;
        //movementDisabled = false;
        ////ground radius used to be 0.2f
        //groundRadius = 0.26f;

        //jump = false;
        //grounded = false;
        rb2d = GetComponent<Rigidbody2D>();
        //runSprites = (Sprite[])AssetDatabase.LoadAllAssetsAtPath("Resources/CharacterOPT/runAnimExportOPT");
        runSprites = Resources.LoadAll<Sprite>("Character/runAnimExport");
        jumpStartSprites = Resources.LoadAll<Sprite>("Character/jumpStartAnim");
        jumpAirSprites = Resources.LoadAll<Sprite>("Character/jumpAirAnim");
        fallSprites = Resources.LoadAll<Sprite>("Character/fallAnim");
        getUpSprites = Resources.LoadAll<Sprite>("Character/getUp");
        animState = CharAnimStates.running;
        charSpriteRenderer = GetComponentInChildren<SpriteRenderer>();
        playerDied = false;
        //PROGRESS_CHANGE_DIR += MainGameManager.Instance.GetComponent<ProgressTracker>().SwitchDir;


        //HAS TO BE CHANGED ACCORDING TO GAME MODE!!
        charMovement = new CharMovementComponent(() =>
        {
            animState = CharAnimStates.jumpStart;
            currentImageIndex = 0;
        },
            rb2d, groundCheck, wallCheck, whatIsGround, whatIsWall);

        wallCheck.gameObject.AddComponent<WallHitAssisted>();
        (wallCheck.gameObject.GetComponent(typeof(IWallHitFunctionality)) as IWallHitFunctionality)
            .Player = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {

        if ((animState == CharAnimStates.jumpAir || animState == CharAnimStates.jumpStart) && charMovement.grounded)
        {
            //hopefully this means we are not fucked up and yet we are airborne
            //so we must be jumping.
            //but we became grounded so we need to set the shit back to running.
            animState = CharAnimStates.running;
        }


        //checking if we are far low
        if (transform.position.y < -100 && !playerDied)
        {
            //it doesn't really matter.
            Debug.Log("FAILED LEVEL, TOO LOW!!!");
            StartCoroutine(SceneGameManager.Instance.WaitThenFailLevel());
        }
        try
        {
            CharacterAnimationController();
        }
        catch { }
    }
    void FixedUpdate()
    {
        charMovement.Movement();
    }
    //void Movement()
    //{
    //    //float h = Input.GetAxis("Horizontal");
    //    float h = 1;
    //    /*
    //     * additional for direction change: if we change it, we multiply dat shit by -1
    //    */
    //    if (changeDir)
    //    {
    //        h *= -1.0f;
    //    }

    //    //additional until here.
    //    if (!movementDisabled)
    //    {
    //        rb2d.velocity = new Vector2(maxSpeed * h, rb2d.velocity.y);
    //    }
    //    //FROM HERE IT WAS IN UPDATE
    //    grounded = Physics2D.OverlapCircle(new Vector2(groundCheck.transform.position.x, groundCheck.transform.position.y),
    //        groundRadius, whatIsGround);
    //    if (!movementDisabled && grounded &&
    ///*Input.GetButton("Jump")*/ JumpInput.JUMP)
    //    {
    //        //then we can jump
    //        currentImageIndex = 0;
    //        animState = CharAnimStates.jumpStart;
    //        //added again from here

    //        //zeroing out the Y axis velocity if we had one
    //        rb2d.velocity = new Vector2(rb2d.velocity.x, 0.0f);

    //        //until here
    //        jump = true;
    //    }

    //    //UNTIL HERE.
    //    if (jump && !movementDisabled)
    //    {
    //        jump = false;
    //        //old from here
    //        //rb2d.AddForce(new Vector2(0.0f, jumpForce));
    //        //to here.

    //        /*
    //         * let's try giving the player a velocity instead of a force.
    //         * this shit is the new one. 
    //         * note that we are not ADDING to the velocity but setting an entirely new one.
    //         * with this we may eliminate the possibilities of adding too much force
    //         * or speed and get unpredictable movement.
    //        */

    //        rb2d.velocity = new Vector2(rb2d.velocity.x, 23.0f);
    //    }
    //}
    public IEnumerator HitTheRunnerBack()
    {
        //movementDisabled = true;
        playerSmoke.SetActive(true);
        StartCoroutine(SceneGameManager.Instance.WaitThenFailLevel());
        Debug.Log("PLAYER DIED");
        yield return null;
        //old from here:

        //MovementDisabledActions();
        //tippings++;
        //if(tippings >= 3)
        //{
        //    playerSmoke.SetActive(true);
        //    StartCoroutine(SceneGameManager.Instance.WaitThenFailLevel());
        //}
        //currentImageIndex = 0;
        //animState = CharAnimStates.fall;
        //walled = false;
        //facePalm = false;
        //rb2d.velocity = new Vector2(0.0f, 0.0f);
        //if(transform.localScale.x < 0.0f)
        //{
        //    //meaning the character is mirrored and facing the other direction
        //    rb2d.AddForce(new Vector2(800.0f, 0.0f));
        //}
        //else
        //{
        //    rb2d.AddForce(new Vector2(-800.0f, 0.0f));
        //}

        //yield return WaitForRealSeconds(1.0f);
        //jump = false;
        //MovementEnabledActions();
        ////movementDisabled = false;
        //currentImageIndex = 0;
        //animState = CharAnimStates.getUp;
    }
    void CharacterAnimationController()
    {
        /*
         * ***** PLAN FOR ANIMATIONS *****
         * by default we are running which is a looping animation.
         * the grounded indicator switches the state to jumpstart.
         * the jumpstart goes off only once and at the end switches to jumpAir.
         * when the player catches gound, the state is running.
         * when the player hits a kill object, or dies, we go to fall.
         * when the coroutine "hit the runner back" finishes, the player goes to getUp.
         * at the end of getUp we are running.
         * 
         * We use the same indexer to get which sprite to use.
         * at the making of the transitions we need to set the indexer to 0
         * so we start from the beginning.
        */
        switch (animState)
        {
            case CharAnimStates.running:
                //here we use a looping animation.
                currentImageIndex = Mathf.RoundToInt(Time.time * 24.0f);
                currentImageIndex = currentImageIndex % runSprites.Length;
                charSpriteRenderer.sprite = runSprites[currentImageIndex];
                break;
            case CharAnimStates.jumpStart:
                //non looping, at the end setting the state to jumpAir
                charSpriteRenderer.sprite = jumpStartSprites[currentImageIndex];
                currentImageIndex++;
                if (currentImageIndex >= jumpStartSprites.Length)
                {
                    currentImageIndex = 0;
                    animState = CharAnimStates.jumpAir;
                }
                break;
            case CharAnimStates.jumpAir:
                currentImageIndex = Mathf.RoundToInt(Time.time * 24.0f);
                currentImageIndex = currentImageIndex % jumpAirSprites.Length;
                charSpriteRenderer.sprite = jumpAirSprites[currentImageIndex];
                break;
            case CharAnimStates.getUp:
                charSpriteRenderer.sprite = getUpSprites[currentImageIndex];
                currentImageIndex++;
                if (currentImageIndex >= getUpSprites.Length)
                {
                    animState = CharAnimStates.running;
                    currentImageIndex = 0;
                }
                break;
            case CharAnimStates.fall:
                if (currentImageIndex < fallSprites.Length)
                {
                    //we don't set this to getting up at the end.
                    //another function - coroutine - takes care of that.
                    //so we just leave it like it is.
                    charSpriteRenderer.sprite = fallSprites[currentImageIndex];
                    currentImageIndex++;
                }
                break;
            case CharAnimStates.still:
                //nothing is here, the character stays where we left him.
                break;
        }
    }
    public static void MovementDisabledActions()
    {
        //Trambouline.behaveAsCollider = true;
        charMovement.movementDisabled = true;
    }
    public static void MovementEnabledActions()
    {
        //Trambouline.behaveAsCollider = false;
        charMovement.movementDisabled = false;
    }
    public IEnumerator WaitThenDisableRigidbody2D(float delay)
    {
        yield return WaitForRealSeconds(delay);
        Destroy(GetComponent<Rigidbody2D>());
    }
    public IEnumerator ScaleSpriteAlpha(float start, float end, float time)
    {
        float lastTime = Time.realtimeSinceStartup;
        float timer = 0.0f;

        while (timer < time)
        {
            //fading out the character using its sprite renderer alpha
            charSpriteRenderer.color = new Color(0.0f, 0.0f, 0.0f, Mathf.Lerp(start, end, timer / time));
            timer += (Time.realtimeSinceStartup - lastTime);
            lastTime = Time.realtimeSinceStartup;
            yield return null;
        }
    }
    public static void UnFreezeCharAnim()
    {
        animState = CharAnimStates.running;
        currentImageIndex = 0;

    }
    IEnumerator _WaitForRealSeconds(float aTime)
    {
        while (aTime > 0.0f)
        {
            aTime -= Mathf.Clamp(Time.unscaledDeltaTime, 0, 0.2f);
            yield return null;
        }
    }
    Coroutine WaitForRealSeconds(float aTime)
    {
        return StartCoroutine(_WaitForRealSeconds(aTime));
    }

    //void SetChangeDir()
    //{
    //    PROGRESS_CHANGE_DIR(MainGameManager.Instance.GetComponent<ProgressTracker>().localDistance);
    //}

}
