﻿using Assets.HighlightDanger.Code;
using Assets.MainMenuStuff.Audio;
using Assets.Managers;
using Assets.Scripts.InGameInit;
using Assets.Scripts.Statistics;
using Assets.TrainingGround.UI.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.TrainingGround.Scripts
{
    public class TrainingGameManager : MonoBehaviour
    {
        /*
         * Order of passed objects to initialize:
         * DataBank
         * TimeScaler
         * 
         */
        public Continue cont;
        DataBank bank;
        ITimeScaler timeScaler;
        Color highlightColor;
        IMusicProvider musicProvider;
        private void Awake()
        {
            try
            {
                var _bank = GameObject.FindGameObjectWithTag("DataBank");
                if(_bank != null)  bank = _bank.GetComponent<DataBank>();


                    
                //if(res != null)
                //{
                //    cont = res.Where(x => x.gameObject.activeSelf)
                //        .First().gameObject.GetComponent<Continue>();
                //}
            }
            catch
            {
                var b = GameObject.FindGameObjectWithTag("DataBank");
                if (b != null)
                {
                    bank = b.GetComponent<DataBank>();
                }
                if (b == null)
                {
                    Debug.Log("Found no DataBank on scene.");return;
                }
            }
            try
            {
                //because fckn unity.
                foreach (var item in GameObject.Find("MusicObject")
                    .GetComponents<MonoBehaviour>())
                {
                    if (typeof(IMusicProvider).IsAssignableFrom(item.GetType()))
                    {
                        musicProvider = item as IMusicProvider;
                        break;
                    }
                }

            }
            catch { print("Found no IMusicProvider."); }
            try
            {
                cont = GameObject.FindGameObjectWithTag("JumpInput")
                    .GetComponent<Continue>();
            }
            catch { print("Found no Continue"); }
            try

            {
                timeScaler = (SceneGameManager.Instance as IGameManager)
                .TimeScaler;
            }
            catch { print("Issue when trying to find timeScaler."); }

            //initialize all that has to be:
            try
            {
                highlightColor = SceneGameManager.Instance
                    .GetComponent<DangerHighlightEffectColor>().HighlightColor;
            }
            catch { print("Issue when trying to find Highlight color."); }
            

            //foreach (var item in goPool)
            //{
            //    if(item.GetComponent<DangerHighlightEffectColor>() 
            //        != null)
            //    {
            //        highlightColor = item.GetComponent<DangerHighlightEffectColor>().HighlightColor;
            //        break;
            //    }
            //}
            //highlightColor = goPool.Where((x) => x.GetComponent<DangerHighlightEffectColor>() != null)
            //    .First().GetComponent<DangerHighlightEffectColor>().HighlightColor;
            foreach (var item in GameObject.FindObjectsOfType<InitializeMe_Training>())
            {
                item.GetComponent<InitializeMe_Training>().Initialize(
                    new object[] {bank, timeScaler,cont, highlightColor, musicProvider});
            }
            //cont.gameObject.SetActive(false);
        }
    }
}
