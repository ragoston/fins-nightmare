﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.TrainingGround.UI.Messages
{
    public class CheckByPointVisibility : MonoBehaviour
    {
        public Action _OnBecameVisible;
        private void OnBecameVisible()
        {
            if (_OnBecameVisible != null)
                _OnBecameVisible.Invoke();
        }
    }
}
