﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.TrainingGround.UI.Messages
{
    public class Continue : MonoBehaviour, IPointerDownHandler
    {
        public Action _OnPointerDown;
        public bool stopped;
        public void OnPointerDown(PointerEventData eventData)
        {
            if (stopped && _OnPointerDown != null)
            {
                stopped = !stopped;
                _OnPointerDown.Invoke();
            }
                
        }
    }
}
