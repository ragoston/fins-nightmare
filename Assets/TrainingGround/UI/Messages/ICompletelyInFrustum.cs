﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.TrainingGround.UI.Messages
{
    public interface ICompletelyInFrustum
    {
        bool IsCompletelyInFrustum(Transform transform,
            Camera cam, Mesh mesh);
        IEnumerator CheckFullVisibility(Transform tr, Camera cm, Mesh m);
        //do something when became fully visible.
        Action OnBecameFullyVisible { get; set; }
    }
}
