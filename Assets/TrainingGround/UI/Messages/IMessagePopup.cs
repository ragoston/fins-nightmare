﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.TrainingGround.UI.Messages
{
    /// <summary>
    /// A message to tell the player what to do. 
    /// The two animations may even be NULL, especially if the message is just ingame text.
    /// </summary>
    public interface IMessagePopup
    {
        Action<MonoBehaviour> AnimationIn { get; set; }
        Action<MonoBehaviour> AnimationOut { get; set; }
        ICompletelyInFrustum FullyVisible { get; set; }
        string Message { get;}
    }
}
