﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.TrainingGround.UI.Messages
{
    public abstract class InitializeMe_Training : MonoBehaviour
    {
        public abstract void Initialize(params object[] pars);
    }
}
