﻿using Assets.Scripts.InGameInit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Scripts.Statistics;
using Assets.Managers;
using Assets.MainMenuStuff.Audio;

namespace Assets.TrainingGround.UI.Messages
{
    [RequireComponent(typeof(TrainingMessageBox))]
    public class InitializeMessageBox : InitializeMe_Training
    {
        public override void Initialize(params object[] pars)
        {
            GetComponent<TrainingMessageBox>()
                .Intialize(pars[0] as DataBank, pars[1] as ITimeScaler, pars[2] as Continue,
                (Color)pars[3], (IMusicProvider)pars[4]);
        }
    }
}
