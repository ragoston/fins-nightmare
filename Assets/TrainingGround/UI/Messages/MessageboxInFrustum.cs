﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.TrainingGround.UI.Messages
{
    internal class MessageboxInFrustum : ICompletelyInFrustum
    {
        public Action OnBecameFullyVisible
        {
            get;set;
        }
        private bool wasVisibleOnce = false;
        /// <summary>
        /// Mesh may have to be generated fron sprite or bounds. 
        /// </summary>
        /// <param name="transform"></param>
        /// <param name="cam"></param>
        /// <param name="mesh"></param>
        /// <returns></returns>
        public bool IsCompletelyInFrustum(Transform transform, 
            Camera cam, Mesh mesh)
        {
            Plane[] planes = GeometryUtility.CalculateFrustumPlanes(cam);

            foreach (Plane plane in planes)
            {
                foreach (Vector3 vertice in mesh.vertices)
                {
                    if (plane.GetDistanceToPoint(transform.TransformPoint(vertice)) < 0)
                    {
                        return false;
                    }
                }
            }
            if (!wasVisibleOnce)
            {
                wasVisibleOnce = true;
                if (OnBecameFullyVisible != null)
                    OnBecameFullyVisible.Invoke();
            }
            return true;
        }
        public MessageboxInFrustum(Action onBecameFullyVisible)
        {
            OnBecameFullyVisible = onBecameFullyVisible;
        }
        public MessageboxInFrustum() { }
        /// <summary>
        /// Making a mesh out of bounds. 
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Mesh BoundsToMesh(Bounds b)
        {
            try
            {
                Mesh m = new Mesh();
                m.Clear();
                Vector3[] vertices = new Vector3[4];
                vertices[0] = new Vector3(b.center.x - b.extents.x, b.center.y + b.center.y, 0.1f);
                vertices[1] = new Vector3(b.center.x + b.extents.x, b.center.y + b.center.y, 0.1f);
                vertices[2] = new Vector3(b.center.x + b.extents.x, b.center.y - b.center.y, 0.1f);
                vertices[3] = new Vector3(b.center.x - b.extents.x, b.center.y - b.center.y, 0.1f);
                int[] triangles = { 0, 1, 2, 2, 3, 0 };
                m.SetVertices(vertices.ToList());
                m.SetTriangles(triangles, 0);
                return m;
            }
            catch { return null; }
        }

        public IEnumerator CheckFullVisibility(Transform tr, Camera cam, Mesh m)
        {
            while (true)
            {
                IsCompletelyInFrustum(tr, cam, m);
                yield return null;
            }
        }
    }
}
