﻿using Assets.HighlightDanger.Code;
using Assets.MainMenuStuff;
using Assets.MainMenuStuff.Audio;
using Assets.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.TrainingGround.UI.Messages
{
    internal class TrainingMessageBox : MonoBehaviour
    {
        ITimeScaler timeScaler;
        public static readonly int MessageBoxPriority_out = 5;
        public static readonly int MessageBoxPriority_in = 20;
        Color highlightColor;
        private float gameTimeScale = 1f;
        [SerializeField]
        Text message;
        internal void Intialize(Assets.Scripts.Statistics.DataBank bank, ITimeScaler timeScaler
            ,Continue cont, Color highlightColor, IMusicProvider musicProvider)
        {
            this.highlightColor = highlightColor;
            if(message == null)
            {
                message = GetChildrenByComponent<Text>();
            }
            try
            {
                message.gameObject.GetComponent<Outline>().effectColor = highlightColor;
            }
            catch
            {
                Debug.Log("The MessageBox has no Outline component. Adding one now...");
            }
            this.timeScaler = timeScaler;
            if (bank != null)
            {
                gameTimeScale = bank.MMTools.SceneManager.GlobalTimeScale;
            }
            
            var chkPnt = GetChildrenByComponent<CheckByPointVisibility>();
            Action _onVisible = () =>
            {
                cont.stopped = true;
                //cont.gameObject.SetActive(true);
                CharacterController2D.MovementDisabledActions();
                if(musicProvider != null)
                    musicProvider.Pause();
                try
                {
                    timeScaler.ScaleTime(gameTimeScale, 0, 1f);
                }
                catch { Time.timeScale = 0; }

            };
            Action _onPtrDown = () =>
            {
                CharacterController2D.MovementEnabledActions();
                if(musicProvider != null)
                    musicProvider.Continue();
                try
                {
                    timeScaler.ScaleTime(0, gameTimeScale, 1f);
                }
                catch { Time.timeScale = gameTimeScale; }

                //cont.gameObject.SetActive(false);
            };
            if (chkPnt != null)
            {
                //chkPnt._OnBecameVisible = _onVisible;
                chkPnt._OnBecameVisible = () =>
                {
                    timeScaler.AddInitiation(MessageBoxPriority_in, _onVisible);
                    timeScaler.ScaleTimeInLine(MessageBoxPriority_in);                                      
                    timeScaler.AddInitiation(MessageBoxPriority_out, _onPtrDown);
                };
            }
            else Debug.Log("Didn't find checkpoint visibility on object" + gameObject.name);


            //cont._OnPointerDown = _onPtrDown;
            cont._OnPointerDown = () =>
            {
                timeScaler.ScaleTimeInLine(MessageBoxPriority_out);
            };
            //cont.gameObject.SetActive(false);
        }
        T GetChildrenByComponent<T>() where T : Component
        {
            foreach (Transform item in transform)
            {
                var cbpv = item.GetComponent<T>();
                if (cbpv != null)
                    return cbpv;
            }
            return null;
        }
    }
}
