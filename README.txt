Originally started in 2016, but finished in 2017, this game was intended as a practice project.
As a reason, the code in some scenarios seem disorganized, as those were done earlier, and
some of the code is well-developed.
I take pride not in the surface scripting, which is just quantity work, but rather in the editor
extensions, e.g. one to convert my sprites to meshes based on transparency, and adjust their sizes
so that on the level their position and size remain the same. 
I made everything alone in the game, that includes the code, the artwork and design and the soundtracks as well. 
Note I'm a developer primarily and only rarely I set out to render something only for the fun of it. 
All 2D assets were modeled and rendered because I had no means to digitally draw. 
As for songwriting, I never preferred that, I just needed some soundtracks for my game. I learned to write basic music
and to use a DAW in a week. The game's artistic side shows no peculiar artist talent, but resilience nonetheless.
After the game began to show its final shape, I figured I could monetize it and upload it to
an online store, like Google Play. Unfortunately due to the lack of promoter support the game didn't get recognized,
it has barely anyone who downloaded it. 
It may still be available on https://play.google.com/store/apps/details?id=com.agirogames.finsnightmare
so anyone interested can download the game, or take a look at its promo video.
